-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for encuestas
CREATE DATABASE IF NOT EXISTS `encuestas` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `encuestas`;

-- Dumping structure for table encuestas.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.admins: ~2 rows (approximately)
DELETE FROM `admins`;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `name`, `email`, `username`, `email_verified_at`, `image`, `access`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'admin@site.com', 'admin', NULL, '604f007face2d1615790207.png', NULL, '$2y$10$0wC4zEftJqMHrmF1thcj3.88ouwfL/2db4ReNNv4zHBSDDMLcxR7W', NULL, '2023-08-13 03:59:17');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Dumping structure for table encuestas.admin_password_resets
CREATE TABLE IF NOT EXISTS `admin_password_resets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.admin_password_resets: ~0 rows (approximately)
DELETE FROM `admin_password_resets`;
/*!40000 ALTER TABLE `admin_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_password_resets` ENABLE KEYS */;

-- Dumping structure for table encuestas.answers
CREATE TABLE IF NOT EXISTS `answers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `surveyor_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_answer` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.answers: ~14 rows (approximately)
DELETE FROM `answers`;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;

-- Dumping structure for table encuestas.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.categories: ~2 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Formulario Privado', 1, '2023-08-01 23:48:44', '2023-08-01 23:48:44'),
	(2, 'Formulario Publico', 1, '2023-08-01 23:53:01', '2023-08-01 23:53:01');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table encuestas.deposits
CREATE TABLE IF NOT EXISTS `deposits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `surveyor_id` int(11) DEFAULT NULL,
  `method_code` int(10) unsigned NOT NULL,
  `amount` decimal(18,8) NOT NULL,
  `method_currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` decimal(18,8) NOT NULL,
  `rate` decimal(18,8) NOT NULL,
  `final_amo` decimal(18,8) DEFAULT '0.00000000',
  `detail` text COLLATE utf8mb4_unicode_ci,
  `btc_amo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btc_wallet` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trx` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `try` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=>success, 2=>pending, 3=>cancel',
  `admin_feedback` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.deposits: ~0 rows (approximately)
DELETE FROM `deposits`;
/*!40000 ALTER TABLE `deposits` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposits` ENABLE KEYS */;

-- Dumping structure for table encuestas.email_sms_templates
CREATE TABLE IF NOT EXISTS `email_sms_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `act` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subj` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` text COLLATE utf8mb4_unicode_ci,
  `sms_body` text COLLATE utf8mb4_unicode_ci,
  `shortcodes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_status` tinyint(4) NOT NULL DEFAULT '1',
  `sms_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.email_sms_templates: ~18 rows (approximately)
DELETE FROM `email_sms_templates`;
/*!40000 ALTER TABLE `email_sms_templates` DISABLE KEYS */;
INSERT INTO `email_sms_templates` (`id`, `act`, `name`, `subj`, `email_body`, `sms_body`, `shortcodes`, `email_status`, `sms_status`, `created_at`, `updated_at`) VALUES
	(1, 'PASS_RESET_CODE', 'Password Reset', 'Password Reset', '<div>We have received a request to reset the password for your account on <b>{{time}} .<br></b></div><div>Requested From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div><div><br></div><br><div><div><div>Your account recovery code is:&nbsp;&nbsp; <font size="6"><b>{{code}}</b></font></div><div><br></div></div></div><div><br></div><div><font size="4" color="#CC0000">If you do not wish to reset your password, please disregard this message.&nbsp;</font><br></div><br>', 'Your account recovery code is: {{code}}', ' {"code":"Password Reset Code","ip":"IP of User","browser":"Browser of User","operating_system":"Operating System of User","time":"Request Time"}', 1, 1, '2019-09-24 23:04:05', '2021-01-06 00:49:06'),
	(2, 'PASS_RESET_DONE', 'Password Reset Confirmation', 'You have Reset your password', '<div><p>\r\n    You have successfully reset your password.</p><p>You changed from&nbsp; IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}}&nbsp;</b> on <b>{{time}}</b></p><p><b><br></b></p><p><font color="#FF0000"><b>If you did not changed that, Please contact with us as soon as possible.</b></font><br></p></div>', 'Your password has been changed successfully', '{"ip":"IP of User","browser":"Browser of User","operating_system":"Operating System of User","time":"Request Time"}', 1, 1, '2019-09-24 23:04:05', '2020-03-07 10:23:47'),
	(3, 'EVER_CODE', 'Email Verification', 'Please verify your email address', '<div><br></div><div>Thanks For join with us. <br></div><div>Please use below code to verify your email address.<br></div><div><br></div><div>Your email verification code is:<font size="6"><b> {{code}}</b></font></div>', 'Your email verification code is: {{code}}', '{"code":"Verification code"}', 1, 1, '2019-09-24 23:04:05', '2021-01-03 23:35:10'),
	(4, 'SVER_CODE', 'SMS Verification ', 'Please verify your phone', 'Your phone verification code is: {{code}}', 'Your phone verification code is: {{code}}', '{"code":"Verification code"}', 0, 1, '2019-09-24 23:04:05', '2020-03-08 01:28:52'),
	(5, '2FA_ENABLE', 'Google Two Factor - Enable', 'Google Two Factor Authentication is now  Enabled for Your Account', '<div>You just enabled Google Two Factor Authentication for Your Account.</div><div><br></div><div>Enabled at <b>{{time}} </b>From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div>', 'Your verification code is: {{code}}', '{"ip":"IP of User","browser":"Browser of User","operating_system":"Operating System of User","time":"Request Time"}', 1, 1, '2019-09-24 23:04:05', '2020-03-08 01:42:59'),
	(6, '2FA_DISABLE', 'Google Two Factor Disable', 'Google Two Factor Authentication is now  Disabled for Your Account', '<div>You just Disabled Google Two Factor Authentication for Your Account.</div><div><br></div><div>Disabled at <b>{{time}} </b>From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div>', 'Google two factor verification is disabled', '{"ip":"IP of User","browser":"Browser of User","operating_system":"Operating System of User","time":"Request Time"}', 1, 1, '2019-09-24 23:04:05', '2020-03-08 01:43:46'),
	(16, 'ADMIN_SUPPORT_REPLY', 'Support Ticket Reply ', 'Reply Support Ticket', '<div><p><span style="font-size: 11pt;" data-mce-style="font-size: 11pt;"><strong>A member from our support team has replied to the following ticket:</strong></span></p><p><b><span style="font-size: 11pt;" data-mce-style="font-size: 11pt;"><strong><br></strong></span></b></p><p><b>[Ticket#{{ticket_id}}] {{ticket_subject}}<br><br>Click here to reply:&nbsp; {{link}}</b></p><p>----------------------------------------------</p><p>Here is the reply : <br></p><p> {{reply}}<br></p></div><div><br></div>', '{{subject}}\r\n\r\n{{reply}}\r\n\r\n\r\nClick here to reply:  {{link}}', '{"ticket_id":"Support Ticket ID", "ticket_subject":"Subject Of Support Ticket", "reply":"Reply from Staff/Admin","link":"Ticket URL For relpy"}', 1, 1, '2020-06-08 18:00:00', '2020-05-04 02:24:40'),
	(206, 'DEPOSIT_COMPLETE', 'Automated Deposit - Successful', 'Deposit Completed Successfully', '<div>Your deposit of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} </b>has been completed Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your Deposit :<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#000000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Paid via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size="5"><b><br></b></font></div><div><font size="5">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br></div>', '{{amount}} {{currrency}} Deposit successfully by {{gateway_name}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion", "post_balance":"Users Balance After this operation"}', 1, 1, '2020-06-24 18:00:00', '2020-11-17 03:10:00'),
	(207, 'DEPOSIT_REQUEST', 'Manual Deposit - User Requested', 'Deposit Request Submitted Successfully', '<div>Your deposit request of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} </b>submitted successfully<b> .<br></b></div><div><b><br></b></div><div><b>Details of your Deposit :<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#FF0000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Pay via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div><br></div>', '{{amount}} Deposit requested by {{method}}. Charge: {{charge}} . Trx: {{trx}}\r\n', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion"}', 1, 1, '2020-05-31 18:00:00', '2020-06-01 18:00:00'),
	(208, 'DEPOSIT_APPROVE', 'Manual Deposit - Admin Approved', 'Your Deposit is Approved', '<div>Your deposit request of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} </b>is Approved .<b><br></b></div><div><b><br></b></div><div><b>Details of your Deposit :<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#FF0000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Paid via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size="5"><b><br></b></font></div><div><font size="5">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br></div>', 'Admin Approve Your {{amount}} {{gateway_currency}} payment request by {{gateway_name}} transaction : {{transaction}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion", "post_balance":"Users Balance After this operation"}', 1, 1, '2020-06-16 18:00:00', '2020-06-14 18:00:00'),
	(209, 'DEPOSIT_REJECT', 'Manual Deposit - Admin Rejected', 'Your Deposit Request is Rejected', '<div>Your deposit request of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} has been rejected</b>.<b><br></b></div><br><div>Transaction Number was : {{trx}}</div><div><br></div><div>if you have any query, feel free to contact us.<br></div><br><div><br><br></div>\r\n\r\n\r\n\r\n{{rejection_message}}', 'Admin Rejected Your {{amount}} {{gateway_currency}} payment request by {{gateway_name}}\r\n\r\n{{rejection_message}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion","rejection_message":"Rejection message"}', 1, 1, '2020-06-09 18:00:00', '2020-06-14 18:00:00'),
	(210, 'WITHDRAW_REQUEST', 'Withdraw  - User Requested', 'Withdraw Request Submitted Successfully', '<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been submitted Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#FF0000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You will get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size="4" color="#FF0000"><b><br></b></font></div><div><font size="4" color="#FF0000"><b>This may take {{delay}} to process the payment.</b></font><br></div><div><font size="5"><b><br></b></font></div><div><font size="5"><b><br></b></font></div><div><font size="5">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br><br></div>', '{{amount}} {{currency}} withdraw requested by {{withdraw_method}}. You will get {{method_amount}} {{method_currency}} in {{duration}}. Trx: {{trx}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion", "post_balance":"Users Balance After this operation", "delay":"Delay time for processing"}', 1, 1, '2020-06-07 18:00:00', '2020-06-14 18:00:00'),
	(211, 'WITHDRAW_REJECT', 'Withdraw - Admin Rejected', 'Withdraw Request has been Rejected and your money is refunded to your account', '<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been Rejected.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#FF0000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You should get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div><br></div><div>----</div><div><font size="3"><br></font></div><div><font size="3"> {{amount}} {{currency}} has been <b>refunded </b>to your account and your current Balance is <b>{{post_balance}}</b><b> {{currency}}</b></font></div><div><br></div><div>-----</div><div><br></div><div><font size="4">Details of Rejection :</font></div><div><font size="4"><b>{{admin_details}}</b></font></div><div><br></div><div><br><br><br><br><br><br></div>', 'Admin Rejected Your {{amount}} {{currency}} withdraw request. Your Main Balance {{main_balance}}  {{method}} , Transaction {{transaction}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion", "post_balance":"Users Balance After this operation", "admin_details":"Details Provided By Admin"}', 1, 1, '2020-06-09 18:00:00', '2020-06-14 18:00:00'),
	(212, 'WITHDRAW_APPROVE', 'Withdraw - Admin  Approved', 'Withdraw Request has been Processed and your money is sent', '<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been Processed Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color="#FF0000">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You will get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div>-----</div><div><br></div><div><font size="4">Details of Processed Payment :</font></div><div><font size="4"><b>{{admin_details}}</b></font></div><div><br></div><div><br><br><br><br><br></div>', 'Admin Approve Your {{amount}} {{currency}} withdraw request by {{method}}. Transaction {{transaction}}', '{"trx":"Transaction Number","amount":"Request Amount By user","charge":"Gateway Charge","currency":"Site Currency","rate":"Conversion Rate","method_name":"Deposit Method Name","method_currency":"Deposit Method Currency","method_amount":"Deposit Method Amount After Conversion", "admin_details":"Details Provided By Admin"}', 1, 1, '2020-06-10 18:00:00', '2020-06-06 18:00:00'),
	(215, 'BAL_ADD', 'Balance Add by Admin', 'Your Account has been Credited', '<div>{{amount}} {{currency}} has been added to your account .</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div>Your Current Balance is : <font size="3"><b>{{post_balance}}&nbsp; {{currency}}&nbsp;</b></font>', '{{amount}} {{currency}} credited in your account. Your Current Balance {{remaining_balance}} {{currency}} . Transaction: #{{trx}}', '{"trx":"Transaction Number","amount":"Request Amount By Admin","currency":"Site Currency", "post_balance":"Users Balance After this operation"}', 1, 1, '2019-09-14 19:14:22', '2021-01-06 00:46:18'),
	(216, 'BAL_SUB', 'Balance Subtracted by Admin', 'Your Account has been Debited', '<div>{{amount}} {{currency}} has been subtracted from your account .</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div>Your Current Balance is : <font size="3"><b>{{post_balance}}&nbsp; {{currency}}</b></font>', '{{amount}} {{currency}} debited from your account. Your Current Balance {{remaining_balance}} {{currency}} . Transaction: #{{trx}}', '{"trx":"Transaction Number","amount":"Request Amount By Admin","currency":"Site Currency", "post_balance":"Users Balance After this operation"}', 1, 1, '2019-09-14 19:14:22', '2019-11-10 09:07:12'),
	(217, 'SURVEY_COMPLETED', 'Survey  - User completed', 'You have completed Survey Successfully', '<div>You have successfully completed a survey named {{survey_name}} .</div><div><b style="font-size: 1rem;"><br></b></div><div><span style="font-size: 1rem;">You got</span><b style="font-size: 1rem;"> {{amount}} {{currency}}</b><span style="color: rgb(33, 37, 41); font-size: 1rem;">&nbsp;&nbsp;</span>as reward<span style="color: rgb(33, 37, 41); font-size: 1rem;">.</span></div><div><font size="5"><b><br></b></font></div><div><font size="5">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br><br></div>', 'You have Successfully completed a survey named {{survey_name}}. You got {{amount}} {{currency}} as reward.', '{"survey_name":"Survey Name","amount":"Reward Amount","post_balance":"Current Balance","currency":"Site Currency"}', 1, 1, '2020-06-07 18:00:00', '2021-04-26 04:50:37'),
	(218, 'SURVEY_ANSWERD', 'Survey  - User answered', 'User has completed Survey Successfully', '<div>A user has successfully completed a survey named {{survey_name}}</div><div><br></div><div>&nbsp;There was total {{total_question}}</div><div><br></div><div>{{total_question}} * {{charge}} = {{amount}} {{currency}} subtracted from your balance.</div><div><span style="color: rgb(33, 37, 41);"><br></span></div><div><font size="5">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br><br></div>', 'A user has Successfully completed a survey named {{survey_name}}. {{amount}} {{currency}} subtracted from your balance as charge.', '{"survey_name":"Survey Name","amount":"Reward Amount","post_balance":"Current Balance","currency":"Site Currency","total_question":"Total answerd question","charge":"Charge per question"}', 1, 1, '2020-06-07 18:00:00', '2021-04-26 04:58:00');
/*!40000 ALTER TABLE `email_sms_templates` ENABLE KEYS */;

-- Dumping structure for table encuestas.extensions
CREATE TABLE IF NOT EXISTS `extensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `act` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `script` text COLLATE utf8mb4_unicode_ci,
  `shortcode` text COLLATE utf8mb4_unicode_ci COMMENT 'object',
  `support` text COLLATE utf8mb4_unicode_ci COMMENT 'help section',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.extensions: ~4 rows (approximately)
DELETE FROM `extensions`;
/*!40000 ALTER TABLE `extensions` DISABLE KEYS */;
INSERT INTO `extensions` (`id`, `act`, `name`, `description`, `image`, `script`, `shortcode`, `support`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'tawk-chat', 'Tawk.to', 'Key location is shown bellow', 'tawky_big.png', '<script>\r\n                        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n                        (function(){\r\n                        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];\r\n                        s1.async=true;\r\n                        s1.src="https://embed.tawk.to/{{app_key}}";\r\n                        s1.charset="UTF-8";\r\n                        s1.setAttribute("crossorigin","*");\r\n                        s0.parentNode.insertBefore(s1,s0);\r\n                        })();\r\n                    </script>', '{"app_key":{"title":"App Key","value":"58dd135ef770\\/default"}}', 'twak.png', 0, NULL, '2019-10-18 23:16:05', '2021-04-26 03:33:20'),
	(2, 'google-recaptcha2', 'Google Recaptcha 2', 'Key location is shown bellow', 'recaptcha3.png', '\r\n<script src="https://www.google.com/recaptcha/api.js"></script>\r\n<div class="g-recaptcha" data-sitekey="{{sitekey}}" data-callback="verifyCaptcha"></div>\r\n<div id="g-recaptcha-error"></div>', '{"sitekey":{"title":"Site Key","value":"6Lfpm3cUAAAAAGIjbEJKhJNKS4X1Gns9ANjh8MfH"}}', 'recaptcha.png', 0, NULL, '2019-10-18 23:16:05', '2021-04-26 00:39:54'),
	(3, 'custom-captcha', 'Custom Captcha', 'Just Put Any Random String', 'customcaptcha.png', NULL, '{"random_key":{"title":"Random String","value":"SecureString"}}', 'na', 0, NULL, '2019-10-18 23:16:05', '2021-05-23 07:32:43'),
	(4, 'google-analytics', 'Google Analytics', 'Key location is shown bellow', 'google-analytics.png', '<script async src="https://www.googletagmanager.com/gtag/js?id={{app_key}}"></script>\r\n                <script>\r\n                  window.dataLayer = window.dataLayer || [];\r\n                  function gtag(){dataLayer.push(arguments);}\r\n                  gtag("js", new Date());\r\n                \r\n                  gtag("config", "{{app_key}}");\r\n                </script>', '{"app_key":{"title":"App Key","value":"Demo"}}', 'ganalytics.png', 0, NULL, NULL, '2021-04-26 03:33:23');
/*!40000 ALTER TABLE `extensions` ENABLE KEYS */;

-- Dumping structure for table encuestas.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table encuestas.frontends
CREATE TABLE IF NOT EXISTS `frontends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_keys` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_values` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.frontends: ~46 rows (approximately)
DELETE FROM `frontends`;
/*!40000 ALTER TABLE `frontends` DISABLE KEYS */;
INSERT INTO `frontends` (`id`, `data_keys`, `data_values`, `created_at`, `updated_at`) VALUES
	(1, 'seo.data', '{"seo_image":"1","keywords":["survey","survlab","viserlab"],"description":"Join with us and get paid to speak your mind. The more surveys you take, the more money you earn. Join today","social_title":"Viserlab Limited","social_description":"Join with us and get paid to speak your mind. The more surveys you take, the more money you earn. Join today","image":"6085148aea7161619334282.png"}', '2020-07-04 23:42:52', '2021-04-27 01:15:20'),
	(25, 'blog.content', '{"heading":"Latest News","sub_heading":"Hic tenetur nihil ex. Doloremque ipsa velit, ea molestias expedita sed voluptatem ex voluptatibus temporibus sequi. sddd"}', '2020-10-28 00:51:34', '2020-10-28 00:52:52'),
	(26, 'blog.element', '{"has_image":["1","1"],"title":"this is a test blog 2","description":"aewf asdf","description_nic":"asdf asdf","blog_icon":"<i class=\\"lab la-hornbill\\"><\\/i>","blog_image_1":"5f99164f1baec1603868239.jpg","blog_image_2":"5ff2e146346d21609752902.jpg"}', '2020-10-28 00:57:19', '2021-01-04 03:35:02'),
	(28, 'counter.content', '{"heading":"Latest News","sub_heading":"Register New Account"}', '2020-10-28 01:04:02', '2020-10-28 01:04:02'),
	(30, 'blog.element', '{"has_image":["1","1"],"title":"This is test blog 1","description":"asdfasdf ffffffffff","description_nic":"asdfasdf asdd vvvvvvvvvvvvvvvvvv","blog_icon":"<i class=\\"las la-highlighter\\"><\\/i>","blog_image_1":"5f9d0689e022d1604126345.jpg","blog_image_2":"5f9d068a341211604126346.jpg"}', '2020-10-31 00:39:05', '2020-11-12 04:36:39'),
	(31, 'social_icon.element', '{"title":"Facebook","social_icon":"<i class=\\"lab la-facebook-f\\"><\\/i>","url":"https:\\/\\/www.google.com\\/"}', '2020-11-12 04:07:30', '2021-03-28 01:27:02'),
	(33, 'feature.content', '{"heading":"asdf","sub_heading":"asdf"}', '2021-01-03 23:40:54', '2021-01-03 23:40:55'),
	(34, 'feature.element', '{"title":"asdf","description":"asdf","feature_icon":"asdf"}', '2021-01-03 23:41:02', '2021-01-03 23:41:02'),
	(35, 'service.element', '{"has_image":"1","title":"High Quality","image":"6060172e78b041616910126.png"}', '2021-03-06 01:12:10', '2021-03-27 23:42:06'),
	(39, 'banner.content', '{"title":" ","heading":"Generador de Formularios","button_name":"Empezar","button_url":"#"}', '2021-03-27 07:53:26', '2023-08-02 18:25:54'),
	(40, 'service.element', '{"has_image":"1","title":"100% Confidentiality","image":"6060174bee17d1616910155.png"}', '2021-03-27 23:42:35', '2021-03-27 23:42:35'),
	(41, 'service.element', '{"has_image":"1","title":"24\\/7 Support","image":"6060175c322101616910172.png"}', '2021-03-27 23:42:52', '2021-03-27 23:42:52'),
	(44, 'counter.element', '{"counter_digit":"2781","sub_title":"Total Survey"}', '2021-03-27 23:47:37', '2021-03-27 23:47:37'),
	(45, 'team.content', '{"heading":"Our Survey Team"}', '2021-03-27 23:54:49', '2021-03-27 23:54:49'),
	(54, 'brand.element', '{"has_image":"1","name":"Max Paper","image":"6060242aa43441616913450.png"}', '2021-03-28 00:37:30', '2021-03-28 00:37:30'),
	(55, 'brand.element', '{"has_image":"1","name":"Crypto","image":"606024466292f1616913478.png"}', '2021-03-28 00:37:58', '2021-03-28 00:37:58'),
	(56, 'brand.element', '{"has_image":"1","name":"Dynamic","image":"6060245236a821616913490.png"}', '2021-03-28 00:38:10', '2021-03-28 00:38:10'),
	(57, 'brand.element', '{"has_image":"1","name":"Deters","image":"6060245fe61421616913503.png"}', '2021-03-28 00:38:23', '2021-03-28 00:38:23'),
	(58, 'brand.element', '{"has_image":"1","name":"Lablatory","image":"60602478d8daa1616913528.png"}', '2021-03-28 00:38:48', '2021-03-28 00:38:48'),
	(60, 'contact_us.element', '{"title":"Direccion","description":"Lapaz - bolivia","icon":"<i class=\\"fas fa-map-marker-alt\\"><\\/i>"}', '2021-03-28 03:22:04', '2023-08-02 19:11:04'),
	(61, 'contact_us.element', '{"title":"Correo Electronico","description":"demo@demo.com","icon":"<i class=\\"fas fa-envelope\\"><\\/i>"}', '2021-03-28 03:23:02', '2023-08-02 19:11:24'),
	(62, 'contact_us.element', '{"title":"Telefono","description":"(591) 67470820","icon":"<i class=\\"fas fa-phone-alt\\"><\\/i>"}', '2021-03-28 03:23:57', '2023-08-02 19:11:59'),
	(63, 'social_icon.element', '{"title":"Twitter","social_icon":"<i class=\\"fab fa-twitter\\"><\\/i>","url":"https:\\/\\/www.google.com\\/"}', '2021-03-28 03:40:28', '2021-03-28 03:40:28'),
	(64, 'social_icon.element', '{"title":"Instagram","social_icon":"<i class=\\"fab fa-instagram\\"><\\/i>","url":"https:\\/\\/www.google.com\\/"}', '2021-03-28 03:40:46', '2021-03-28 03:40:46'),
	(65, 'social_icon.element', '{"title":"Youtube","social_icon":"<i class=\\"fab fa-youtube\\"><\\/i>","url":"https:\\/\\/www.google.com\\/"}', '2021-03-28 03:41:04', '2021-03-28 03:41:04'),
	(66, 'social_icon.element', '{"title":"Linkedin","social_icon":"<i class=\\"fab fa-linkedin\\"><\\/i>","url":"https:\\/\\/www.google.com\\/"}', '2021-03-28 03:41:40', '2021-03-28 03:41:40'),
	(67, 'notice.content', '{"description":"Get survey responses from around the world in minutes with Audience Target the types of people you want to hear from based on specific attributes, like country, gender, age, income, employment status, and more. A survey panel owned exclusively by members, empowering you to exchange your data and opinions for company shares. Get survey responses from around the world in minutes with Audience Target the types of people you want to hear from based on specific attributes, like country, gender, age, income, employment status, and more. A survey panel owned exclusively by members, empowering you to exchange your data and opinions for company shares."}', '2021-03-31 00:08:22', '2021-04-27 01:37:20');
/*!40000 ALTER TABLE `frontends` ENABLE KEYS */;

-- Dumping structure for table encuestas.gateways
CREATE TABLE IF NOT EXISTS `gateways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NULL',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `supported_currencies` text COLLATE utf8mb4_unicode_ci,
  `crypto` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: fiat currency, 1: crypto currency',
  `extra` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `input_form` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.gateways: ~22 rows (approximately)
DELETE FROM `gateways`;
/*!40000 ALTER TABLE `gateways` DISABLE KEYS */;
INSERT INTO `gateways` (`id`, `code`, `alias`, `image`, `name`, `status`, `parameters`, `supported_currencies`, `crypto`, `extra`, `description`, `input_form`, `created_at`, `updated_at`) VALUES
	(1, 101, 'paypal', '5f6f1bd8678601601117144.jpg', 'Paypal', 1, '{"paypal_email":{"title":"PayPal Email","global":true,"value":"sb-zlbi7986064@personal.example.com"}}', '{"AUD":"AUD","BRL":"BRL","CAD":"CAD","CZK":"CZK","DKK":"DKK","EUR":"EUR","HKD":"HKD","HUF":"HUF","INR":"INR","ILS":"ILS","JPY":"JPY","MYR":"MYR","MXN":"MXN","TWD":"TWD","NZD":"NZD","NOK":"NOK","PHP":"PHP","PLN":"PLN","GBP":"GBP","RUB":"RUB","SGD":"SGD","SEK":"SEK","CHF":"CHF","THB":"THB","USD":"$"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2021-01-17 03:02:44'),
	(2, 102, 'perfect_money', '5f6f1d2a742211601117482.jpg', 'Perfect Money', 1, '{"passphrase":{"title":"ALTERNATE PASSPHRASE","global":true,"value":"6451561651551"},"wallet_id":{"title":"PM Wallet","global":false,"value":""}}', '{"USD":"$","EUR":"\\u20ac"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:26:46'),
	(3, 103, 'stripe', '5f6f1d4bc69e71601117515.jpg', 'Stripe Hosted', 1, '{"secret_key":{"title":"Secret Key","global":true,"value":"sk_test_51HuxFUHyGzEKoTKAfIosswAQduMOGU4q4elcNr8OE6LoBZcp2MHKalOW835wjRiF6fxVTc7RmBgatKfAt1Qq0heb00rUaCOd2T"},"publishable_key":{"title":"PUBLISHABLE KEY","global":true,"value":"pk_test_51HuxFUHyGzEKoTKAueAuF9BrMDA5boMcpJLLt0vu4q3QdPX5isaGudKNe6OyVjZP1UugpYd6JA7i7TczRWsbutaP004YmBiSp5"}}', '{"USD":"USD","AUD":"AUD","BRL":"BRL","CAD":"CAD","CHF":"CHF","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","INR":"INR","JPY":"JPY","MXN":"MXN","MYR":"MYR","NOK":"NOK","NZD":"NZD","PLN":"PLN","SEK":"SEK","SGD":"SGD"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:26:49'),
	(4, 104, 'skrill', '5f6f1d41257181601117505.jpg', 'Skrill', 1, '{"pay_to_email":{"title":"Skrill Email","global":true,"value":"merchant@skrill.com"},"secret_key":{"title":"Secret Key","global":true,"value":"---"}}', '{"AED":"AED","AUD":"AUD","BGN":"BGN","BHD":"BHD","CAD":"CAD","CHF":"CHF","CZK":"CZK","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","HRK":"HRK","HUF":"HUF","ILS":"ILS","INR":"INR","ISK":"ISK","JOD":"JOD","JPY":"JPY","KRW":"KRW","KWD":"KWD","MAD":"MAD","MYR":"MYR","NOK":"NOK","NZD":"NZD","OMR":"OMR","PLN":"PLN","QAR":"QAR","RON":"RON","RSD":"RSD","SAR":"SAR","SEK":"SEK","SGD":"SGD","THB":"THB","TND":"TND","TRY":"TRY","TWD":"TWD","USD":"USD","ZAR":"ZAR","COP":"COP"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:26:52'),
	(5, 105, 'paytm', '5f6f1d1d3ec731601117469.jpg', 'PayTM', 1, '{"MID":{"title":"Merchant ID","global":true,"value":"DIY12386817555501617"},"merchant_key":{"title":"Merchant Key","global":true,"value":"bKMfNxPPf_QdZppa"},"WEBSITE":{"title":"Paytm Website","global":true,"value":"DIYtestingweb"},"INDUSTRY_TYPE_ID":{"title":"Industry Type","global":true,"value":"Retail"},"CHANNEL_ID":{"title":"CHANNEL ID","global":true,"value":"WEB"},"transaction_url":{"title":"Transaction URL","global":true,"value":"https:\\/\\/pguat.paytm.com\\/oltp-web\\/processTransaction"},"transaction_status_url":{"title":"Transaction STATUS URL","global":true,"value":"https:\\/\\/pguat.paytm.com\\/paytmchecksum\\/paytmCallback.jsp"}}', '{"AUD":"AUD","ARS":"ARS","BDT":"BDT","BRL":"BRL","BGN":"BGN","CAD":"CAD","CLP":"CLP","CNY":"CNY","COP":"COP","HRK":"HRK","CZK":"CZK","DKK":"DKK","EGP":"EGP","EUR":"EUR","GEL":"GEL","GHS":"GHS","HKD":"HKD","HUF":"HUF","INR":"INR","IDR":"IDR","ILS":"ILS","JPY":"JPY","KES":"KES","MYR":"MYR","MXN":"MXN","MAD":"MAD","NPR":"NPR","NZD":"NZD","NGN":"NGN","NOK":"NOK","PKR":"PKR","PEN":"PEN","PHP":"PHP","PLN":"PLN","RON":"RON","RUB":"RUB","SGD":"SGD","ZAR":"ZAR","KRW":"KRW","LKR":"LKR","SEK":"SEK","CHF":"CHF","THB":"THB","TRY":"TRY","UGX":"UGX","UAH":"UAH","AED":"AED","GBP":"GBP","USD":"USD","VND":"VND","XOF":"XOF"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:26:54'),
	(6, 106, 'payeer', '5f6f1bc61518b1601117126.jpg', 'Payeer', 1, '{"merchant_id":{"title":"Merchant ID","global":true,"value":"866989763"},"secret_key":{"title":"Secret key","global":true,"value":"7575"}}', '{"USD":"USD","EUR":"EUR","RUB":"RUB"}', 0, '{"status":{"title": "Status URL","value":"ipn.payeer"}}', NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:26:58'),
	(7, 107, 'paystack', '5f7096563dfb71601214038.jpg', 'PayStack', 1, '{"public_key":{"title":"Public key","global":true,"value":"pk_test_3c9c87f51b13c15d99eb367ca6ebc52cc9eb1f33"},"secret_key":{"title":"Secret key","global":true,"value":"sk_test_2a3f97a146ab5694801f993b60fcb81cd7254f12"}}', '{"USD":"USD","NGN":"NGN"}', 0, '{"callback":{"title": "Callback URL","value":"ipn.paystack"},"webhook":{"title": "Webhook URL","value":"ipn.paystack"}}\r\n', NULL, NULL, '2019-09-14 13:14:22', '2020-12-28 01:25:38'),
	(8, 108, 'voguepay', '5f6f1d5951a111601117529.jpg', 'VoguePay', 1, '{"merchant_id":{"title":"MERCHANT ID","global":true,"value":"demo"}}', '{"USD":"USD","GBP":"GBP","EUR":"EUR","GHS":"GHS","NGN":"NGN","ZAR":"ZAR"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:52:09'),
	(9, 109, 'flutterwave', '5f6f1b9e4bb961601117086.jpg', 'Flutterwave', 1, '{"public_key":{"title":"Public Key","global":true,"value":"demo_publisher_key"},"secret_key":{"title":"Secret Key","global":true,"value":"demo_secret_key"},"encryption_key":{"title":"Encryption Key","global":true,"value":"demo_encryption_key"}}', '{"BIF":"BIF","CAD":"CAD","CDF":"CDF","CVE":"CVE","EUR":"EUR","GBP":"GBP","GHS":"GHS","GMD":"GMD","GNF":"GNF","KES":"KES","LRD":"LRD","MWK":"MWK","MZN":"MZN","NGN":"NGN","RWF":"RWF","SLL":"SLL","STD":"STD","TZS":"TZS","UGX":"UGX","USD":"USD","XAF":"XAF","XOF":"XOF","ZMK":"ZMK","ZMW":"ZMW","ZWD":"ZWD"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2021-01-04 03:29:50'),
	(10, 110, 'razorpay', '5f6f1d3672dd61601117494.jpg', 'RazorPay', 1, '{"key_id":{"title":"Key Id","global":true,"value":"rzp_test_kiOtejPbRZU90E"},"key_secret":{"title":"Key Secret ","global":true,"value":"osRDebzEqbsE1kbyQJ4y0re7"}}', '{"INR":"INR"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:51:34'),
	(11, 111, 'stripe_js', '5f7096a31ed9a1601214115.jpg', 'Stripe Storefront', 1, '{"secret_key":{"title":"Secret Key","global":true,"value":"sk_test_51HuxFUHyGzEKoTKAfIosswAQduMOGU4q4elcNr8OE6LoBZcp2MHKalOW835wjRiF6fxVTc7RmBgatKfAt1Qq0heb00rUaCOd2T"},"publishable_key":{"title":"PUBLISHABLE KEY","global":true,"value":"pk_test_51HuxFUHyGzEKoTKAueAuF9BrMDA5boMcpJLLt0vu4q3QdPX5isaGudKNe6OyVjZP1UugpYd6JA7i7TczRWsbutaP004YmBiSp5"}}', '{"USD":"USD","AUD":"AUD","BRL":"BRL","CAD":"CAD","CHF":"CHF","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","INR":"INR","JPY":"JPY","MXN":"MXN","MYR":"MYR","NOK":"NOK","NZD":"NZD","PLN":"PLN","SEK":"SEK","SGD":"SGD"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-12-05 03:56:20'),
	(12, 112, 'instamojo', '5f6f1babbdbb31601117099.jpg', 'Instamojo', 1, '{"api_key":{"title":"API KEY","global":true,"value":"test_2241633c3bc44a3de84a3b33969"},"auth_token":{"title":"Auth Token","global":true,"value":"test_279f083f7bebefd35217feef22d"},"salt":{"title":"Salt","global":true,"value":"19d38908eeff4f58b2ddda2c6d86ca25"}}', '{"INR":"INR"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:44:59'),
	(13, 501, 'blockchain', '5f6f1b2b20c6f1601116971.jpg', 'Blockchain', 1, '{"api_key":{"title":"API Key","global":true,"value":"55529946-05ca-48ff-8710-f279d86b1cc5"},"xpub_code":{"title":"XPUB CODE","global":true,"value":"xpub6CKQ3xxWyBoFAF83izZCSFUorptEU9AF8TezhtWeMU5oefjX3sFSBw62Lr9iHXPkXmDQJJiHZeTRtD9Vzt8grAYRhvbz4nEvBu3QKELVzFK"}}', '{"BTC":"BTC"}', 1, NULL, NULL, NULL, '2019-09-14 13:14:22', '2021-01-31 06:55:45'),
	(14, 502, 'blockio', '5f6f19432bedf1601116483.jpg', 'Block.io', 1, '{"api_key":{"title":"API Key","global":false,"value":"1658-8015-2e5e-9afb"},"api_pin":{"title":"API PIN","global":true,"value":"covidvai2020"}}', '{"BTC":"BTC","LTC":"LTC","DOGE":"DOGE"}', 1, '{"cron":{"title": "Cron URL","value":"ipn.blockio"}}', NULL, NULL, '2019-09-14 13:14:22', '2021-01-03 23:19:59'),
	(15, 503, 'coinpayments', '5f6f1b6c02ecd1601117036.jpg', 'CoinPayments', 1, '{"public_key":{"title":"Public Key","global":true,"value":"7638eebaf4061b7f7cdfceb14046318bbdabf7e2f64944773d6550bd59f70274"},"private_key":{"title":"Private Key","global":true,"value":"Cb6dee7af8Eb9E0D4123543E690dA3673294147A5Dc8e7a621B5d484a3803207"},"merchant_id":{"title":"Merchant ID","global":true,"value":"93a1e014c4ad60a7980b4a7239673cb4"}}', '{"BTC":"Bitcoin","BTC.LN":"Bitcoin (Lightning Network)","LTC":"Litecoin","CPS":"CPS Coin","VLX":"Velas","APL":"Apollo","AYA":"Aryacoin","BAD":"Badcoin","BCD":"Bitcoin Diamond","BCH":"Bitcoin Cash","BCN":"Bytecoin","BEAM":"BEAM","BITB":"Bean Cash","BLK":"BlackCoin","BSV":"Bitcoin SV","BTAD":"Bitcoin Adult","BTG":"Bitcoin Gold","BTT":"BitTorrent","CLOAK":"CloakCoin","CLUB":"ClubCoin","CRW":"Crown","CRYP":"CrypticCoin","CRYT":"CryTrExCoin","CURE":"CureCoin","DASH":"DASH","DCR":"Decred","DEV":"DeviantCoin","DGB":"DigiByte","DOGE":"Dogecoin","EBST":"eBoost","EOS":"EOS","ETC":"Ether Classic","ETH":"Ethereum","ETN":"Electroneum","EUNO":"EUNO","EXP":"EXP","Expanse":"Expanse","FLASH":"FLASH","GAME":"GameCredits","GLC":"Goldcoin","GRS":"Groestlcoin","KMD":"Komodo","LOKI":"LOKI","LSK":"LSK","MAID":"MaidSafeCoin","MUE":"MonetaryUnit","NAV":"NAV Coin","NEO":"NEO","NMC":"Namecoin","NVST":"NVO Token","NXT":"NXT","OMNI":"OMNI","PINK":"PinkCoin","PIVX":"PIVX","POT":"PotCoin","PPC":"Peercoin","PROC":"ProCurrency","PURA":"PURA","QTUM":"QTUM","RES":"Resistance","RVN":"Ravencoin","RVR":"RevolutionVR","SBD":"Steem Dollars","SMART":"SmartCash","SOXAX":"SOXAX","STEEM":"STEEM","STRAT":"STRAT","SYS":"Syscoin","TPAY":"TokenPay","TRIGGERS":"Triggers","TRX":" TRON","UBQ":"Ubiq","UNIT":"UniversalCurrency","USDT":"Tether USD (Omni Layer)","VTC":"Vertcoin","WAVES":"Waves","XCP":"Counterparty","XEM":"NEM","XMR":"Monero","XSN":"Stakenet","XSR":"SucreCoin","XVG":"VERGE","XZC":"ZCoin","ZEC":"ZCash","ZEN":"Horizen"}', 1, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:43:56'),
	(16, 504, 'coinpayments_fiat', '5f6f1b94e9b2b1601117076.jpg', 'CoinPayments Fiat', 1, '{"merchant_id":{"title":"Merchant ID","global":true,"value":"93a1e014c4ad60a7980b4a7239673cb4"}}', '{"USD":"USD","AUD":"AUD","BRL":"BRL","CAD":"CAD","CHF":"CHF","CLP":"CLP","CNY":"CNY","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","INR":"INR","ISK":"ISK","JPY":"JPY","KRW":"KRW","NZD":"NZD","PLN":"PLN","RUB":"RUB","SEK":"SEK","SGD":"SGD","THB":"THB","TWD":"TWD"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-10-22 03:17:29'),
	(17, 505, 'coingate', '5f6f1b5fe18ee1601117023.jpg', 'Coingate', 1, '{"api_key":{"title":"API Key","global":true,"value":"Ba1VgPx6d437xLXGKCBkmwVCEw5kHzRJ6thbGo-N"}}', '{"USD":"USD","EUR":"EUR"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:43:44'),
	(18, 506, 'coinbase_commerce', '5f6f1b4c774af1601117004.jpg', 'Coinbase Commerce', 1, '{"api_key":{"title":"API Key","global":true,"value":"c47cd7df-d8e8-424b-a20a"},"secret":{"title":"Webhook Shared Secret","global":true,"value":"55871878-2c32-4f64-ab66"}}', '{"USD":"USD","EUR":"EUR","JPY":"JPY","GBP":"GBP","AUD":"AUD","CAD":"CAD","CHF":"CHF","CNY":"CNY","SEK":"SEK","NZD":"NZD","MXN":"MXN","SGD":"SGD","HKD":"HKD","NOK":"NOK","KRW":"KRW","TRY":"TRY","RUB":"RUB","INR":"INR","BRL":"BRL","ZAR":"ZAR","AED":"AED","AFN":"AFN","ALL":"ALL","AMD":"AMD","ANG":"ANG","AOA":"AOA","ARS":"ARS","AWG":"AWG","AZN":"AZN","BAM":"BAM","BBD":"BBD","BDT":"BDT","BGN":"BGN","BHD":"BHD","BIF":"BIF","BMD":"BMD","BND":"BND","BOB":"BOB","BSD":"BSD","BTN":"BTN","BWP":"BWP","BYN":"BYN","BZD":"BZD","CDF":"CDF","CLF":"CLF","CLP":"CLP","COP":"COP","CRC":"CRC","CUC":"CUC","CUP":"CUP","CVE":"CVE","CZK":"CZK","DJF":"DJF","DKK":"DKK","DOP":"DOP","DZD":"DZD","EGP":"EGP","ERN":"ERN","ETB":"ETB","FJD":"FJD","FKP":"FKP","GEL":"GEL","GGP":"GGP","GHS":"GHS","GIP":"GIP","GMD":"GMD","GNF":"GNF","GTQ":"GTQ","GYD":"GYD","HNL":"HNL","HRK":"HRK","HTG":"HTG","HUF":"HUF","IDR":"IDR","ILS":"ILS","IMP":"IMP","IQD":"IQD","IRR":"IRR","ISK":"ISK","JEP":"JEP","JMD":"JMD","JOD":"JOD","KES":"KES","KGS":"KGS","KHR":"KHR","KMF":"KMF","KPW":"KPW","KWD":"KWD","KYD":"KYD","KZT":"KZT","LAK":"LAK","LBP":"LBP","LKR":"LKR","LRD":"LRD","LSL":"LSL","LYD":"LYD","MAD":"MAD","MDL":"MDL","MGA":"MGA","MKD":"MKD","MMK":"MMK","MNT":"MNT","MOP":"MOP","MRO":"MRO","MUR":"MUR","MVR":"MVR","MWK":"MWK","MYR":"MYR","MZN":"MZN","NAD":"NAD","NGN":"NGN","NIO":"NIO","NPR":"NPR","OMR":"OMR","PAB":"PAB","PEN":"PEN","PGK":"PGK","PHP":"PHP","PKR":"PKR","PLN":"PLN","PYG":"PYG","QAR":"QAR","RON":"RON","RSD":"RSD","RWF":"RWF","SAR":"SAR","SBD":"SBD","SCR":"SCR","SDG":"SDG","SHP":"SHP","SLL":"SLL","SOS":"SOS","SRD":"SRD","SSP":"SSP","STD":"STD","SVC":"SVC","SYP":"SYP","SZL":"SZL","THB":"THB","TJS":"TJS","TMT":"TMT","TND":"TND","TOP":"TOP","TTD":"TTD","TWD":"TWD","TZS":"TZS","UAH":"UAH","UGX":"UGX","UYU":"UYU","UZS":"UZS","VEF":"VEF","VND":"VND","VUV":"VUV","WST":"WST","XAF":"XAF","XAG":"XAG","XAU":"XAU","XCD":"XCD","XDR":"XDR","XOF":"XOF","XPD":"XPD","XPF":"XPF","XPT":"XPT","YER":"YER","ZMW":"ZMW","ZWL":"ZWL"}\r\n\r\n', 0, '{"endpoint":{"title": "Webhook Endpoint","value":"ipn.coinbase_commerce"}}', NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:43:24'),
	(24, 113, 'paypal_sdk', '5f6f1bec255c61601117164.jpg', 'Paypal Express', 1, '{"clientId":{"title":"Paypal Client ID","global":true,"value":"Ae0-tixtSV7DvLwIh3Bmu7JvHrjh5EfGdXr_cEklKAVjjezRZ747BxKILiBdzlKKyp-W8W_T7CKH1Ken"},"clientSecret":{"title":"Client Secret","global":true,"value":"EOhbvHZgFNO21soQJT1L9Q00M3rK6PIEsdiTgXRBt2gtGtxwRer5JvKnVUGNU5oE63fFnjnYY7hq3HBA"}}', '{"AUD":"AUD","BRL":"BRL","CAD":"CAD","CZK":"CZK","DKK":"DKK","EUR":"EUR","HKD":"HKD","HUF":"HUF","INR":"INR","ILS":"ILS","JPY":"JPY","MYR":"MYR","MXN":"MXN","TWD":"TWD","NZD":"NZD","NOK":"NOK","PHP":"PHP","PLN":"PLN","GBP":"GBP","RUB":"RUB","SGD":"SGD","SEK":"SEK","CHF":"CHF","THB":"THB","USD":"$"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-10-31 23:50:27'),
	(25, 114, 'stripe_v3', '5f709684736321601214084.jpg', 'Stripe Checkout', 1, '{"secret_key":{"title":"Secret Key","global":true,"value":"sk_test_51HuxFUHyGzEKoTKAfIosswAQduMOGU4q4elcNr8OE6LoBZcp2MHKalOW835wjRiF6fxVTc7RmBgatKfAt1Qq0heb00rUaCOd2T"},"publishable_key":{"title":"PUBLISHABLE KEY","global":true,"value":"pk_test_51HuxFUHyGzEKoTKAueAuF9BrMDA5boMcpJLLt0vu4q3QdPX5isaGudKNe6OyVjZP1UugpYd6JA7i7TczRWsbutaP004YmBiSp5"},"end_point":{"title":"End Point Secret","global":true,"value":"w5555"}}', '{"USD":"USD","AUD":"AUD","BRL":"BRL","CAD":"CAD","CHF":"CHF","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","INR":"INR","JPY":"JPY","MXN":"MXN","MYR":"MYR","NOK":"NOK","NZD":"NZD","PLN":"PLN","SEK":"SEK","SGD":"SGD"}', 0, '{"webhook":{"title": "Webhook Endpoint","value":"ipn.stripe_v3"}}', NULL, NULL, '2019-09-14 13:14:22', '2020-12-05 03:56:14'),
	(27, 115, 'mollie', '5f6f1bb765ab11601117111.jpg', 'Mollie', 1, '{"mollie_email":{"title":"Mollie Email ","global":true,"value":"admin@gmail.com"},"api_key":{"title":"API KEY","global":true,"value":"test_cucfwKTWfft9s337qsVfn5CC4vNkrn"}}', '{"AED":"AED","AUD":"AUD","BGN":"BGN","BRL":"BRL","CAD":"CAD","CHF":"CHF","CZK":"CZK","DKK":"DKK","EUR":"EUR","GBP":"GBP","HKD":"HKD","HRK":"HRK","HUF":"HUF","ILS":"ILS","ISK":"ISK","JPY":"JPY","MXN":"MXN","MYR":"MYR","NOK":"NOK","NZD":"NZD","PHP":"PHP","PLN":"PLN","RON":"RON","RUB":"RUB","SEK":"SEK","SGD":"SGD","THB":"THB","TWD":"TWD","USD":"USD","ZAR":"ZAR"}', 0, NULL, NULL, NULL, '2019-09-14 13:14:22', '2020-09-26 04:45:11'),
	(30, 116, 'cashmaal', '5f9a8b62bb4dd1603963746.png', 'Cashmaal', 1, '{"web_id":{"title":"Web Id","global":true,"value":"3748"},"ipn_key":{"title":"IPN Key","global":true,"value":"546254628759524554647987"}}', '{"PKR":"PKR","USD":"USD"}', 0, '{"webhook":{"title": "IPN URL","value":"ipn.cashmaal"}}', NULL, NULL, NULL, '2020-10-29 03:29:06');
/*!40000 ALTER TABLE `gateways` ENABLE KEYS */;

-- Dumping structure for table encuestas.gateway_currencies
CREATE TABLE IF NOT EXISTS `gateway_currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_code` int(11) DEFAULT NULL,
  `gateway_alias` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_amount` decimal(18,8) NOT NULL,
  `max_amount` decimal(18,8) NOT NULL,
  `percent_charge` decimal(5,2) NOT NULL DEFAULT '0.00',
  `fixed_charge` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `rate` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_parameter` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.gateway_currencies: ~0 rows (approximately)
DELETE FROM `gateway_currencies`;
/*!40000 ALTER TABLE `gateway_currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `gateway_currencies` ENABLE KEYS */;

-- Dumping structure for table encuestas.general_settings
CREATE TABLE IF NOT EXISTS `general_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sitename` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_text` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'currency text',
  `cur_sym` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'currency symbol',
  `email_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_template` text COLLATE utf8mb4_unicode_ci,
  `sms_api` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondary_color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_config` text COLLATE utf8mb4_unicode_ci COMMENT 'email configuration',
  `ev` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'email verification, 0 - dont check, 1 - check',
  `en` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'email notification, 0 - dont send, 1 - send',
  `sv` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'sms verication, 0 - dont check, 1 - check',
  `sn` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'sms notification, 0 - dont send, 1 - send',
  `registration` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Off	, 1: On',
  `surveyor_registration` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Off , 1: On',
  `survey_approval` tinyint(1) NOT NULL DEFAULT '1',
  `social_login` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'social login',
  `social_credential` text COLLATE utf8mb4_unicode_ci,
  `active_template` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sys_version` text COLLATE utf8mb4_unicode_ci,
  `get_amount` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `paid_amount` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.general_settings: ~0 rows (approximately)
DELETE FROM `general_settings`;
/*!40000 ALTER TABLE `general_settings` DISABLE KEYS */;
INSERT INTO `general_settings` (`id`, `sitename`, `cur_text`, `cur_sym`, `email_from`, `email_template`, `sms_api`, `base_color`, `secondary_color`, `mail_config`, `ev`, `en`, `sv`, `sn`, `registration`, `surveyor_registration`, `survey_approval`, `social_login`, `social_credential`, `active_template`, `sys_version`, `get_amount`, `paid_amount`, `created_at`, `updated_at`) VALUES
	(1, 'Generador de formularios', 'USD', '$', 'demo@demo.com', '<table style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(0, 23, 54); text-decoration-style: initial; text-decoration-color: initial;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#001736"><tbody><tr><td valign="top" align="center"><table class="mobile-shell" width="650" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="td container" style="width: 650px; min-width: 650px; font-size: 0pt; line-height: 0pt; margin: 0px; font-weight: normal; padding: 55px 0px;"><div style="text-align: center;"><img src="https://i.imgur.com/C9IS7Z1.png" style="height: 240 !important;width: 338px;margin-bottom: 20px;"></div><table style="width: 650px; margin: 0px auto;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="padding-bottom: 10px;"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="tbrr p30-15" style="padding: 60px 30px; border-radius: 26px 26px 0px 0px;" bgcolor="#000036"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="color: rgb(255, 255, 255); font-family: Muli, Arial, sans-serif; font-size: 20px; line-height: 46px; padding-bottom: 25px; font-weight: bold;">Hi {{name}} ,</td></tr><tr><td style="color: rgb(193, 205, 220); font-family: Muli, Arial, sans-serif; font-size: 20px; line-height: 30px; padding-bottom: 25px;">{{message}}</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table style="width: 650px; margin: 0px auto;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="p30-15 bbrr" style="padding: 50px 30px; border-radius: 0px 0px 26px 26px;" bgcolor="#000036"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="text-footer1 pb10" style="color: rgb(0, 153, 255); font-family: Muli, Arial, sans-serif; font-size: 18px; line-height: 30px; text-align: center; padding-bottom: 10px;">© 2023  Todos los derechos reservados. .</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>', 'https://api.infobip.com/api/v3/sendsms/plain?user=viserlab&password=26289099&sender=ViserLab&SMSText={{message}}&GSM={{number}}&type=longSMS', '000080', 'FFCC00', '{"name":"php"}', 0, 1, 0, 1, 1, 1, 0, 0, '{"google_client_id":"53929591142-l40gafo7efd9onfe6tj545sf9g7tv15t.apps.googleusercontent.com","google_client_secret":"BRdB3np2IgYLiy4-bwMcmOwN","fb_client_id":"277229062999748","fb_client_secret":"1acfc850f73d1955d14b282938585122"}', 'basic', '{"version":0,"details":""}', 2.00000000, 1.00000000, NULL, '2023-08-02 00:29:24');
/*!40000 ALTER TABLE `general_settings` ENABLE KEYS */;

-- Dumping structure for table encuestas.languages
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_align` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: left to right text align, 1: right to left text align',
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not default language, 1: default language',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.languages: ~2 rows (approximately)
DELETE FROM `languages`;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`id`, `name`, `code`, `icon`, `text_align`, `is_default`, `created_at`, `updated_at`) VALUES
	(1, 'Español', 'en', '5f15968db08911595250317.png', 0, 1, '2020-07-06 03:47:55', '2023-08-02 01:37:59'),
	(4, 'bangla', 'bn', '5f1596a650cd11595250342.png', 0, 0, '2020-07-20 07:05:42', '2021-01-03 23:59:33');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

-- Dumping structure for table encuestas.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.migrations: ~22 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2020_06_14_061757_create_support_tickets_table', 3),
	(5, '2020_06_14_061837_create_support_messages_table', 3),
	(6, '2020_06_14_061904_create_support_attachments_table', 3),
	(7, '2020_06_14_062359_create_admins_table', 3),
	(8, '2020_06_14_064604_create_transactions_table', 4),
	(9, '2020_06_14_065247_create_general_settings_table', 5),
	(12, '2014_10_12_100000_create_password_resets_table', 6),
	(13, '2020_06_14_060541_create_user_logins_table', 6),
	(14, '2020_06_14_071708_create_admin_password_resets_table', 7),
	(15, '2020_09_14_053026_create_countries_table', 8),
	(16, '2021_03_15_093340_create_categories_table', 9),
	(17, '2021_03_15_101940_create_surveyors_table', 10),
	(18, '2021_03_15_104146_create_surveyor_password_resets_table', 11),
	(19, '2021_03_15_111649_create_surveyor_logins_table', 12),
	(20, '2021_03_15_112235_create_surveyor_logins_table', 13),
	(21, '2021_03_15_113536_create_surveyors_table', 14),
	(22, '2021_03_23_103847_create_surveys_table', 15),
	(23, '2021_03_23_104010_create_questions_table', 16),
	(24, '2021_03_24_070020_create_options_table', 17),
	(25, '2021_03_31_095452_create_answers_table', 18);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table encuestas.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'template name',
  `secs` text COLLATE utf8mb4_unicode_ci,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.pages: ~4 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name`, `slug`, `tempname`, `secs`, `is_default`, `created_at`, `updated_at`) VALUES
	(1, 'Inicio', 'home', 'templates.basic.', '["about","counter","service","team","testimonial","faq","subscriber","brand"]', 1, '2020-07-11 06:23:58', '2021-04-25 01:42:42'),
	(3, 'Nosotros', 'about-us', 'templates.basic.', '["about","faq"]', 0, '2020-07-11 06:35:35', '2021-04-07 07:18:21'),
	(4, 'Servicios', 'services', '', '["service","about","subscriber","brand"]', 0, '2021-04-26 03:41:14', '2021-04-26 16:15:49'),
	(12, 'Preguntas Frecuentes', 'faq', 'templates.basic.', '["faq","subscriber"]', 0, '2021-04-25 01:54:16', '2021-04-26 03:40:42'),
	(14, 'INICIO', 'home', 'assets/templates/basic/', NULL, 0, '2023-08-01 21:41:01', '2023-08-01 21:41:01');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table encuestas.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table encuestas.questions
CREATE TABLE IF NOT EXISTS `questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'radio:1, checkbox:2',
  `custom_input` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Yes:1, No:0',
  `custom_input_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Required:1, Not Required:0',
  `custom_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.questions: ~14 rows (approximately)
DELETE FROM `questions`;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

-- Dumping structure for table encuestas.subscribers
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.subscribers: ~0 rows (approximately)
DELETE FROM `subscribers`;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;

-- Dumping structure for table encuestas.support_attachments
CREATE TABLE IF NOT EXISTS `support_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `support_message_id` int(11) NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.support_attachments: ~0 rows (approximately)
DELETE FROM `support_attachments`;
/*!40000 ALTER TABLE `support_attachments` DISABLE KEYS */;
INSERT INTO `support_attachments` (`id`, `support_message_id`, `attachment`, `created_at`, `updated_at`) VALUES
	(1, 1, '64c968bf0afff1690921151.jpg', '2023-08-02 00:19:11', '2023-08-02 00:19:11');
/*!40000 ALTER TABLE `support_attachments` ENABLE KEYS */;

-- Dumping structure for table encuestas.support_messages
CREATE TABLE IF NOT EXISTS `support_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supportticket_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.support_messages: ~0 rows (approximately)
DELETE FROM `support_messages`;
/*!40000 ALTER TABLE `support_messages` DISABLE KEYS */;
INSERT INTO `support_messages` (`id`, `supportticket_id`, `admin_id`, `message`, `created_at`, `updated_at`) VALUES
	(1, '1', 0, 'este es un mensaje de pruba', '2023-08-02 00:19:11', '2023-08-02 00:19:11'),
	(2, '2', 0, 'asdfsadf', '2023-08-02 19:09:39', '2023-08-02 19:09:39');
/*!40000 ALTER TABLE `support_messages` ENABLE KEYS */;

-- Dumping structure for table encuestas.support_tickets
CREATE TABLE IF NOT EXISTS `support_tickets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `surveyor_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0: Open, 1: Answered, 2: Replied, 3: Closed',
  `last_reply` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.support_tickets: ~0 rows (approximately)
DELETE FROM `support_tickets`;
/*!40000 ALTER TABLE `support_tickets` DISABLE KEYS */;
INSERT INTO `support_tickets` (`id`, `user_id`, `surveyor_id`, `name`, `email`, `ticket`, `subject`, `status`, `last_reply`, `created_at`, `updated_at`) VALUES
	(1, NULL, 1, 'rene soto', 'rene@gmail.com', '946562', 'a que se debe la duda', 0, '2023-08-01 20:19:10', '2023-08-02 00:19:10', '2023-08-02 00:19:10'),
	(2, NULL, NULL, 'afasdfasd', 'asdfsadf@gmail.com', '47822513', 'asdfsadf', 0, '2023-08-02 15:09:39', '2023-08-02 19:09:39', '2023-08-02 19:09:39');
/*!40000 ALTER TABLE `support_tickets` ENABLE KEYS */;

-- Dumping structure for table encuestas.surveyors
CREATE TABLE IF NOT EXISTS `surveyors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_by` int(11) DEFAULT NULL,
  `balance` decimal(18,8) DEFAULT '0.00000000',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'contains full address',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: banned, 1: active',
  `ev` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: email unverified, 1: email verified',
  `sv` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: sms unverified, 1: sms verified',
  `ver_code` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stores verification code',
  `ver_code_send_at` datetime DEFAULT NULL COMMENT 'verification send time',
  `ts` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 2fa off, 1: 2fa on',
  `tv` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: 2fa unverified, 1: 2fa verified',
  `tsc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `surveyors_username_unique` (`username`),
  UNIQUE KEY `surveyors_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.surveyors: ~0 rows (approximately)
DELETE FROM `surveyors`;
/*!40000 ALTER TABLE `surveyors` DISABLE KEYS */;
INSERT INTO `surveyors` (`id`, `firstname`, `lastname`, `username`, `email`, `mobile`, `ref_by`, `balance`, `password`, `image`, `address`, `status`, `ev`, `sv`, `ver_code`, `ver_code_send_at`, `ts`, `tv`, `tsc`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Osamu', 'Yokosaki', 'oyokosakip', 'oyokosakip@gmail.com', NULL, NULL, 0.00000000, '$2y$10$0wC4zEftJqMHrmF1thcj3.88ouwfL/2db4ReNNv4zHBSDDMLcxR7W', NULL, NULL, 1, 1, 1, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, '2023-08-27 20:04:55', '2023-08-27 20:04:55');
/*!40000 ALTER TABLE `surveyors` ENABLE KEYS */;

-- Dumping structure for table encuestas.surveyor_logins
CREATE TABLE IF NOT EXISTS `surveyor_logins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `surveyor_id` int(11) NOT NULL,
  `surveyor_ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.surveyor_logins: ~51 rows (approximately)
DELETE FROM `surveyor_logins`;
/*!40000 ALTER TABLE `surveyor_logins` DISABLE KEYS */;
INSERT INTO `surveyor_logins` (`id`, `surveyor_id`, `surveyor_ip`, `location`, `browser`, `os`, `longitude`, `latitude`, `country`, `country_code`, `created_at`, `updated_at`) VALUES
	(65, 1, '127.0.0.1', ' - -  -  ', 'Chrome', 'Windows 10', '', '', '', '', '2023-09-22 23:06:14', '2023-09-22 23:06:14');
/*!40000 ALTER TABLE `surveyor_logins` ENABLE KEYS */;

-- Dumping structure for table encuestas.surveyor_password_resets
CREATE TABLE IF NOT EXISTS `surveyor_password_resets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.surveyor_password_resets: ~0 rows (approximately)
DELETE FROM `surveyor_password_resets`;
/*!40000 ALTER TABLE `surveyor_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyor_password_resets` ENABLE KEYS */;

-- Dumping structure for table encuestas.surveys
CREATE TABLE IF NOT EXISTS `surveys` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `surveyor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_limit` tinyint(1) DEFAULT NULL COMMENT 'Yes:1, No:0',
  `country_limit` tinyint(1) DEFAULT NULL COMMENT 'Yes:1, No:0',
  `start_age` int(3) DEFAULT NULL,
  `end_age` int(3) DEFAULT NULL,
  `country` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'pending:1, approved:0, rejected:3',
  `users` longtext COLLATE utf8mb4_unicode_ci,
  `color` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_text` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_limite` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.surveys: ~5 rows (approximately)
DELETE FROM `surveys`;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;

-- Dumping structure for table encuestas.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `surveyor_id` int(11) DEFAULT NULL,
  `amount` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `charge` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `post_balance` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `trx_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trx` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.transactions: ~8 rows (approximately)
DELETE FROM `transactions`;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` (`id`, `user_id`, `surveyor_id`, `amount`, `charge`, `post_balance`, `trx_type`, `trx`, `details`, `created_at`, `updated_at`) VALUES
	(1, 3, NULL, 2.00000000, 0.00000000, 2.00000000, '+', 'D9UHCRFYGXWM', 'For Completing pruguntas de conocimiento general', '2023-08-02 22:55:47', '2023-08-02 22:55:47'),
	(2, NULL, 1, 4.00000000, 0.00000000, -4.00000000, '-', 'S1V6PWMSDSEV', 'For Get Answerd pruguntas de conocimiento general', '2023-08-02 22:55:47', '2023-08-02 22:55:47'),
	(3, 1, NULL, 3.00000000, 0.00000000, 3.00000000, '+', 'VWSACKAUDXB7', 'For Completing preguntas de validacion', '2023-08-03 16:33:15', '2023-08-03 16:33:15'),
	(4, NULL, 3, 6.00000000, 0.00000000, -6.00000000, '-', '3ZDK4C4QJ5JZ', 'For Get Answerd preguntas de validacion', '2023-08-03 16:33:15', '2023-08-03 16:33:15'),
	(5, 1, NULL, 2.00000000, 0.00000000, 5.00000000, '+', 'R68XCWV7J8KG', 'For Completing definicion de personalizacion', '2023-08-03 16:43:18', '2023-08-03 16:43:18'),
	(6, NULL, 3, 4.00000000, 0.00000000, -10.00000000, '-', 'VO1U585MUCO4', 'For Get Answerd definicion de personalizacion', '2023-08-03 16:43:18', '2023-08-03 16:43:18'),
	(7, 1, NULL, 1.00000000, 0.00000000, 6.00000000, '+', 'RTXWZYYH8Q55', 'For Completing verificacion de casilla de texto', '2023-08-03 16:54:15', '2023-08-03 16:54:15'),
	(8, NULL, 3, 2.00000000, 0.00000000, -12.00000000, '-', '8M6PMJE8RPJS', 'For Get Answerd verificacion de casilla de texto', '2023-08-03 16:54:15', '2023-08-03 16:54:15'),
	(9, 1, NULL, 2.00000000, 0.00000000, 8.00000000, '+', '5R3PSTZ4R71P', 'For Completing preuebas', '2023-08-03 23:59:44', '2023-08-03 23:59:44'),
	(10, NULL, 1, 4.00000000, 0.00000000, -8.00000000, '-', 'DQDOXQKN9PF6', 'For Get Answerd preuebas', '2023-08-03 23:59:44', '2023-08-03 23:59:44');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping structure for table encuestas.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `profession` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_by` int(11) DEFAULT NULL,
  `balance` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `completed_survey` int(11) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci COMMENT 'contains full address',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: banned, 1: active',
  `ev` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: email unverified, 1: email verified',
  `sv` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: sms unverified, 1: sms verified',
  `ver_code_send_at` datetime DEFAULT NULL COMMENT 'verification send time',
  `ts` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 2fa off, 1: 2fa on',
  `tv` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: 2fa unverified, 1: 2fa verified',
  `tsc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ver_code` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.users: ~4 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `email`, `mobile`, `age`, `profession`, `ref_by`, `balance`, `completed_survey`, `password`, `image`, `address`, `status`, `ev`, `sv`, `ver_code_send_at`, `ts`, `tv`, `tsc`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`, `ver_code`, `category_id`) VALUES
	(1, 'Limbert', 'Loza', 'llozag', 'llozag@gmail.com', NULL, NULL, NULL, NULL, 0.00000000, 2, '$2y$10$0wC4zEftJqMHrmF1thcj3.88ouwfL/2db4ReNNv4zHBSDDMLcxR7W', '1695418493_tester1234.jpg', NULL, 1, 0, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, '2023-08-27 20:10:53', '2023-09-22 21:34:53', NULL, 3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table encuestas.user_categories
CREATE TABLE IF NOT EXISTS `user_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.user_categories: ~4 rows (approximately)
DELETE FROM `user_categories`;
/*!40000 ALTER TABLE `user_categories` DISABLE KEYS */;
INSERT INTO `user_categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'DPTO. I-PERSONAL', 1, '2023-09-21 09:27:23', '2023-09-21 09:27:26'),
	(2, 'DPTO. II-INTELIGENCIA', 1, '2023-09-21 09:27:24', '2023-09-21 09:27:27'),
	(3, 'DPTO. III-OPERACIONES', 1, '2023-09-21 09:27:24', '2023-09-21 09:27:28'),
	(4, 'DPTO. IV-LOGISTICA', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(5, 'DPTO. V-AC/DC', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(6, 'DPTO. VI-EDUCACION', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(7, 'DPTO. VII-PRODUCCION Y ECOLOGIA', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(8, 'DPTO. VIII-DOCTRINA', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(9, 'DPTO. IX-CIBERDEFENSA', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(10, 'DGJURE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(11, 'DGPLAE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(12, 'DGBPIE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(13, 'DGAFE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(14, 'CEEE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36'),
	(15, 'IGE', 1, '2023-09-21 09:27:25', '2023-09-22 22:21:36');
/*!40000 ALTER TABLE `user_categories` ENABLE KEYS */;

-- Dumping structure for table encuestas.user_logins
CREATE TABLE IF NOT EXISTS `user_logins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.user_logins: ~25 rows (approximately)
DELETE FROM `user_logins`;
/*!40000 ALTER TABLE `user_logins` DISABLE KEYS */;
INSERT INTO `user_logins` (`id`, `user_id`, `user_ip`, `location`, `browser`, `os`, `longitude`, `latitude`, `country`, `country_code`, `created_at`, `updated_at`) VALUES
	(1, 1, '127.0.0.1', ' - -  -  ', 'Chrome', 'Windows 10', '', '', '', '', '2023-08-03 16:29:05', '2023-08-03 16:29:05'),
	(31, 1, '127.0.0.1', ' - -  -  ', 'Chrome', 'Windows 10', '', '', '', '', '2023-09-22 23:06:45', '2023-09-22 23:06:45');
/*!40000 ALTER TABLE `user_logins` ENABLE KEYS */;

-- Dumping structure for table encuestas.withdrawals
CREATE TABLE IF NOT EXISTS `withdrawals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `amount` decimal(18,8) NOT NULL,
  `currency` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(18,8) NOT NULL,
  `charge` decimal(18,8) NOT NULL,
  `trx` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `final_amount` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `after_charge` decimal(18,8) NOT NULL,
  `withdraw_information` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=>success, 2=>pending, 3=>cancel,  ',
  `admin_feedback` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.withdrawals: ~0 rows (approximately)
DELETE FROM `withdrawals`;
/*!40000 ALTER TABLE `withdrawals` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdrawals` ENABLE KEYS */;

-- Dumping structure for table encuestas.withdraw_methods
CREATE TABLE IF NOT EXISTS `withdraw_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_limit` decimal(18,8) DEFAULT NULL,
  `max_limit` decimal(18,8) NOT NULL DEFAULT '0.00000000',
  `delay` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fixed_charge` decimal(18,8) DEFAULT NULL,
  `rate` decimal(18,8) DEFAULT NULL,
  `percent_charge` decimal(5,2) DEFAULT NULL,
  `currency` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_data` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table encuestas.withdraw_methods: ~0 rows (approximately)
DELETE FROM `withdraw_methods`;
/*!40000 ALTER TABLE `withdraw_methods` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdraw_methods` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
