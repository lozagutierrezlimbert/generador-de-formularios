<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnWidths;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Support\Collection;
use App\Answer;
use App\Survey;
use PhpOffice\PhpSpreadsheet\Style\Alignment;


use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AnswersExport implements FromCollection , WithHeadings , WithColumnWidths
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        return new Collection($this->data);
    }
    public function headings(): array
    {
        return [

        ];
    }
    
    public function columnWidths(): array
    {
        // Define el ancho de las columnas aquí
        return [
            'A' => 15,  // Ancho de la columna A
            'B' => 20,  // Ancho de la columna B
            'C' => 20,  // Ancho de la columna C
            'D' => 15,  // Ancho de la columna D
            'E' => 30,  // Ancho de la columna E
            'F' => 30,  // Ancho de la columna F
            'G' => 30,  // Ancho de la columna E
            'H' => 30,
            'I' => 30,  // Ancho de la columna E
            'J' => 30,
            'K' => 30,  // Ancho de la columna E
            'L' => 30,
            'M' => 30,  // Ancho de la columna E
        ];
    }
    public function styles(Worksheet $sheet)
    {
        // Ajustar el ancho automáticamente para todas las columnas con datos
        foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        // Otros estilos si es necesario
        $sheet->getStyle('A:Z')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        // Puedes agregar más configuraciones según tus necesidades
    }

}

