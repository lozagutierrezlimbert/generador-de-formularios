<?php

namespace App\Http\Controllers\Surveyor;

use App\Answer;
use App\Category;
use App\UserCategory;
use App\Deposit;
use App\GeneralSetting;
use App\Lib\GoogleAuthenticator;
use App\Http\Controllers\Controller;
use App\Option;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\FileTypeValidate;
use App\Survey;
use App\Transaction;
use Illuminate\Support\Facades\Hash;
use PDF;
use App\Surveyor;
use App\User;
use App\Exports\AnswersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class SurveyorController extends Controller
{
    public function dashboard()
    {
        $page_title = 'Dashboard';
        $surveyor = auth()->guard('surveyor')->user();
        $totalDeposit = Deposit::where('surveyor_id',$surveyor->id)->where('status',1)->sum('amount');
        $totalTransaction = Transaction::where('surveyor_id',$surveyor->id)->count();
        $approvedSurvey = $surveyor->surveys->where('status',0)->count();
        $pendingSurvey = $surveyor->surveys->where('status',1)->count();
        $rejectedSurvey = $surveyor->surveys->where('status',3)->count();

        $survey_chart = Answer::where('surveyor_id',$surveyor->id)->groupBy('survey_id')->orderBy('created_at')->get()->groupBy(function ($d) {
            return $d->created_at->format('F');
        });

        $survey_all = [];
        $month_survey = []; 
        foreach ($survey_chart as $key => $value) {
            $survey_all[] = count($value);
            $month_survey[] = $key;
        }
        return   view('surveyor.dashboard',compact('page_title','totalDeposit','surveyor','totalTransaction','approvedSurvey','pendingSurvey','rejectedSurvey','survey_all','month_survey'));
    }

    public function profile()
    {
        $page_title = 'Perfil';
        $surveyor = Auth::guard('surveyor')->user();
        return view('surveyor.profile', compact('page_title', 'surveyor'));
    }

    public function profileUpdate(Request $request)
    {
        $this->validate($request, [
            'image' => [new FileTypeValidate(['jpeg', 'jpg', 'png'])],
            'firstname' => 'required|string|max:50',
            'lastname' => 'required|string|max:50',
        ],[
            'firstname.required'=>'First Name Field is required',
            'lastname.required'=>'Last Name Field is required'
        ]);

        $surveyor = Auth::guard('surveyor')->user();


        $in['firstname'] = $request->firstname;
        $in['lastname'] = $request->lastname;
        $surveyor_image = $surveyor->image;
        if($request->hasFile('image')) {
            try{

                $location = imagePath()['profile']['surveyor']['path'];
                $size = imagePath()['profile']['surveyor']['size'];
                $old = $surveyor->image;
                $surveyor_image = uploadImage($request->image, $location , $size, $old);

            }catch(\Exception $exp) {
                return back()->withNotify(['error', 'Could not upload the image.']);
            }
        }

        $in['image'] = $surveyor_image;
        $surveyor->fill($in)->save();

        $notify[] = ['success', 'Perfil actualizado correctamente.'];
        return redirect()->route('surveyor.profile')->withNotify($notify);

    }

    public function password()
    {
        $page_title = 'Cambiar Contraseña';
        $surveyor = Auth::guard('surveyor')->user();
        return view('surveyor.password', compact('page_title', 'surveyor'));
    }

    public function passwordUpdate(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        $surveyor = Auth::guard('surveyor')->user();
        if (!Hash::check($request->old_password, $surveyor->password)) {
            $notify[] = ['error', 'Password Do not match !!'];
            return back()->withErrors(['Invalid old password.']);
        }

        $surveyor->update([
            'password' => Hash::make($request->password),
        ]);

        $notify[] = ['success', 'Password Changed Successfully.'];
        return redirect()->route('surveyor.password')->withNotify($notify);
    }

    public function show2faForm()
    {
        $gnl = GeneralSetting::first();
        $ga = new GoogleAuthenticator();
        $surveyor = Auth::guard('surveyor')->user();
        $secret = $ga->createSecret();
        $qrCodeUrl = $ga->getQRCodeGoogleUrl($surveyor->username . '@' . $gnl->sitename, $secret);
        $prevcode = $surveyor->tsc;
        $prevqr = $ga->getQRCodeGoogleUrl($surveyor->username . '@' . $gnl->sitename, $prevcode);
        $page_title = 'Two Factor';
        return view('surveyor.twofactor', compact('page_title', 'secret', 'qrCodeUrl', 'prevcode', 'prevqr'));
    }

    public function create2fa(Request $request)
    {
        $surveyor = Auth::guard('surveyor')->user();
        $this->validate($request, [
            'key' => 'required',
            'code' => 'required',
        ]);

        $ga = new GoogleAuthenticator();
        $secret = $request->key;
        $oneCode = $ga->getCode($secret);

        if ($oneCode === $request->code) {
            $surveyor->tsc = $request->key;
            $surveyor->ts = 1;
            $surveyor->tv = 1;
            $surveyor->save();


            $surveyorAgent = getIpInfo();
            $osBrowser = osBrowser();
            notify($surveyor, '2FA_ENABLE', [
                'operating_system' => @$osBrowser['os_platform'],
                'browser' => @$osBrowser['browser'],
                'ip' => @$surveyorAgent['ip'],
                'time' => @$surveyorAgent['time']
            ]);


            $notify[] = ['success', 'Google Authenticator Enabled Successfully'];
            return back()->withNotify($notify);
        } else {
            $notify[] = ['error', 'Wrong Verification Code'];
            return back()->withNotify($notify);
        }
    }


    public function disable2fa(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
        ]);

        $surveyor = Auth::guard('surveyor')->user();
        $ga = new GoogleAuthenticator();

        $secret = $surveyor->tsc;
        $oneCode = $ga->getCode($secret);
        $surveyorCode = $request->code;

        if ($oneCode == $surveyorCode) {

            $surveyor->tsc = null;
            $surveyor->ts = 0;
            $surveyor->tv = 1;
            $surveyor->save();


            $surveyorAgent = getIpInfo();
            $osBrowser = osBrowser();
            notify($surveyor, '2FA_DISABLE', [
                'operating_system' => @$osBrowser['os_platform'],
                'browser' => @$osBrowser['browser'],
                'ip' => @$surveyorAgent['ip'],
                'time' => @$surveyorAgent['time']
            ]);


            $notify[] = ['success', 'Two Factor Authenticator Disable Successfully'];
            return back()->withNotify($notify);
        } else {
            $notify[] = ['error', 'Wrong Verification Code'];
            return back()->withNotify($notify);
        }
    }

    public function depositHistory()
    {
        $page_title = 'Deposit History';
        $empty_message = 'No history found.';
        $logs = auth()->guard('surveyor')->user()->deposits()->with(['gateway'])->latest()->paginate(getPaginate());
        return view('surveyor.deposit_history', compact('page_title', 'empty_message', 'logs'));
    }

    public function transactionHistory()
    {
        $page_title = 'Successfull Transactions';
        $transactions = auth()->guard('surveyor')->user()->transactions()->with('surveyor')->latest()->paginate(getPaginate());
        $empty_message = 'No transactions';
        return view('surveyor.transactions', compact('page_title', 'transactions', 'empty_message'));
    }

    public function transactionSearch(Request $request)
    {
        $request->validate(['search' => 'required']);
        $search = $request->search;
        $page_title = 'Transactions Search - ' . $search;
        $empty_message = 'No transactions.';

        $transactions = Transaction::where('surveyor_id',auth()->guard('surveyor')->user()->id)->where('trx', $search)->orderBy('id','desc')->paginate(getPaginate());

        return view('surveyor.transactions', compact('page_title', 'transactions', 'empty_message'));
    }

    public function surveyAll()
    {
        $page_title = 'Todas las encuestas';
        $surveys = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->latest()->paginate(getPaginate());
        $empty_message = 'Aun no tienes encuestas registradas';
        return view('surveyor.survey.index', compact('page_title','surveys','empty_message'));
    }

    public function surveyNew()
    {
        $page_title = 'Nueva encuesta';
        $categories = Category::where('status','1')->latest()->get();
        return view('surveyor.survey.new', compact('page_title','categories'));
    }

    public function surveyStore(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'image' => [new FileTypeValidate(['jpeg', 'jpg', 'png'])],
            'category_id' => 'required|numeric|gt:0',
            'name' => 'required|string|max:191',
            'fecha_hora' => [
                'required',
                'date',
                'after_or_equal:'.Carbon::now()->addHour(), 
            ],
        ],[
            'fecha_hora.after'=>'El tiempo minimo es un hora despues de la creacion de la encuesta'
        ]);


        $general = GeneralSetting::first();
  
        $survey_image = '';
        if($request->hasFile('image')) {
            try{
                $location = imagePath()['survey']['path'];
                $size = imagePath()['survey']['size'];

                $survey_image = uploadImage($request->image, $location , $size);

            }catch(\Exception $exp) {
                return back()->withNotify(['error', 'Could not upload the image.']);
            }
        }


        $fechaHora = $request->fecha_hora;
        $fechaHoraMySQL = date('Y-m-d H:i:s', strtotime($fechaHora));

        $survey = new Survey();
        $survey->image = $survey_image;
        $survey->name = $request->name;
        $survey->color = $request->color;
        $survey->type_text = $request->type_text;
        $survey->category_id = $request->category_id;
        $survey->surveyor_id = auth()->guard('surveyor')->user()->id;
        $survey->status = $general->survey_approval;
        $survey->fecha_limite = $fechaHoraMySQL;
        $survey->save();

        $notify[] = ['success', 'Encuesta agregada de manera correcta'];
        return redirect()->route('surveyor.survey.question.new',$survey->id)->withNotify($notify);
    }

    public function surveyEdit($id)
    {
        $survey = Survey::findOrFail($id);
        // dd($survey);}
        if ($survey->surveyor_id != auth()->guard('surveyor')->user()->id) {
            $notify[] = ['success', 'No tienes autorizacion para editar esta encuesta'];
            return back()->withNotify($notify);
        }

        if ($survey->status == 3) {
            $notify[] = ['error', 'Do no try to cheat us'];
            return back()->withNotify($notify);
        }

        $page_title = 'Editar Encuesta';
        $categories = Category::where('status','1')->latest()->get();
        return view('surveyor.survey.edit',compact('page_title','survey','categories'));
    }

    public function surveyUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'image' => [new FileTypeValidate(['jpeg', 'jpg', 'png'])],
            'category_id' => 'required|numeric|gt:0',
            'name' => 'required|string|max:191',
            'fecha_hora' => [
                'required',
                'date',
                'after_or_equal:'.Carbon::now()->addHour(), 
            ],
        ],[
            'fecha_hora.after'=>'El tiempo minimo es un hora despues de la creacion de la encuesta'
        ]);

        $survey = Survey::findOrFail($id);

        if ($survey->surveyor_id != auth()->guard('surveyor')->user()->id) {
            $notify[] = ['success', 'No tienes permisos para actualizar esta encuesta'];
            return back()->withNotify($notify);
        }

        $general = GeneralSetting::first();

        $survey_image = $survey->image;
        if($request->hasFile('image')) {
            try{
                $location = imagePath()['survey']['path'];
                $size = imagePath()['survey']['size'];
                $old = $survey_image;
                $survey_image = uploadImage($request->image, $location , $size, $old);

            }catch(\Exception $exp) {
                return back()->withNotify(['error', 'Could not upload the image.']);
            }
        }


        
        $fechaHora = $request->fecha_hora;
        $fechaHoraMySQL = date('Y-m-d H:i:s', strtotime($fechaHora));

        $survey->image = $survey_image;
        $survey->name = $request->name;
        $survey->color = $request->color;
        $survey->type_text = $request->type_text;
 
        $survey->category_id = $request->category_id;
        $survey->status = $general->survey_approval;
        $survey->fecha_limite = $fechaHoraMySQL;
        $survey->save();

        $notify[] = ['success', 'Encuesta actualizada correctamente'];
        return back()->withNotify($notify);
    }

    public function questionAll($id)
    {
        $page_title = 'Todas las preguntas';
        $survey = Survey::findOrFail($id);

        if ($survey->surveyor_id != auth()->guard('surveyor')->user()->id) {
            $notify[] = ['success', 'You are not authorized to see this survey'];
            return back()->withNotify($notify);
        }

        $questions = $survey->questions()->paginate(getPaginate());
        $empty_message = 'No question found';
        return view('surveyor.question.index', compact('page_title','survey','empty_message','questions'));
    }



    public function userAdd($id)
    {
        $page_title = 'Categoria que pueden responder esta encuesta';
        $empty_message = 'No hay usuarios registrados ';
        
        $survey = Survey::where('id',$id)->with('questions')->first();
        $users = User::latest()->paginate(getPaginate());
        $userCategories = UserCategory::has('users')->get();
    
        
        return view('surveyor.question.list', compact('page_title', 'empty_message', 'users', 'survey' , 'userCategories'));
    }


    public function updateStatus(Request $request)
    {
        $category_id = $request->input('category_id');
        $dataValue = $request->input('dataValue');
        $survey = $request->input('survey');
        
        $category = UserCategory::findOrFail($category_id);
        $survey = Survey::where('id',$survey['id'])->first();

        if($dataValue === 'true'){
        //se desabilitaran a estos ususarios de esta categoria para que no puedan responder esta escuesta
            if($survey->users){
                // dd("con usuarios");
                $users = $category->users;
                $survey_users = $survey->users;
                foreach ($users as $user) {
                    if(!in_array($user->id,$survey->users)){
                        array_push($survey_users,$user->id);            
                    }
                }
                $survey->users = $survey_users;
                    $survey->save();
                    $response = [
                        'message' => "Usuaruios desabilitados para esta encuesta",
                        'success' => true,
                        'is_enabled' => false
                    
                    ];
                    return response()->json($response);
            }else{
              
                $users = $category->users;
                $survey->users = [];
                $survey->save();
                $survey_users = $survey->users;
                foreach ($users as $user) {
             
                    if(!in_array($user->id,$survey->users)){
                     
                        array_push($survey_users,$user->id);            
                    }
                }
                $survey->users = $survey_users;
                    $survey->save();
                    $response = [
                        'message' => "Usuaruios desabilitados para esta encuesta",
                        'success' => true,
                        'is_enabled' => false
                    
                    ];
                    return response()->json($response);

            }
       
        }elseif($dataValue === 'false'){
            //se habiliatarn a todos los usuarios de esta categoria para que respondan a esta encuesta
            $users = $category->users;
            $survey_users = $survey->users;
            foreach ($users as $user) {
                if(in_array($user->id,$survey->users)){
                    $survey_users = array_diff($survey_users, [$user->id]);           
                }
            }
            $survey->users = $survey_users;
            $survey->save();
            $response = [
                'message' => "Usuarios habilitados para esta encuesta",
                'success' => true,
                'is_enabled' => true
            ];
            return response()->json($response);

        }elseif($dataValue === 'conres'){
            $users = $category->users;
            $survey_users = $survey->users;
            foreach ($users as $user) {
                if(in_array($user->id,$survey->users)){
                    $survey_users = array_diff($survey_users, [$user->id]);           
                }
            }
            $survey->users = $survey_users;
            $survey->save();
            $response = [
                'message' => "Usuarios habilitados para esta encuesta",
                'success' => true,
                'conres' => true,
            ];
            return response()->json($response);
        }
   
        $response = [
            'message' => "Ocurrio un error",
            'success' => false,
            'is_enabled' => false
        ];
    return response()->json($response);
    }

    public function questionNew($id)
    {
        $survey = Survey::findOrFail($id);
        $page_title = 'Nueva Pregunta';
        return view('surveyor.question.new', compact('page_title','survey'));
    }

    public function questionStore(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'survey_id' => 'required|gt:0',
            'type' => 'required|min:1|max:2',
            'question' => 'required|max:500',
            'options.*' => 'max:500',
        ],[
      
        ]);

        $survey = Survey::findOrFail($request->survey_id);

        $question = new Question();
        $question->survey_id = $survey->id;
        $question->question = $request->question;
        $question->type = $request->type;
        $question->options = array_values($request->options);
        $question->save();

        $notify[] = ['success', 'Pregunta agregada correctamente'];
        return back()->withNotify($notify);
    }

    public function questionEdit($q_id,$s_id)
    {
        $question = Question::findOrFail($q_id);

        if ($question->survey_id != $s_id) {
            $notify[] = ['error', 'No tienes autorizacion para actualizar encuestas'];
            return back()->withNotify($notify);
        }

        $page_title = 'Editar preguntas';
        return view('surveyor.question.edit',compact('page_title','question','s_id'));
    }

    public function questionUpdate(Request $request,$id)
    {
        $this->validate($request, [
            'survey_id' => 'required|gt:0',
            'type' => 'required|min:1|max:2',
            'question' => 'required|max:255',
            'options.*' => 'required|max:191',
        ],[
    
        ]);


        $survey = Survey::findOrFail($request->survey_id);
        $general = GeneralSetting::first();
        $survey->status = $general->survey_approval;
        $survey->save();

        $question = Question::findOrFail($id);
        if ($question->survey_id != $survey->id) {
            $notify[] = ['error', 'No tienes permiso para actualizar preguntas'];
            return back()->withNotify($notify);
        }


        if(!$request->options){

            $options = $question->options;
        }
        if($request->options){

            $options = array_merge( $question->options,$request->options);
        }

        $question->question = $request->question;
        $question->type = $request->type;
        $question->custom_input = $request->custom_input;
        $question->custom_input_type = $request->custom_input_type??0;
        $question->custom_question = $request->custom_question;
        $question->options = $options;
        $question->save();
        $notify[] = ['success', 'Pregunta Actualizada'];
        return back()->withNotify($notify);
    }

    public function questionView($q_id,$s_id)
    {
        $question = Question::findOrFail($q_id);

        if ($question->survey_id != $s_id) {
            $notify[] = ['error', 'YNo tienes permiso para ver esta pregunta'];
            return back()->withNotify($notify);
        }

        $page_title = 'Ver pregunta';
        return view('surveyor.question.view',compact('page_title','question','s_id'));
    }

    public function report()
    {
        $page_title = 'Reporte de encuestas';
        $surveys = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->where('status',0)->latest()->paginate(getPaginate());
        $empty_message = 'No  tiene encuestas';
        return view('surveyor.report.index', compact('page_title','surveys','empty_message'));
    }

    public function reportQuestion($id)
    {
        $page_title = 'Reporte de encuesta';
        $survey = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->findOrFail($id);
        if (count($survey->answers) <= 0) {
            $notify[] = ['error', 'La encuesta aun no fue respondida'];
            return back()->withNotify($notify);
        }
        return view('surveyor.report.question', compact('page_title','survey'));
    }
    public function reportAnswers($id)
    {
        $page_title = 'Reporte de respuestas';
        $survey = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->findOrFail($id);
     
        if (count($survey->answers) <= 0) {
            $notify[] = ['error', 'La encuesta aun no fue respondida'];
            return back()->withNotify($notify);
        }

        $data = Answer::query()
        ->select([
            'answers.id as Id',
            'surveyors.username as Encuestador',
            'surveys.name as Encuesta',
            DB::raw('COALESCE(users.username, "Anónimo") as Usuario'),
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftjoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->groupBy('answers.user_id')
        ->get()
        ->toArray();

        $respuestas = Answer::query()
        ->select([
            'questions.question as Pregunta',
            'answers.answer as respuesta',
            'answers.custom_answer as  file',
            'answers.question_id as preguntaId',
            'users.username as Usuario',
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftJoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->get()
        ->toArray();
      
        $respuestasPorPregunta = [];
        // dd($respuestas);
        foreach ($respuestas as $respuesta) {
            $pregunta = $respuesta['Pregunta'];
            if (!isset($respuestasPorPregunta[$pregunta])) {
                $respuestasPorPregunta[$pregunta] = [];
            }
        if($respuesta['respuesta'] === '[]' || empty($respuesta)  ){
            $valorRespuesta = $respuesta['file'];
        }else{
            $valorRespuesta =  $respuesta['respuesta'];
        }
                $respuestasPorPregunta[$pregunta][] = $valorRespuesta;
        }      
        $respuestasSolo = [];    
        foreach ($respuestasPorPregunta as $pregunta => $respuestas) {
            $respuestasSolo[] = $respuestas;
        }

        if (count($respuestasSolo) > 1) {
            $respuestasSolo = array_map(null, ...$respuestasSolo);
        }else{

            $respuestasSolo = array_map(function ($valor) {
                return [$valor];
            }, $respuestasSolo[0]);
        } 
        $answers = $survey->answers;
        // dd($respuestasSolo);
        return view('surveyor.report.answers', compact('page_title','survey','answers','data','respuestasSolo','respuestasPorPregunta'));
    }
    public function answersDownloadExcel($id)
    {
        $data = Answer::query()
        ->select([
            'answers.id as Id',
            'surveyors.username as Encuestador',
            'surveys.name as Encuesta',
            DB::raw('COALESCE(users.username, "Anónimo") as Usuario'),
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftjoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->groupBy('answers.user_id')
        ->get()
        ->toArray();
        $nuevoArray = [
            'Id' => 'Id',
            'Encuestador' => 'Creador de Formularios',
            'Encuesta' => 'Formulario',
            'Usuario' => 'Usuario',
        ];
        array_unshift($data, $nuevoArray);

        $spreadsheet = new Spreadsheet();
        $startCell = 'A1';
        $this->addTableToWorksheet($spreadsheet, $startCell, $data);

        $respuestas = Answer::query()
        ->select([
            'questions.question as Pregunta',
            'answers.answer as respuesta',
            'answers.question_id as preguntaId',
            'users.username as Usuario',
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftJoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->get()
        ->toArray();
      
        $respuestasPorPregunta = [];

        foreach ($respuestas as $respuesta) {
            $pregunta = $respuesta['Pregunta'];
            if (!isset($respuestasPorPregunta[$pregunta])) {
                $respuestasPorPregunta[$pregunta] = [];
            }

            $respuestasPorPregunta[$pregunta][] = $respuesta['respuesta'];
        }      
        // dd($respuestasPorPregunta); 
        $respuestasSolo = [];    
        // Iteramos sobre las respuestas y construimos el array de respuestas para cada pregunta
        foreach ($respuestasPorPregunta as $pregunta => $respuestas) {
            // dd($respuestas);
            $respuestasSolo[] = array_merge([$pregunta], $respuestas);
        }

        if (count($respuestasSolo) > 1) {
            // Transponemos el array para intercambiar filas por columnas
            $respuestasSolo = array_map(null, ...$respuestasSolo);
        }else{

            $respuestasSolo = array_map(function ($valor) {
                return [$valor];
            }, $respuestasSolo[0]);
        }
        //    dd($respuestasSolo);
           $lastColumn = $spreadsheet->getActiveSheet()->getHighestColumn();
            $nextColumn = ++$lastColumn;
            $startCell = $nextColumn . '1';
            $this->addTableToWorksheet($spreadsheet, $startCell, $respuestasSolo);

        $res = $spreadsheet->getActiveSheet()->toArray();
        return Excel::download(new AnswersExport($res), 'answers.xlsx');

    }
    
    private function addTableToWorksheet($spreadsheet, $startCell, $data)
    {
        $spreadsheet->getActiveSheet()->fromArray($data, null, $startCell);
    }
    

    public function reportDownload($id)
    {
        $survey = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->findOrFail($id);

        if(count($survey->questions) <= 0) {
            $notify[] = ['error', 'Reporte no disponible'];
            return back()->withNotify($notify);
        }

        $page_title = 'Descargar reporte';
        $filename = strtolower(str_replace(' ','_',$survey->name));
        return view('surveyor.report.report',compact('survey','page_title','filename'));
    }
    
    public function answersDownload($id)
    {
        // dd("descargar pdf de respuestas");
        $survey = Survey::where('surveyor_id',auth()->guard('surveyor')->user()->id)->findOrFail($id);

        if(count($survey->questions) <= 0) {
            $notify[] = ['error', 'No report available'];
            return back()->withNotify($notify);
        }




       
        $data = Answer::query()
        ->select([
            'answers.id as Id',
            'surveyors.username as Encuestador',
            'surveys.name as Encuesta',
            DB::raw('COALESCE(users.username, "Anónimo") as Usuario'),
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftjoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->groupBy('answers.user_id')
        ->get()
        ->toArray();

        $respuestas = Answer::query()
        ->select([
            'questions.question as Pregunta',
            'answers.answer as respuesta',
            'answers.custom_answer as  file',
            'answers.question_id as preguntaId',
            'users.username as Usuario',
        ])
        ->join('surveys', 'answers.survey_id', '=', 'surveys.id')
        ->join('surveyors', 'surveys.surveyor_id', '=', 'surveyors.id')
        ->leftJoin('users', 'answers.user_id', '=', 'users.id')
        ->join('questions', 'answers.question_id', '=', 'questions.id')
        ->where('answers.survey_id', $id)
        ->orderBy('answers.created_at', 'asc') 
        ->get()
        ->toArray();
      
        $respuestasPorPregunta = [];
        // dd($respuestas);
        foreach ($respuestas as $respuesta) {
            $pregunta = $respuesta['Pregunta'];
            if (!isset($respuestasPorPregunta[$pregunta])) {
                $respuestasPorPregunta[$pregunta] = [];
            }
        if($respuesta['respuesta'] === '[]' || empty($respuesta)  ){
            $valorRespuesta = $respuesta['file'];
        }else{
            $valorRespuesta =  $respuesta['respuesta'];
        }
                $respuestasPorPregunta[$pregunta][] = $valorRespuesta;
        }      
        $respuestasSolo = [];    
        foreach ($respuestasPorPregunta as $pregunta => $respuestas) {
            $respuestasSolo[] = $respuestas;
        }

        if (count($respuestasSolo) > 1) {
            $respuestasSolo = array_map(null, ...$respuestasSolo);
        }else{

            $respuestasSolo = array_map(function ($valor) {
                return [$valor];
            }, $respuestasSolo[0]);
        } 
        // dd($respuestasSolo);
            $page_title = 'Descargar reporte de respuestas';
        $filename = strtolower(str_replace(' ','_',$survey->name));
        $answers = $survey->answers->reverse();
        $answers = $survey->answers;
        return view('surveyor.report.answersReport', compact('page_title','survey','answers','filename','data','respuestasSolo','respuestasPorPregunta'));
    }
}

