<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserCategory;

class CategoryController extends Controller
{
    public function categories(){
        $page_title = 'Categorias de encuestas';
        $empty_message = 'No hay categorias registradas';
        $categories = Category::latest()->paginate(getPaginate());

        return view('admin.category', compact('page_title', 'empty_message','categories'));
    }
    public function categoriesUser(){
        $page_title = 'Categorias de usuarios';
        $empty_message = 'No hay categorias registradas';
        $categories = UserCategory::latest()->paginate(getPaginate());

        return view('admin.categoryuser', compact('page_title', 'empty_message','categories'));
    }

    public function activate(Request $request)
    {
        $request->validate(['id' => 'required|integer']);
        $category = Category::findOrFail($request->id);
        $category->status = 1;
        $category->save();
        $notify[] = ['success', $category->name . ' has been activated'];
        return back()->withNotify($notify);
    }

    public function deactivate(Request $request)
    {
        $request->validate(['id' => 'required|integer']);
        $category = Category::findOrFail($request->id);
        $category->status = 0;
        $category->save();
        $notify[] = ['success', $category->name . ' has been disabled'];
        return back()->withNotify($notify);
    }

    public function activateUser(Request $request)
    {
        $request->validate(['id' => 'required|integer']);
        $category = UserCategory::findOrFail($request->id);
        $category->status = 1;
        $category->save();
        $notify[] = ['success', $category->name . ' has been activated'];
        return back()->withNotify($notify);
    }

    public function deactivateUser(Request $request)
    {
        $request->validate(['id' => 'required|integer']);
        $category = UserCategory::findOrFail($request->id);
        $category->status = 0;
        $category->save();
        $notify[] = ['success', $category->name . ' has been disabled'];
        return back()->withNotify($notify);
    }
    public function storeCategory(Request $request){

        $request->validate([
            'name' => 'required|string|max:190|unique:categories'
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->status = 1;
        $category->save();

        $notify[] = ['success', 'Category details has been added'];
        return back()->withNotify($notify);
    }
    public function storeCategoryUser(Request $request){

        $request->validate([
            'name' => 'required|string|max:190|unique:user_categories'
        ]);

        $category = new UserCategory();
        $category->name = $request->name;
        $category->status = 1;
        $category->save();

        $notify[] = ['success', 'Categoria creada'];
        return back()->withNotify($notify);
    }

    public function updateCategory(Request $request,$id){

        $request->validate([
            'name' => 'required|string|max:190|unique:categories,name,'.$id,
        ]);

        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();

        $notify[] = ['success', 'categoria actualizada'];
        return back()->withNotify($notify);
    }
    public function updateCategoryUser(Request $request,$id){

        $request->validate([
            'name' => 'required|string|max:190|unique:user_categories,name,'.$id,
        ]);

        $category = UserCategory::findOrFail($id);
        $category->name = $request->name;
        $category->save();

        $notify[] = ['success', 'categoria de usuario actualizada'];
        return back()->withNotify($notify);
    }

    public function searchCategory(Request $request)
    {

        $request->validate(['search' => 'required']);
        $search = $request->search;
        $page_title = 'Category Search - ' . $search;
        $empty_message = 'No categories found';
        $categories = Category::where('name', 'like',"%$search%")->paginate(getPaginate());

        return view('admin.category', compact('page_title', 'categories', 'empty_message'));
    }
    public function searchCategoryUser(Request $request)
    {

        $request->validate(['search' => 'required']);
        $search = $request->search;
        $page_title = 'Category Search - ' . $search;
        $empty_message = 'No existe coincidencias';
        $categories = UserCategory::where('name', 'like',"%$search%")->paginate(getPaginate());

        return view('admin.categoryuser', compact('page_title', 'categories', 'empty_message'));
    }
    
}
