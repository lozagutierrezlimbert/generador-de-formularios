<?php

namespace App\Http\Controllers;

use Image;
use App\Frontend;
use App\Answer;
use App\Language;
use App\Page;
use App\Subscriber;
use App\SupportAttachment;
use App\SupportMessage;
use App\Survey;
use App\SupportTicket;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class SiteController extends Controller
{
    public function __construct(){
        $this->activeTemplate = activeTemplate();
    }
    public function survey($id){

        if(auth()->check()){
            return redirect()->route('user.survey.questions',$id);
        }

        $survey = Survey::findOrFail($id);
        $page_title = $survey->name;
        $empty_message = 'Sin datos ';
        // dd($survey);
        return view($this->activeTemplate.'survey_share', compact('page_title', 'survey','empty_message'));
    }
    public function surveyPrivate($id){


        if(auth()->check()){
            return redirect()->route('user.survey.questions',$id);
        }
        $survey = Survey::findOrFail($id);
        $page_title = $survey->name;
        $empty_message = 'Sin datos';
    //    dd($survey);
        return view($this->activeTemplate.'survey_share_private', compact('page_title', 'survey','empty_message'));
    }
    public function index(){

        $count = Page::where('tempname',$this->activeTemplate)->where('slug','home')->count();
        if($count == 0){
            $page = new Page();
            $page->tempname = $this->activeTemplate;
            $page->name = 'HOME';
            $page->slug = 'home';
            $page->save();
        }

        $data['page_title'] = 'Home';
        $data['sections'] = Page::where('tempname',$this->activeTemplate)->where('slug','home')->firstOrFail();
        return view($this->activeTemplate . 'home', $data);
    }
    public function pages($slug)
    {
        $page = Page::where('tempname',$this->activeTemplate)->where('slug',$slug)->firstOrFail();
        $data['page_title'] = $page->name;
        $data['sections'] = $page;
        return view($this->activeTemplate . 'pages', $data);
    }

    public function subscriberStore(Request $request) {


        $validate = Validator::make($request->all(),[
            'email' => 'required|email|unique:subscribers',
        ]);

        if($validate->fails()){
            return response()->json($validate->errors());
        }

        Subscriber::create([
            'email' => $request->email
        ]);

        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->save();

        $notify = ['success' => 'Subscribe Successfully!'];
        return response()->json($notify);
    }

    public function contact()
    {
        $data['page_title'] = "Contactanos";
        return view($this->activeTemplate . 'contact', $data);
    }


    public function contactSubmit(Request $request)
    {
        $ticket = new SupportTicket();
        $message = new SupportMessage();

        $imgs = $request->file('attachments');
        $allowedExts = array('jpg', 'png', 'jpeg', 'pdf');

        $this->validate($request, [
            'attachments' => [
                'sometimes',
                'max:4096',
                function ($attribute, $value, $fail) use ($imgs, $allowedExts) {
                    foreach ($imgs as $img) {
                        $ext = strtolower($img->getClientOriginalExtension());
                        if (($img->getSize() / 1000000) > 2) {
                            return $fail("Images MAX  2MB ALLOW!");
                        }
                        if (!in_array($ext, $allowedExts)) {
                            return $fail("Only png, jpg, jpeg, pdf images are allowed");
                        }
                    }
                    if (count($imgs) > 5) {
                        return $fail("Maximum 5 images can be uploaded");
                    }
                },
            ],
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'subject' => 'required|max:100',
            'message' => 'required',
        ]);


        $random = getNumber();

        $ticket->user_id = auth()->id();
        $ticket->name = $request->name;
        $ticket->email = $request->email;


        $ticket->ticket = $random;
        $ticket->subject = $request->subject;
        $ticket->last_reply = Carbon::now();
        $ticket->status = 0;
        $ticket->save();

        $message->supportticket_id = $ticket->id;
        $message->message = $request->message;
        $message->save();

        $path = imagePath()['ticket']['path'];

        if ($request->hasFile('attachments')) {
            foreach ($request->file('attachments') as $image) {
                try {
                    $attachment = new SupportAttachment();
                    $attachment->support_message_id = $message->id;
                    $attachment->image = uploadImage($image, $path);
                    $attachment->save();

                } catch (\Exception $exp) {
                    $notify[] = ['error', 'Could not upload your ' . $image];
                    return back()->withNotify($notify)->withInput();
                }

            }
        }
        $notify[] = ['success', 'ticket created successfully!'];

        return redirect()->route('ticket.view', [$ticket->ticket])->withNotify($notify);
    }

    public function changeLanguage($lang = null)
    {
        $language = Language::where('code', $lang)->first();
        if (!$language) $lang = 'en';
        session()->put('lang', $lang);
        return back();
    }

    public function placeholderImage($size = null){
        if ($size != 'undefined') {
            $size = $size;
            $imgWidth = explode('x',$size)[0];
            $imgHeight = explode('x',$size)[1];
            $text = $imgWidth . '×' . $imgHeight;
        }else{
            $imgWidth = 150;
            $imgHeight = 150;
            $text = 'Undefined Size';
        }
        $fontFile = realpath('assets/font') . DIRECTORY_SEPARATOR . 'RobotoMono-Regular.ttf';
        $fontSize = round(($imgWidth - 50) / 8);
        if ($fontSize <= 9) {
            $fontSize = 9;
        }
        if($imgHeight < 100 && $fontSize > 30){
            $fontSize = 30;
        }

        $image     = imagecreatetruecolor($imgWidth, $imgHeight);
        $colorFill = imagecolorallocate($image, 144, 238, 144);
        $bgFill    = imagecolorallocate($image, 144, 238, 144);
        imagefill($image, 0, 0, $bgFill);
        $textBox = imagettfbbox($fontSize, 0, $fontFile, $text);
        $textWidth  = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);
        $textX      = ($imgWidth - $textWidth) / 2;
        $textY      = ($imgHeight + $textHeight) / 2;
        header('Content-Type: image/jpeg');
        imagettftext($image, $fontSize, 0, $textX, $textY, $colorFill, $fontFile, $text);
        imagejpeg($image);
        imagedestroy($image);
    }

//seccion para responder encuestas de manera anonima
public function surveyQuestions($id)
{
    // dd("redirigiendo para responder encuestas");
    $page_title = 'Preguntas de encuestas';
    $empty_message = 'Encuesta sun datos';
    $survey = Survey::findOrFail($id);



    if (count($survey->questions) <= 0) {
        $notify[] = ['error', 'No hay ninguna pregunta disponible para esta encuesta.'];
        return back()->withNotify($notify);
    }


    return view($this->activeTemplate.'question', compact('page_title', 'survey', 'empty_message'));
}

public function surveyQuestionsAnswers(Request $request, $id){
 
    $request->validate([
        "answer" => "required|array|min:1",
        "answer.*" => "required_with:answer",
    ]);

    $survey = Survey::where('id',$id)->with('questions')->first();

    $answers = $request['answer'];
    foreach ($survey->questions as $item) {
        $surveyAns = @$answers[$item->id];

        if (!$surveyAns) {
            $notify[] = ['error','
            Por favor responda todas las preguntas'];
            return back()->withNotify($notify);
        }
    }
 
    foreach ($answers as $key => $item) {
        $custom_ans = $item['c']??null;

        if($custom_ans){
            unset($item['c']);
            $file = $custom_ans;
            $filename = time() . '_' . 'public' . '.' . $file->getClientOriginalExtension();
            $location = 'assets/images/answers/' . $filename;
            $file->move(public_path($location), $filename); 
            $custom_ans = $filename;
        }
        $create_ans = new Answer();
        $create_ans->surveyor_id = $survey->surveyor->id;
        $create_ans->survey_id = $survey->id;
        $create_ans->user_id = 0;
        $create_ans->question_id = $key;
        $create_ans->answer = array_values($item);
        $create_ans->custom_answer = $custom_ans;
        $create_ans->save();
    }
    $notify[] = ['success', 'Has completado esta encuesta exitosamente'];
    return redirect()->route('home')->withNotify($notify);
}


}
