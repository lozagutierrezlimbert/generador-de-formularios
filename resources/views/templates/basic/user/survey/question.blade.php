@extends($activeTemplate.'layouts.master')

@section('content')
    <div class="survey-area mt-30" style="background-color: {{ $survey->color }} ; font-family: {{ $survey->type_text }}" >
        <div class="panel-card-header section--bg text-white">
            <div class="panel-card-title"><i class="lar la-question-circle"></i> @lang('Responde todas las preguntas')</div>
        </div>

        <div class="panel-body multi_step_form">
            <div class="mb-2 step-tracker"></div>
            <form id="msform" method="POST" action="{{route('user.survey.questions.answers',$survey->id)}}" onsubmit="return submitUserForm();" enctype="multipart/form-data">
                @csrf
                <ul id="progressbar">
                    @foreach ($survey->questions as $k => $item)
                        <li class="@if($k == 0) active done @else @endif"><span></span></li>
                    @endforeach
                </ul>
                @foreach($survey->questions as $key => $item)
                    <fieldset>

                        <h3 class="title">{{__($item->question)}}</h3>

                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <div class="radio-wrapper">
                                 @if($item->options)
                                        @foreach ($item->options as $k => $data)
                                            <div>      
                                            @if($item->type == 1)<!-- opcion unica -->
                                            <input type="radio" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][]" value="{{$data}}">
                                            <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                            @elseif($item->type == 2) <!-- opcion multiple -->
                                            <input type="checkbox" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][]" value="{{$data}}">
                                            <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                            @elseif($item->type == 5)<!-- casilla de verificacion -->
                                            <input type="checkbox" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][]" value="{{$data}}">
                                            <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                            @endif
                                            </div>
                                            @endforeach                              
                                            @if($item->type == 3)
                                            <label for="">{{__($item->custom_question)}} @if($item->custom_input_type == 1) <span class="text--danger">*(@lang('respuesta requerida'))</span> @endif</label>
                                            <input type="text"  class="form-control mb-4" placeholder="@lang('Ingrese su respuesta')" name="answer[{{$item->id}}][]" @if($item->custom_input_type == 1) required @endif>
                                            @elseif($item->type == 4)
                                            <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                            <textarea class="form-control mb-4" placeholder="@lang('Ingrese su respuesta')" name="answer[{{$item->id}}][]" rows="5" @if($item->custom_input_type == 1) required @endif></textarea>
                                            @elseif($item->type == 6)
                                            <div class="input-group">
                                            <select id="answer-{{$key}}" name="answer[{{$item->id}}][]" class="form-select"  
                                            style="width: 200px; height: 30px; background-color: lightgray; color: darkblue;">
                                            @foreach ($item->options as $k => $data)
                                            <option value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                            </select>    
                                            </div> 
                                            @elseif($item->type == 7)
                                            <div class="input-group">
                                                <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                                <input type="file" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][c]" >
                                            </div>
                                            @elseif($item->type == 8)
                                            <div class="input-group">
                                                <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                                <input type="date" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][]" >
                                            </div>
                                            @elseif($item->type == 9)
                                            <div class="input-group">
                                                <label for="answer-{{$key}}-{{$k}}">{{$data}}</label>
                                                <input type="time" id="answer-{{$key}}-{{$k}}" name="answer[{{$item->id}}][]" >
                                            </div>
                                            @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <a href="#0" class="@if($key > 0) previous @endif action-button previous_button text-center">@lang('Atras')</a>

                        @if($loop->last)
                            <button type="submit" class="action-button" id="disable-btn">@lang('Terminar')</button>
                            <p class="mt-2">@lang('Esta es la última pregunta. Después de hacer clic en el botón Finalizar, se enviarán sus respuestas y no tendrá posibilidad de cambiarlas. Si no puede enviar, verifique todas sus preguntas anteriores, es posible que se pierda alguna pregunta para responder')</p>
                        @else
                            <button type="button" class="next action-button">@lang('Siguiente')</button>
                        @endif

                    </fieldset>
                @endforeach
        </div>
    </div>
@endsection

@push('script')
<script src="{{asset($activeTemplateTrue.'js/jquery.easing.min.js')}}"></script>

<script>
    "use strict";

    (function ($) {

        calculateStepTracker()
        function calculateStepTracker(){
            let totalLength = $('#progressbar li').length;
            let doneStep    = $('#progressbar li.done').length;

            $('.step-tracker').text(`${doneStep}/${totalLength}`)
        }

            //* Form js
        function verificationForm() {
            //jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches

            $(".next").on('click',function () {
                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active done");



                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50) + "%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'position': 'absolute'
                        });
                        next_fs.css({
                            'left': left,
                            'opacity': opacity
                        });
                    },
                    duration: 0,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
                calculateStepTracker();
            });

            $(".previous").on('click',function () {
                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //de-activate current step on progressbar
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active done");

                //show the previous fieldset
                previous_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1 - now) * 50) + "%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'left': left
                        });
                        previous_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'opacity': opacity,
                            'position': 'relative'
                        });
                    },
                    duration: 0,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
                calculateStepTracker();
            });
        };
        /*Function Calls*/
        verificationForm();


    })(jQuery);

    function submitUserForm() {
            $("#disable-btn").addClass('d-none');
            return true;
        }
</script>
@endpush



