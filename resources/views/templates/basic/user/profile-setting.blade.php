@extends($activeTemplate.'layouts.master')
@section('content')

    <div class="user-profile-area mt-30">
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row justify-content-center mb-30-none">
                <div class="col-xl-5 col-md-12 col-sm-12 mb-30">
                    <div class="panel panel-default">
                        <div class="panel-heading d-flex flex-wrap align-items-center justify-content-between">
                            <div class="panel-title"><i class="las la-user"></i> @lang('Detalles de usuario')</div>
                            <div class="panel-options">
                                <a href="#" data-rel="collapse"><i class="las la-chevron-circle-down"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body-inner">
                                <div class="profile-thumb-area text-center">
                                    <div class="profile-thumb">
                                        <div class="image-preview bg_img" data-background="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->image,imagePath()['profile']['user']['size']) }}"></div>
                                    </div>
                                    <div class="profile-edit">
                                        <input type="file" name="image" id="imageUpload" class="upload" accept=".png, .jpg, .jpeg">
                                        <div class="rank-label">
                                            <label for="imageUpload" class="imgUp bg--primary">
                                                @lang('Cargar imagen')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="profile-content-area text-center mt-20">
                                        <h3 class="name">{{__($user->fullname)}}</h3>
                                        <h5 class="email">@lang('Correo') : {{__($user->email)}}</h5>
                                       
                                       
                                        <h5 class="reference">@lang('Estado') : <span class="badge badge--primary text-white">@lang('Active')</span></h5>

                                        <a href="#0" class="btn btn--success text-white btn-rounded btn-block btn-icon icon-left mt-20"
                                            data-clipboard-text="bbakaHwKsaMc">
                                            <i class="las la-clipboard-check"></i> @lang('Estado : Activo')
                                        </a>

                                        <div class="profile-footer-btn mt-10">
                                            <div class="row mb-10-none">
                                                <div class="col-md-6 col-sm-12 mb-10">
                                                    <a href="{{ route('user.change-password') }}"
                                                        class="btn btn--primary  text-white btn-rounded btn-block btn-icon icon-left"><i
                                                            class="las la-lock"></i> @lang('Cambiar Contraseña')</a>
                                                </div>
                                                <div class="col-md-6 col-sm-12 mb-10">
                                                    <a href="{{route('user.survey')}}" class="btn btn--primary text-white btn-rounded btn-block btn-icon icon-left"><i class="lar la-question-circle"></i> @lang('Empezar Formulario')</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-md-12 col-sm-12 mb-30">
                    <div class="panel panel-default">
                        <div class="panel-heading d-flex flex-wrap align-items-center justify-content-between">
                            <div class="panel-title"><i class="las la-user"></i> @lang('Datos del Usuario')</div>
                            <div class="panel-options-form">
                                <a href="#" data-rel="collapse"><i class="las la-chevron-circle-down"></i></a>
                            </div>
                        </div>
                        <div class="panel-form-area">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Nombre') <span class="text-danger">*</span></label>
                                    <input type="text" name="firstname" class="form-control" value="{{$user->firstname}}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Apellido') <span class="text-danger">*</span></label>
                                    <input type="text" name="lastname" class="form-control" value="{{$user->lastname}}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Correo')</label>
                                    <input type="email" class="form-control" value="{{$user->email}}" readonly>
                                </div>
  
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Categoria') <span class="text-danger">*</span></label>
                                    <select name="category_id" class="form-control " required>
                                            <option value="">@lang('Selecciona una categoría')</option>
                                            @foreach($userCategories as $category)
                                                <option value="{{ $category->id }}" @if($user->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                <div class="col-lg-12 form-group text-center">
                                    <button type="submit" class="submit-btn">@lang('Actualizar')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

