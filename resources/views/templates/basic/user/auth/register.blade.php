@extends($activeTemplate.'layouts.frontend')
@section('content')
@include($activeTemplate.'partials.breadcrumb')

    <section class="account-section ptb-80">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-10">
                    <div class="account-form-area bg-overlay-black section--bg">
                        <div class="account-logo-area text-center">
                            <div class="account-logo">
                                <a href="{{route('home')}}"><img src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="@lang('logo')"></a>
                            </div>
                        </div>
                        <div class="account-header text-center">
                            <h2 class="title">@lang('Registrate ahora')</h2>
                            <h3 class="sub-title">@lang('Ya tienes una cuenta')? <a href="{{route('user.login')}}">@lang('Inicia sesión')</a></h3>
                        </div>
                        <form class="account-form" action="{{ route('user.register') }}" method="POST" onsubmit="return submitUserForm();">
                            @csrf
                            <div class="row ml-b-20">
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Nombre')*</label>
                                    <input type="text" class="form-control form--control" name="firstname" value="{{ old('firstname') }}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Apellido')*</label>
                                    <input type="text" class="form-control form--control" name="lastname" value="{{ old('lastname') }}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Nombre de usuario')*</label>
                                    <input type="text" class="form-control form--control" name="username" value="{{ old('username') }}" required>
                                </div>
                             
                  
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Correo')*</label>
                                    <input type="email" class="form-control form--control" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Contraseña')*</label>
                                    <input type="password" class="form-control form--control" name="password" required>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>@lang('Confirmar contraseña')*</label>
                                    <input type="password" class="form-control form--control" name="password_confirmation" required>
                                </div>
                                <div class="col-lg-6 form-group mx-auto text-center">
                                <label>@lang('Seleccione una Unidad o Repartición')*</label>
                                <select name="category_id" class="form-control form--control" required>
                                    <option value="">@lang('Selecciona Unidad o repartición')</option>
                                    @foreach($userCategories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                </div>
                                   <div class="col-lg-12 form-group text-center mt-2">
                                    <button type="submit" class="submit-btn">@lang('Registrar ahora')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
