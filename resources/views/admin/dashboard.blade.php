@extends('admin.layouts.app')

@section('panel')
 

    <div class="row mb-none-30">
        <div class="col-xl-3 col-lg-4 col-sm-6 mb-30">
            <div class="dashboard-w1 bg--primary b-radius--10 box-shadow">
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="numbers">
                        <span class="amount">{{$widget['total_users']}}</span>
                    </div>
                    <div class="desciption">
                        <span class="text--small">@lang('Todos los usuarios')</span>
                    </div>
                    <a href="{{route('admin.users.all')}}" class="btn btn-sm text--small bg--white text--black box--shadow3 mt-3">@lang('Ver todo')</a>
                </div>
            </div>
        </div>

    </div>

    <div class="row my-3">
        <div class="col-md-12">
            <h4>@lang('Todos los Formularios')</h4>
        </div>
    </div>

    <div class="row mb-none-30">
          <div class="col-xl-4 col-md-6 mb-30">
            <div class="widget bb--3 border--primary b-radius--10 bg--white p-4 box--shadow2 has--link">
              <a href="{{route('admin.manage.survey.approved')}}" class="item--link"></a>
              <div class="widget__icon b-radius--rounded bg--primary"><i class="las la-user-plus"></i></div>
              <div class="widget__content">
                <p class="text-uppercase text-muted"></p>
                <h2 class="text--primary font-weight-bold">{{$widget['approved_surveys']}}</h2>
              </div>
              <div class="widget__arrow">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
          </div>
        </div>

    <div class="row mb-none-30 mt-5">

    <div class="col-xl-12 mb-30">
        <div class="card ">
            <div class="card-header">
                <h6 class="card-title mb-0">@lang('Lista de usuarios')</h6>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive--sm table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                        <tr>
                            <th scope="col">@lang('Usuarios')</th>
                            <th scope="col">@lang('Nombre de Usuario')</th>
                            <th scope="col"  class="text-center">@lang('Correo')</th>
                            <th scope="col">@lang('Opciones')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($latestUser as $user)
                            <tr>
                                <td data-label="@lang('Usuario')">
                                    <div class="user">
                                        <div class="thumb"><img src="{{ getImage(imagePath()['profile']['user']['path'].'/'.$user->image,imagePath()['profile']['user']['size'])}}" alt="@lang('image')"></div>
                                        <span class="name">{{$user->fullname}}</span>
                                    </div>
                                </td>
                                <td data-label="@lang('Nombre de Usuario')"><a href="{{ route('admin.users.detail', $user->id) }}">{{ $user->username }}</a></td>
                                <td data-label="@lang('Correo')" class="text-center" >{{ $user->email }}</td>
                                <td data-label="@lang('Opciones')">
                                    <a href="{{ route('admin.users.detail', $user->id) }}" class="icon-btn" data-toggle="tooltip" data-original-title="@lang('Detalles')">
                                        <i class="las la-desktop text--shadow"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                        @endforelse

                        @forelse($latestSurveyor as $surveyor)
                            <tr>
                                <td data-label="@lang('User')">
                                    <div class="user">
                                        <div class="thumb"><img src="{{ getImage(imagePath()['profile']['surveyor']['path'].'/'.$surveyor->image,imagePath()['profile']['surveyor']['size'])}}" alt="@lang('image')"></div>
                                        <span class="name">{{$surveyor->fullname}}</span>
                                    </div>
                                </td>
                                <td data-label="@lang('Username')"><a href="{{ route('admin.surveyors.detail', $surveyor->id) }}">{{ $surveyor->username }}</a></td>
                                <td data-label="@lang('Email')"  class="text-center" >{{ $surveyor->email }}</td>
                                <td data-label="@lang('Action')">
                                    <a href="{{ route('admin.surveyors.detail', $surveyor->id) }}" class="icon-btn" data-toggle="tooltip" data-original-title="@lang('Detalles')">
                                        <i class="las la-desktop text--shadow"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



        <div class="col-xl-6 mb-30">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">@lang('Respuestas mensuales')</h5>
                    <div id="survey-response"></div>
                </div>
            </div>
        </div>

    </div>

  

 




@endsection
@push('script')

    <script src="{{asset('assets/admin/js/vendor/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/vendor/chart.js.2.8.0.js')}}"></script>


    <script>

          var options = {
              series: [{
                  name: 'Total Deposit',
                  data: [
                    @foreach($report['months'] as $month)
                      {{ getAmount(@$depositsMonth->where('months',$month)->first()->depositAmount) }},
                    @endforeach
                  ]
              }, {
                  name: 'Total Withdraw',
                  data: [
                    @foreach($report['months'] as $month)
                      {{ getAmount(@$withdrawalMonth->where('months',$month)->first()->withdrawAmount) }},
                    @endforeach
                  ]
              }],
              chart: {
                  type: 'bar',
                  height: 400,
                  toolbar: {
                      show: false
                  }
              },
              plotOptions: {
                  bar: {
                      horizontal: false,
                      columnWidth: '50%',
                      endingShape: 'rounded'
                  },
              },
              dataLabels: {
                  enabled: true
              },
              stroke: {
                  show: true,
                  width: 2,
                  colors: ['transparent']
              },
              xaxis: {
                  categories: @json($report['months']->flatten()),
              },
              yaxis: {
                  title: {
                      text: "{{__($general->cur_sym)}}",
                      style: {
                          color: '#7c97bb'
                      }
                  }
              },
              grid: {
                  xaxis: {
                      lines: {
                          show: false
                      }
                  },
                  yaxis: {
                      lines: {
                          show: false
                      }
                  },
              },
              fill: {
                  opacity: 1
              },
              tooltip: {
                  y: {
                      formatter: function (val) {
                          return "{{__($general->cur_sym)}}" + val + " "
                      }
                  }
              }
          };

          var chart = new ApexCharts(document.querySelector("#apex-bar-chart"), options);
          chart.render();



  </script>


    <script>
        // apex-line chart
        var options = {
            chart: {
                height: 430,
                type: "area",
                toolbar: {
                    show: false
                },
                dropShadow: {
                    enabled: true,
                    enabledSeries: [0],
                    top: -2,
                    left: 0,
                    blur: 10,
                    opacity: 0.08
                },
                animations: {
                    enabled: true,
                    easing: 'linear',
                    dynamicAnimation: {
                        speed: 1000
                    }
                },
            },
            dataLabels: {
                enabled: false
            },
            series: [
                {
                    name: "Series 1",
                    data: @json($withdrawals['per_day_amount']->flatten())
                }
            ],
            fill: {
                type: "gradient",
                gradient: {
                    shadeIntensity: 1,
                    opacityFrom: 0.7,
                    opacityTo: 0.9,
                    stops: [0, 90, 100]
                }
            },
            xaxis: {
                categories: @json($withdrawals['per_day']->flatten())
            },
            grid: {
                padding: {
                    left: 5,
                    right: 5
                },
                xaxis: {
                    lines: {
                        show: false
                    }
                },
                yaxis: {
                    lines: {
                        show: false
                    }
                },
            },
        };

        var chart2 = new ApexCharts(document.querySelector("#withdraw-line"), options);

        chart2.render();

    </script>


    <script>
        // Survey Response
        var options = {
          chart: {
            height: 430,
            type: "area",
            toolbar: {
              show: false
            },
            dropShadow: {
              enabled: true,
              enabledSeries: [0],
              top: -2,
              left: 0,
              blur: 10,
              opacity: 0.08
            },
            animations: {
              enabled: true,
              easing: 'linear',
              dynamicAnimation: {
                speed: 1000
              }
            },
          },
          dataLabels: {
            enabled: false
          },
          series: [
            {
              name: "@lang('Survey')",
              data: @php echo json_encode($survey_all) @endphp,
            }
          ],
          fill: {
            type: "gradient",
            gradient: {
              shadeIntensity: 1,
              opacityFrom: 0.7,
              opacityTo: 0.9,
              stops: [0, 90, 100]
            }
          },
          xaxis: {
            categories: @php echo json_encode($month_survey) @endphp,
          },
          grid: {
            padding: {
              left: 5,
              right: 5
            },
            xaxis: {
              lines: {
                  show: false
              }
            },
            yaxis: {
              lines: {
                  show: false
              }
            },
          },
        };

        var chart3 = new ApexCharts(document.querySelector("#survey-response"), options);

        chart3.render();

    </script>



    <script>

        var ctx = document.getElementById('userBrowserChart');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: @json($chart['user_browser_counter']->keys()),
                datasets: [{
                    data: {{ $chart['user_browser_counter']->flatten() }},
                    backgroundColor: [
                        '#ff7675',
                        '#6c5ce7',
                        '#ffa62b',
                        '#ffeaa7',
                        '#D980FA',
                        '#fccbcb',
                        '#45aaf2',
                        '#05dfd7',
                        '#FF00F6',
                        '#1e90ff',
                        '#2ed573',
                        '#eccc68',
                        '#ff5200',
                        '#cd84f1',
                        '#7efff5',
                        '#7158e2',
                        '#fff200',
                        '#ff9ff3',
                        '#08ffc8',
                        '#3742fa',
                        '#1089ff',
                        '#70FF61',
                        '#bf9fee',
                        '#574b90'
                    ],
                    borderColor: [
                        'rgba(231, 80, 90, 0.75)'
                    ],
                    borderWidth: 0,

                }]
            },
            options: {
                aspectRatio: 1,
                responsive: true,
                maintainAspectRatio: true,
                elements: {
                    line: {
                        tension: 0 // disables bezier curves
                    }
                },
                scales: {
                    xAxes: [{
                        display: false
                    }],
                    yAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });



        var ctx = document.getElementById('userOsChart');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: @json($chart['user_os_counter']->keys()),
                datasets: [{
                    data: {{ $chart['user_os_counter']->flatten() }},
                    backgroundColor: [
                        '#ff7675',
                        '#6c5ce7',
                        '#ffa62b',
                        '#ffeaa7',
                        '#D980FA',
                        '#fccbcb',
                        '#45aaf2',
                        '#05dfd7',
                        '#FF00F6',
                        '#1e90ff',
                        '#2ed573',
                        '#eccc68',
                        '#ff5200',
                        '#cd84f1',
                        '#7efff5',
                        '#7158e2',
                        '#fff200',
                        '#ff9ff3',
                        '#08ffc8',
                        '#3742fa',
                        '#1089ff',
                        '#70FF61',
                        '#bf9fee',
                        '#574b90'
                    ],
                    borderColor: [
                        'rgba(0, 0, 0, 0.05)'
                    ],
                    borderWidth: 0,

                }]
            },
            options: {
                aspectRatio: 1,
                responsive: true,
                elements: {
                    line: {
                        tension: 0 // disables bezier curves
                    }
                },
                scales: {
                    xAxes: [{
                        display: false
                    }],
                    yAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false,
                }
            },
        });


        // Donut chart
        var ctx = document.getElementById('userCountryChart');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: @json($chart['user_country_counter']->keys()),
                datasets: [{
                    data: {{ $chart['user_country_counter']->flatten() }},
                    backgroundColor: [
                        '#ff7675',
                        '#6c5ce7',
                        '#ffa62b',
                        '#ffeaa7',
                        '#D980FA',
                        '#fccbcb',
                        '#45aaf2',
                        '#05dfd7',
                        '#FF00F6',
                        '#1e90ff',
                        '#2ed573',
                        '#eccc68',
                        '#ff5200',
                        '#cd84f1',
                        '#7efff5',
                        '#7158e2',
                        '#fff200',
                        '#ff9ff3',
                        '#08ffc8',
                        '#3742fa',
                        '#1089ff',
                        '#70FF61',
                        '#bf9fee',
                        '#574b90'
                    ],
                    borderColor: [
                        'rgba(231, 80, 90, 0.75)'
                    ],
                    borderWidth: 0,

                }]
            },
            options: {
                aspectRatio: 1,
                responsive: true,
                elements: {
                    line: {
                        tension: 0 // disables bezier curves
                    }
                },
                scales: {
                    xAxes: [{
                        display: false
                    }],
                    yAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });
    </script>
@endpush
