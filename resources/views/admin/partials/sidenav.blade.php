<div class="sidebar capsule--rounded bg_img overlay--dark"
     data-background="{{getImage('assets/admin/images/sidebar/3.jpg','400x800')}}">
    <button class="res-sidebar-close-btn"><i class="las la-times"></i></button>
    <div class="sidebar__inner">
        <div class="sidebar__logo">
            <a href="{{route('admin.dashboard')}}" class="sidebar__main-logo"><img
                    src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="@lang('image')"></a>
            <a href="{{route('admin.dashboard')}}" class="sidebar__logo-shape"><img
                    src="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" alt="@lang('image')"></a>
            <button type="button" class="navbar__expand"></button>
        </div>

        <div class="sidebar__menu-wrapper" id="sidebar__menuWrapper">
            <ul class="sidebar__menu">
                <li class="sidebar-menu-item {{menuActive('admin.dashboard')}}">
                    <a href="{{route('admin.dashboard')}}" class="nav-link ">
                        <i class="menu-icon las la-home"></i>
                        <span class="menu-title">@lang('Panel')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item {{menuActive('admin.category*')}}">
                    <a href="{{route('admin.category')}}" class="nav-link ">
                        <i class="menu-icon las la-tags"></i>
                        <span class="menu-title">@lang('Tipo de Formulario')</span>
                    </a>
                </li> 
                <li class="sidebar-menu-item {{menuActive('admin.categoryUser*')}}">
                    <a href="{{route('admin.categoryUser')}}" class="nav-link ">
                        <i class="menu-icon las la-tags"></i>
                        <span class="menu-title">@lang('Categoria de usuarios')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item sidebar-dropdown">
                    <a href="javascript:void(0)" class="{{menuActive('admin.manage.survey*',3)}}">
                        <i class="menu-icon lar la-question-circle"></i>
                        <span class="menu-title">@lang('Gestion Formularios') </span>
                        @if($pending_survey_count > 0)
                            <span class="menu-badge pill bg--primary ml-auto">
                                <i class="fa fa-exclamation"></i>
                            </span>
                        @endif
                    </a>
                    <div class="sidebar-submenu {{menuActive('admin.manage.survey*',2)}} ">
                        <ul>
                            <li class="sidebar-menu-item {{menuActive('admin.manage.survey.approved')}}">
                                <a href="{{route('admin.manage.survey.approved')}}" class="nav-link">
                                    <i class="menu-icon las la-dot-circle"></i>
                                    <span class="menu-title">@lang('Lista de Formularios')</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <!-- <li class="sidebar-menu-item sidebar-dropdown">
                    <a href="javascript:void(0)" class="{{menuActive('admin.surveyors*',3)}}">
                        <i class="menu-icon las la-users"></i>
                        <span class="menu-title">@lang('Encuestadores')</span>
                    </a>
                    <div class="sidebar-submenu {{menuActive('admin.surveyors*',2)}} ">
                        <ul>
                            <li class="sidebar-menu-item {{menuActive('admin.surveyors.all')}} ">
                                <a href="{{route('admin.surveyors.all')}}" class="nav-link">
                                    <i class="menu-icon las la-dot-circle"></i>
                                    <span class="menu-title">@lang('Lista de encuestadores')</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li> -->

                <li class="sidebar-menu-item sidebar-dropdown">
                    <a href="javascript:void(0)" class="{{menuActive('admin.users*',3)}}">
                        <i class="menu-icon las la-users"></i>
                        <span class="menu-title">@lang('Usuarios')</span>
                    </a>
                    <div class="sidebar-submenu {{menuActive('admin.users*',2)}} ">
                        <ul>
                            <li class="sidebar-menu-item {{menuActive('admin.users.all')}} ">
                                <a href="{{route('admin.users.all')}}" class="nav-link">
                                    <i class="menu-icon las la-dot-circle"></i>
                                    <span class="menu-title">@lang('Todos los usuarios')</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- sidebar end -->
