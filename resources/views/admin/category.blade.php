
@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12 col-md-12 mb-30">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive--sm table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('nro')</th>
                                <th scope="col">@lang('Nombre')</th>
                                <th scope="col">@lang('Opciones')</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @forelse ($categories as $item)
                                <tr>
                                    <td data-label="@lang('Nro')">{{ $loop->index+1 }}</td>
                                    <td data-label="@lang('Nombre')">{{ $item->name }}</td>
                                    <td data-label="@lang('Opciones')">
                                        <a href="#" class="icon-btn  updateBtn" data-route="{{ route('admin.category.update',$item->id) }}" data-resourse="{{$item}}" data-toggle="modal" data-target="#updateBtn" ><i class="la la-pencil-alt"></i></a>
                                        @if ($item->status == 0)
                                            <button type="button"
                                                    class="icon-btn btn--success ml-1 activateBtn"
                                                    data-toggle="modal" data-target="#activateModal"
                                                    data-id="{{$item->id}}"
                                                    data-original-title="@lang('Enable')">
                                                <i class="la la-eye"></i>
                                            </button>
                                        @else
                                            <button type="button"
                                                    class="icon-btn btn--danger ml-1 deactivateBtn"
                                                    data-toggle="modal" data-target="#deactivateModal"
                                                    data-id="{{$item->id}}"
                                                    data-original-title="@lang('Disable')">
                                                <i class="la la-eye-slash"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ $empty_message }}</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $categories->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>

{{-- Add METHOD MODAL --}}
<div id="addModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('Agregar nueva categoria')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.category.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>@lang('Nombre')</label>
                        <input type="text"class="form-control" placeholder="@lang('Ingrese nombre')" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Cerrar')</button>
                    <button type="submit" class="btn btn--primary">@lang('Guardar')</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Update METHOD MODAL --}}
<div id="updateBtn" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('Actualizar categoria')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="edit-route" method="POST">
                @csrf

                <div class="modal-body">
                    <div class="form-group">
                        <label>@lang('Nombre')</label>
                        <input type="text"class="form-control name" placeholder="@lang('Ingrese nombre')" name="name" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Cerrar')</button>
                    <button type="submit" class="btn btn--primary">@lang('Actualizar')</button>
                </div>
            </form>
        </div>
    </div>
</div>

    {{-- ACTIVATE METHOD MODAL --}}
    <div id="activateModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('activar categoria ? ')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.category.activate') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id">
                    <div class="modal-body">
                        <p>@lang('Esta seguro de activar esta categoria ?')</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Cerrar')</button>
                        <button type="submit" class="btn btn--primary">@lang('Activar')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- DEACTIVATE METHOD MODAL --}}
    <div id="deactivateModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Confirmar desactivar categoria')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.category.deactivate') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id">
                    <div class="modal-body">
                        <p>@lang('Esta seguro de desabilitar esta categoria')</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Cerrar')</button>
                        <button type="submit" class="btn btn--danger">@lang('Desabilitar')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('breadcrumb-plugins')
        <div class="btn--search--area">
            <div class="item">
                <a href="javascript:void(0)" class="btn btn-lg btn--primary box--shadow1 text--small addBtn"><i class="fa fa-fw fa-plus"></i>@lang('Agregar nuevo')</a>
            </div>
            <div class="item">
                @if(request()->routeIs('admin.category'))
                    <form action="{{ route('admin.category.search') }}" method="GET" class="form-inline float-sm-right bg--white">
                        <div class="input-group has_append">
                            <input type="text" name="search" class="form-control" placeholder="@lang('Nombre de la catogoria')" value="{{ $search ?? '' }}">
                            <div class="input-group-append">
                                <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                @else
                    <form action="{{ route('admin.category.search') }}" method="GET" class="form-inline float-sm-right bg--white">
                        <div class="input-group has_append">
                            <input type="text" name="search" class="form-control" placeholder="@lang('Category Name')" value="{{ $search ?? '' }}">
                            <div class="input-group-append">
                                <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    @endpush
@endsection

@push('script')
<script>
    'use strict';

    (function ($) {
        $('.addBtn').on('click', function () {
            var modal = $('#addModal');
            modal.modal('show');
        });

        $('.updateBtn').on('click', function () {
            var modal = $('#updateBtn');

            var resourse = $(this).data('resourse');

            var route = $(this).data('route');
            $('.name').val(resourse.name);
            $('.edit-route').attr('action',route);

        });

        $('.activateBtn').on('click', function () {
                var modal = $('#activateModal');
                modal.find('input[name=id]').val($(this).data('id'));
            });

            $('.deactivateBtn').on('click', function () {
                var modal = $('#deactivateModal');
                modal.find('input[name=id]').val($(this).data('id'));
            });
    })(jQuery);
</script>
@endpush
