@extends('surveyor.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--md  table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                            <tr>
                                <th scope="col">@lang('Nro')</th>
                                <th scope="col">@lang('Categoria')</th>
                                <th scope="col">@lang('Fecha de creación')</th>
                                <th scope="col">@lang('Opciones')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($userCategories as $category)
                            <tr>
                            <td data-label="@lang('Nro')">{{$loop->index+1}}</td>
                                <td data-label="@lang('Categoria')">{{ $category->name }}</a></td>
                        
                                <td data-label="@lang('Fecha de registro')">{{ showDateTime($category->created_at) }}</td>
                                <td data-label="@lang('Accion')">

                                @php
                                $usersInCategory = $category->users; 
                                $usersInSurvey = $survey->users;
                                //esto es true todos los usuarios de esta categoria han respondido a este encuesta 
                                $allIdsInSurvey = collect($usersInCategory)->pluck('id')->diff($usersInSurvey)->isEmpty();
                                // esto es true si almenos un usuario de la categoria ha respondido la encuesta
                                $atLeastOneInSurvey = collect($usersInCategory)->pluck('id')->intersect($usersInSurvey)->isNotEmpty();
                              
                                $usersWithResponses = $category->users()
                                        ->whereHas('answers', function ($query) use ($survey) {
                                            $query->where('survey_id', $survey->id);
                                        })
                                        ->count();
                                  
                                @endphp
                                @if($allIdsInSurvey)
                                <!-- se mostrara desabilitado en el caso de que todos hayan respondido esta encuesta eso quiere decir que esta inabilitado  -->
                                <button class="btn btn-danger btn-status" data-value = false data-id="{{ $category->id }}" data-survey="{{ json_encode($survey) }}"> Deshabilitado</button>
                                @elseif($atLeastOneInSurvey)
                                <button class="btn btn-warning btn-status" data-value = conres data-id="{{ $category->id }}" data-survey="{{ json_encode($survey) }}"> Con Respuestas</button>
                                @else
                                <button class="btn btn-success btn-status" data-value = true data-id="{{ $category->id }}" data-survey="{{ json_encode($survey) }}">Habilitado</button>
                                @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                            @endforelse
                           

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ $users->links('admin.partials.paginate') }}
                </div>
            </div>
        </div>


    </div>
@endsection
@push('script')
<script src="{{asset($activeTemplateTrue.'js/jquery.easing.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>


<script>
    // import axios from 'axios';
    document.addEventListener('DOMContentLoaded', function () {
    // Agregar el evento click a un elemento superior (en este caso, el body)
    document.body.addEventListener('click', function (event) {
        const target = event.target;

        // Verificar si el clic se hizo en un botón con la clase .btn-status
        if (target && target.classList.contains('btn-status')) {
            const categoryId = target.getAttribute('data-id');
            const dataValue = target.getAttribute('data-value');
            const surveyData = JSON.parse(target.getAttribute('data-survey'));
            const url = '{{ route('surveyor.update.user.status') }}';
            const data = {
                category_id: categoryId,
                dataValue: dataValue,
                survey: surveyData // Aquí incluimos la información de la encuesta
            };
            // Enviar una solicitud POST al servidor para cambiar el estado del usuario
            axios.post(url, data)
                .then(response => {
                    if (response.data.success) {
                        // Actualizar el estado del botón
                        if (!response.data.is_enabled) {
                            target.classList.remove('btn-success');
                            target.classList.add('btn-danger');
                            target.textContent = 'Deshabilitado';
                            location.reload();
                        } else {
                            target.classList.remove('btn-danger');
                            target.classList.add('btn-success');
                            target.textContent = 'Habilitado';
                            location.reload();
                        }

                        if (response.data.conres) {
                            target.classList.remove('btn-warning');
                            target.classList.add('btn-success');
                            target.textContent = 'Habilitado';
                            location.reload();
                        }
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        }
    });
});


</script>
@endpush



