@extends('surveyor.layouts.app')
@section('panel')
    <div class="row">
        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--md  table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th scope="col">@lang('Nro')</th>
                                    <th scope="col">@lang('Pregunta')</th>
                                    <th scope="col">@lang('Opciones')</th>
                                    <th scope="col">@lang('Tipo')</th>
                                    <!-- <th scope="col">@lang('Custom Input')</th> -->
                                    <th scope="col">@lang('Opciones')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($questions as $item)
                                    <tr>
                                        <td data-label="@lang('Nro')">{{$loop->index+1}}</td>
                                        <td data-label="@lang('Pregunta')">{{__($item->question)}}</td>
                                        <td data-label="@lang('Opciones')"> {{@count($item->options)}}</td>
                                        <td data-label="@lang('Tipo')">
                                                @if ($item->type == 1)
                                                    @lang('Opcion unica')
                                                @elseif ($item->type == 2)
                                                    @lang('Opcion Multiple')
                                                @elseif ($item->type == 3)
                                                    @lang('Respuesta Corta')
                                                @elseif ($item->type == 4)
                                                    @lang('Respuesta larga')
                                                @elseif ($item->type == 5)
                                                    @lang('Casilla de verificacion')
                                                @elseif ($item->type == 6)
                                                    @lang('Desplegable')
                                                @elseif ($item->type == 7)
                                                    @lang('Cargar Archivos')
                                                @elseif ($item->type == 8)
                                                    @lang('Fecha')
                                                @elseif ($item->type == 9)
                                                    @lang('Hora')
                                                @elseif ($item->type == 10)
                                                    @lang('Escala')
                                                @endif
                                            </td>
                                        <!-- <td data-label="@lang('Custom Input')">
                                            @if ($item->custom_input == 0)
                                                @lang('No')
                                            @elseif ($item->custom_input == 1)
                                                @lang('Yes')
                                            @endif
                                        </td> -->
                                        @if ($survey->status != 3)
                                            <td data-label="@lang('Action')"><a href="{{route('surveyor.survey.question.edit',[$item->id,$survey->id])}}" class="icon-btn"><i class="la la-pencil-alt"></i></a></td>
                                        @elseif($survey->status == 3)
                                            <td data-label="@lang('Action')"><a href="{{route('surveyor.survey.question.view',[$item->id,$survey->id])}}" class="icon-btn"><i class="la la-eye"></i></a></td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="100%">{{__($empty_message)}}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table><!-- table end -->
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ $questions->links('admin.partials.paginate') }}
                </div>
            </div><!-- card end -->
        </div>
    </div>
@endsection

@push('breadcrumb-plugins')
    <a href="{{route('surveyor.survey.all')}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="las la-angle-double-left"></i>@lang('Volver')</a>
    <a href="{{route('surveyor.survey.question.new',$survey->id)}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="fa fa-fw fa-plus"></i>@lang('Agregar Pregunta')</a>
@endpush
