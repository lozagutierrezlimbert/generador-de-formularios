@extends('surveyor.layouts.app')

@section('panel')
    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <form action="{{route('surveyor.survey.question.update',$question->id)}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12">
                            <div class="form-row">
                                    <div class="col-md-6 for-custom-input">
                                        <div class="form-group">
                                            <label>@lang('Seleccione tipo de pregunta')</label>
                                            <select name="type" class="form-control" required>
                                                <option value="1" {{ $question->type == 1 ? 'selected' : '' }}>@lang('Opcion unica')</option>
                                                <option value="2" {{ $question->type == 2 ? 'selected' : '' }}>@lang('Opcion Multiple')</option>
                                                <option value="3" {{ $question->type == 3 ? 'selected' : '' }}>@lang('Respuesta Corta')</option>
                                                <option value="4" {{ $question->type == 4 ? 'selected' : '' }}>@lang('Respuesta larga')</option>
                                                <option value="5" {{ $question->type == 5 ? 'selected' : '' }}>@lang('Casilla de verificacion')</option>
                                                <option value="6" {{ $question->type == 6 ? 'selected' : '' }}>@lang('Desplegable')</option>
                                                <option value="7" {{ $question->type == 7 ? 'selected' : '' }}>@lang('Cargar Archivos')</option>
                                                <option value="8" {{ $question->type == 8 ? 'selected' : '' }}>@lang('Fecha')</option>
                                                <option value="9" {{ $question->type == 9 ? 'selected' : '' }}>@lang('Hora')</option>
                                                <!-- <option value="10" {{ $question->type == 10 ? 'selected' : '' }}>@lang('Escala')</option> -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('Pregunta')</label>
                                    <textarea class="form-control" name="question" placeholder="@lang('Ingrese su pregunta')" required>{{$question->question}}</textarea>
                                    <input type="hidden" value="{{$s_id}}" name="survey_id" required>
                                </div>

                                <!-- <div id="custom-input-question">

                                </div> -->

                                <div class="payment-method-item p-2"  id = "opciones">
                                    <div class="payment-method-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card border--primary">
                                                    <h5 class="card-header bg--primary  text-white">@lang('Todas las opciones')
                                                        <button type="button" class="btn btn-sm btn-outline-light float-right addUserData"><i class="la la-fw la-plus"></i>@lang('Agregar Nuevo')
                                                        </button>
                                                    </h5>
                                                    <div class="card-body addedField">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="col-md-12">
                                                                            @if($question->options)
                                                                                @foreach ($question->options as $item)
                                                                                    <input class="form-control mb-2" type="text" placeholder="@lang('Enter option')" value="{{$item}}" readonly>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn--primary btn-block">@lang('Actualizar')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('breadcrumb-plugins')
    <a href="{{route('surveyor.survey.question.all',$s_id)}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="las la-angle-double-left"></i>@lang('Volver')</a>
@endpush

@push('script')
<script>
    'use strict';

    (function ($) {

        $('select[name="type"]').on('change',function () {
            var opcionesDiv = $('#opciones');
            if ($('select[name="type"]').val() == 1) {
                opcionesDiv.show();     
                }
            if ($('select[name="type"]').val() == 2) {
                opcionesDiv.show();     
                }
             if ($('select[name="type"]').val() == 3) {
                opcionesDiv.hide();              
            }
            if ($('select[name="type"]').val() == 4) {
                opcionesDiv.hide();              
            }
            if ($('select[name="type"]').val() == 5) {
                opcionesDiv.show();              
            }
            if ($('select[name="type"]').val() == 6) {
                opcionesDiv.show();              
            }
            if ($('select[name="type"]').val() == 7) {
                opcionesDiv.hide();              
            }
            if ($('select[name="type"]').val() == 8) {
                opcionesDiv.hide();              
            }
            if ($('select[name="type"]').val() == 9) {
                opcionesDiv.hide();              
            }
            // if ($('select[name="type"]').val() == 10) {
            //     opcionesDiv.hide();              
            // }
        });

        $('.addUserData').on('click', function () {
                var html = `
                <div class="row user-data">
                    <div class="col-md-11">
                        <div class="form-group">
                            <div class="input-group mb-md-0">
                                <input name="options[]" class="form-control" type="text" placeholder="@lang('Ingrese una opcion')" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                                <button class="btn btn--danger btn-lg removeBtn w-100 mt-28 text-center" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                        <div class="form-group">
                    </div>
                </div>`;
                $('.addedField').append(html)
            });
            $(document).on('click', '.removeBtn', function () {
                $(this).closest('.user-data').remove();
            });

        })(jQuery);
</script>
@endpush
