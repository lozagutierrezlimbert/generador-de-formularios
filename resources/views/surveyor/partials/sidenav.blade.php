<div class="sidebar capsule--rounded bg_img overlay--dark"
     data-background="{{asset('assets/surveyor/images/sidebar/2.jpg')}}">
    <button class="res-sidebar-close-btn"><i class="las la-times"></i></button>
    <div class="sidebar__inner">
        <div class="sidebar__logo">
            <a href="{{route('surveyor.dashboard')}}" class="sidebar__main-logo"><img
                    src="{{getImage(imagePath()['logoIcon']['path'] .'/logo.png')}}" alt="@lang('image')"></a>
            <a href="{{route('surveyor.dashboard')}}" class="sidebar__logo-shape"><img
                    src="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}" alt="@lang('image')"></a>
            <button type="button" class="navbar__expand"></button>
        </div>

        <div class="sidebar__menu-wrapper" id="sidebar__menuWrapper">
            <ul class="sidebar__menu">
                <li class="sidebar-menu-item {{menuActive('surveyor.dashboard')}}">
                    <a href="{{route('surveyor.dashboard')}}" class="nav-link ">
                        <i class="menu-icon las la-home"></i>
                        <span class="menu-title">@lang('Panel')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item {{menuActive('surveyor.survey*')}}">
                    <a href="{{ route('surveyor.survey.all') }}" class="nav-link ">
                        <i class="menu-icon lar la-question-circle"></i>
                        <span class="menu-title">@lang('Formulario')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item {{menuActive('surveyor.report*')}}">
                    <a href="{{ route('surveyor.report') }}" class="nav-link ">
                        <i class="menu-icon las la-chart-bar"></i>
                        <span class="menu-title">@lang('Reportes')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item {{menuActive('surveyor.profile')}}">
                    <a href="{{ route('surveyor.profile') }}" class="nav-link ">
                        <i class="menu-icon las la-user-circle"></i>
                        <span class="menu-title">@lang('Perfil')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item {{menuActive('surveyor.password')}}">
                    <a href="{{route('surveyor.password')}}" class="nav-link ">
                        <i class="menu-icon las la-key"></i>
                        <span class="menu-title">@lang('Cambiar contraseña')</span>
                    </a>
                </li>

                <li class="sidebar-menu-item">
                    <a href="{{ route('surveyor.logout') }}" class="nav-link ">
                        <i class="menu-icon las la-sign-out-alt"></i>
                        <span class="menu-title">@lang('Salir')</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
