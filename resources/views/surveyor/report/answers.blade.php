@extends('surveyor.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-lg-12">
            <div class="card border--primary">
                <h5 class="card-header bg--primary d-flex flex-wrap justify-content-between align-items-center">
                <span class="d-block mr-2 text-white">{{__($survey->name)}}</span>
                <div class="download-links">
                    <a href="{{route('surveyor.answers.download',$survey->id)}}" target="_blank" class="btn btn-sm btn-outline-light"><i class="las la-download"></i>@lang('PDF')</a>
                    <a href="{{route('surveyor.answers.downloadExcel',$survey->id)}}" target="_blank" class="btn btn-sm btn-outline-light"><i class="las la-download"></i>@lang('EXCEL')</a>
                </div>
                </h5>
                <div class="card-body">
                    @if(count($survey->questions) > 0)
                    <div class="table-responsive--md  table-responsive">

                    <div class="table-container">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th scope="col">@lang('Nro')</th>
                                    <th scope="col">@lang('Creador de Formularios')</th>
                                    <th scope="col">@lang('Formulario')</th>
                                    <th scope="col">@lang('Usuario')</th>
                                    <!-- <th scope="col">@lang('Pregunta')</th>
                                    <th scope="col">@lang('Repuesta')</th>
                                    <th scope="col">@lang('Archivo')</th> -->
                                    @foreach ($respuestasPorPregunta as $key => $value)
                                        <th scope="col">@lang('Pregunta : ') {{ $key }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $dataIndex = 0 ;
                            @endphp
                                @forelse($respuestasSolo as $key => $value)
                                    <tr>
                                        <td data-label="@lang('Nro')">{{$loop->index+1}}</td>
                                        @if(isset($data[$dataIndex]))
                                        <td data-label="@lang('Encuestador')">{{__($data[$dataIndex]['Encuestador'])}}</td>
                                        @else
                                        <td data-label="@lang('Encuestador')"> </td>
                                        @endif

                                        @if(isset($data[$dataIndex]))
                                        <td data-label="@lang('Encuesta')">{{__($data[$dataIndex]['Encuesta'])}}</td>
                                        @else
                                        <td data-label="@lang('Encuestador')"> </td>
                                        @endif
                                        
                                        @if(isset($data[$dataIndex]))
                                        <td data-label="@lang('Usuario')">{{ __($data[$dataIndex]['Usuario']) }}</td>
                                        @else
                                        <td data-label="@lang('Encuestador')"> </td>
                                        @endif
                                       
                                       
                                        @php
                                            $dataIndex++ ;
                                        @endphp
                                        @for ($index = 0; $index < count($value); $index++)
                                       @if ($value[$index] && file_exists(public_path('assets/images/answers/' . $value[$index] . '/' . $value[$index])))
                                         <td data-label="@lang('Respuestas')">
                                            <a href="{{ asset('assets/images/answers/' . $value[$index] . '/' . $value[$index]) }}" download="{{ $value[$index] }}">
                                                Descargar Archivo
                                            </a>
                                        </td>
                                        @else
                                        <td data-label="@lang('Respuestas')">{{ __($value[$index]) }}</td>
                                        @endif
                                        
                                        @endfor
                                    </tr>
                                
                                @empty
                                    <tr>
                                        <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                    </tr>
                                @endforelse
                            </tbody>                         
                        </table>
                    </div>
                    </div>
                    @elseif(count($survey->questions) <= 0)
                        <h5 class="card-header mt-3 text-center">@lang('Ninguna pregunta disponible')</h5>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="custom-answer" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">@lang('Respuestas personalizadas')</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div >
                <ul id="answer"></ul>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn--primary" data-dismiss="modal">@lang('Cerrar')</button>
            </div>
          </div>
        </div>
      </div>
@endsection


@push('style')
    <style>
        .progressbar {
            position: relative;
            display: block;
            width: 100%;
            height: 5px;
            background-color: #e9ecef;
        }
        .bar {
            position:absolute;
            width: 0px;
            height: 100%;
            top: 0;
            left: 0;
            overflow:hidden;
        }
        .table-container {
        display: inline-block; /* o float: left; */
        margin-right: 10px; /* O ajusta el margen según sea necesario */
    }
    </style>
@endpush

@push('script')
    <script>
        'use strict';

        $(".progressbar").each(function(){
        $(this).find(".bar").animate({
            "width": $(this).attr("data-perc")
        },2000);
        $(this).find(".label").animate({
            "left": $(this).attr("data-perc")
        },2000);
        });


        $('.custom-answer').on('click', function () {
            var modal = $('#custom-answer');
            modal.find('#answer').empty();
            var resource = $(this).data('resource');

            $.each(resource, function (index, value) {
                if (value.custom_answer) {
                    modal.find('#answer').append(`
                        <li><i class="las la-dot-circle"></i> ${value.custom_answer}</li>
                    `);
                }
            });
        });
    </script>
@endpush
