@extends('surveyor.layouts.app')

@section('panel')
    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <form action="{{route('surveyor.survey.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>@lang('Imagen')</label>
                                    <div class="image-upload">
                                        <div class="thumb">
                                            <div class="avatar-preview">
                                                <div class="profilePicPreview" style="background-image: url({{ getImage('/',imagePath()['survey']['size']) }})">
                                                    <button type="button" class="remove-image"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>

                                            <div class="avatar-edit">
                                                <input type="file" class="profilePicUpload" name="image" id="profilePicUpload1" accept=".png, .jpg, .jpeg" >
                                                <label for="profilePicUpload1" class="bg--success"> @lang('Image')</label>
                                                <small class="mt-2 text-facebook">@lang('Extenciones soportadas'): <b>@lang('jpeg, jpg, png')</b>.
                                                @lang('La imagen se converita a: '): <b>{{imagePath()['survey']['size']}}</b> px.

                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label>@lang('Seleccionar Categoria')</label>
                                    <select name="category_id" class="form-control" required>
                                        @foreach ($categories as $item)
                                            <option value="{{$item->id}}">{{__($item->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>@lang('Nombre del Formulario')</label>
                                    <input type="text"class="form-control" placeholder="@lang('Enter Name')" name="name" required>
                                </div>

                                <div class="form-group">
                                    <label>@lang('Seleccionar color')</label>
                                    <select name="color" class="form-control" >
                                            <option value="#FFFFFF"></option> 
                                            @php
                                 
                                                $colors = [
                                                    'Rojo' => '#FF0000',
                                                    'Azul' => '#0000FF',
                                                    'Verde' => '#00FF00',
                                                    'Amarillo' => '#FFFF00',
                                                    'Morado' => '#800080',
                                                    'Naranja' => '#FFA500',
                                                    'Rosado' => '#FFC0CB',
                                                    'Cian' => '#00FFFF',
                                                    'Turquesa' => '#40E0D0',
                                                    'Magenta' => '#FF00FF',
                                                    'Gris' => '#808080',
                                                    'Blanco' => '#FFFFFF',
                                                 
                                                ];
                                            @endphp
                                            @foreach ($colors as $colorName => $colorValue)
                                                <option value="{{ $colorValue }}" style="background-color: {{ $colorValue }};">
                                                    {{ $colorName }}
                                                </option>
                                            @endforeach
                                        </select>
                                </div>

                                <div class="form-group">
                                    <label>@lang('Seleccionar tipo de letra')</label>
                                    <select name="type_text" class="form-control" >
                                        <option value="Arial, sans-serif"></option> 
                                        @php
                                            $fonts = [
                                                'Arial, sans-serif' => 'Arial',
                                                'Times New Roman, serif' => 'Times New Roman',
                                                'Courier New, monospace' => 'Courier New',
                                                'Verdana, sans-serif' => 'Verdana',
                                                'Georgia, serif' => 'Georgia',
                                                'Comic Sans MS, cursive' => 'Comic Sans MS',
                                                'Impact, Charcoal, sans-serif' => 'Impact',
                                                'Trebuchet MS, sans-serif' => 'Trebuchet MS',
                                                'Palatino Linotype, Book Antiqua, Palatino, serif' => 'Palatino Linotype',
                                                'Lucida Console, Monaco, monospace' => 'Lucida Console',
                                              
                                            ];
                                        @endphp
                                        @foreach ($fonts as $fontValue => $fontName)
                                            <option value="{{ $fontValue }}" style="font-family: {{ $fontValue }};">
                                                {{ $fontName }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>  

                                <div class="form-group">
                                <label for="fecha_hora">Fecha y Hora de Finalización</label>
                                <input type="datetime-local" id="fecha_hora" name="fecha_hora" class="form-control" required>
                            </div>
                                
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn--primary btn-block">@lang('Crear')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('breadcrumb-plugins')
    <a href="{{route('surveyor.survey.all')}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="las la-angle-double-left"></i>@lang('Volver')</a>
@endpush

@push('script')
    <script>
        'use strict';

        (function ($) {

            var age_limit_div = `<div class="row">
                                    <div class="form-group col-md-6">
                                        <label>@lang('Minimum Age')</label>
                                        <input type="number" class="form-control" placeholder="@lang('Enter minimum age')" name="start_age" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>@lang('Maximum Age')</label>
                                        <input type="number" class="form-control" placeholder="@lang('Enter maximum age')" name="end_age" required>
                                    </div>
                                </div>`;

            var country_limit_div = `<div class="row">
                                        <div class="form-group col-md-12">
                                            <label>@lang('Select Countries')</label>
                                            <select class="select2-multi-select" class="form-control" name="country[]" multiple="multiple" required>
                                                    @include('partials.country')
                                            </select>
                                        </div>
                                    </div>`;


            $('select[name="age_limit"]').on('change',function () {

                var age_val = $('select[name="age_limit"]').val();

                if (age_val == 1) {

                    $('.age-limit-div').html(age_limit_div);

                }

                if (age_val == 0) {

                    $('.age-limit-div').html('');
                }
            });

            $('select[name="country_limit"]').on('change',function () {

                var country_val = $('select[name="country_limit"]').val();

                if (country_val == 1) {

                    $('.country-limit-div').html(country_limit_div);
                    triggerSelect2();

                }
                if (country_val == 0) {

                    $('.country-limit-div').html('');
                }

            });

            function triggerSelect2(){

                $(document).find('.select2-multi-select').select2({
                    dropdownParent: $(document).find('.card')
                });
            }

            triggerSelect2();

        })(jQuery);
    </script>
@endpush
