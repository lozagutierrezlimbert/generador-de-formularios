@extends('surveyor.layouts.app')
@section('panel')

    <div class="row">
        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--md  table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th scope="col">@lang('Nro')</th>
                                    <th scope="col">@lang('Categoria')</th>
                                    <th scope="col">@lang('Imagen')</th>
                                    <th scope="col">@lang('Nombre')</th>
                                    <th scope="col">@lang('Estado')</th> 
                                    <th scope="col">@lang('Nro de preguntas')</th>
                                    <th scope="col">@lang('Fecha de creación')</th>
                                    <th scope="col">@lang('Compartir enlace')</th>
                                    <th scope="col">@lang('Opciones')</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($surveys as $item)
                                    <tr>
                                        <td data-label="@lang('Nro')">{{$loop->index+1}}</td>
                                        <td data-label="@lang('Categoria')">{{__($item->category->name)}}</td>
                                        <td data-label="@lang('Imagen')">
                                            <div class="user">
                                                <div class="thumb">
                                                    <img src="{{ getImage(imagePath()['survey']['path'].'/'.$item->image,imagePath()['survey']['size'])}}" alt="@lang('image')">
                                                </div>
                                            </div>
                                        </td>
                                        <td data-label="@lang('Nombre')">{{__($item->name)}}</td>
                                        <td data-label="@lang('Estado')">
                                            @if ($item->status == 0)
                                                <span class="text--small badge font-weight-normal badge--success">@lang('Aprobado')</span>
                                            @elseif($item->status == 1)
                                                <span class="text--small badge font-weight-normal badge--warning">@lang('Pendiente')</span>
                                            @elseif($item->status == 3)
                                                <span class="text--small badge font-weight-normal badge--danger">@lang('Rechazado')</span>
                                            @endif
                                        </td>
                                        <td data-label="@lang('Nro de Preguntas')">{{ $item->questions->count() }}</td>
                                        <td data-label="@lang('Fecha de creación')">{{ showDateTime($item->created_at) }}</td>
                                        <td data-label="@lang('Compartir')">
                                            @if ($item->status == 0)
                                                <a href="{{route('survey.share',$item->id)}}" target="_blank"  rel="noopener noreferrer" class="icon-btn mr-1" data-toggle="tooltip" title="@lang('Compartir enalce')" data-original-title="@lang('Compartir enlace')">
                                                <i class="fa fa-share-alt"></i> 
                                                </a>
                                                <a id="copy-link" href="#" data-url="{{ route('survey.share', $item->id) }}">
                                                    Enlace Publico
                                                </a>
                                                <button id="copy-button">Copiar</button> 
                                                
                                                

                                                <a href="{{route('survey.share.private',$item->id)}}" target="_blank"  rel="noopener noreferrer" class="icon-btn mr-1" data-toggle="tooltip" title="@lang('Compartir enalce')" data-original-title="@lang('Compartir enlace')">
                                                <i class="fa fa-share-alt"></i> 
                                                </a>
                                                <a id="copy-link1" href="#" data-url="{{ route('survey.share.private', $item->id) }}">
                                                    Enlace Privado
                                                </a>
                                                <!-- <button id="copy-button1">Copiar</button>  -->
                                            @endif
                                        </td> 
                                        <td data-label="@lang('Opciones')">
                                            @if ($item->status != 3)
                                                <a href="{{route('surveyor.survey.edit',$item->id)}}" class="icon-btn mr-1" data-toggle="tooltip" title="@lang('Editar')" data-original-title="@lang('Editar')">
                                                    <i class="la la-pencil-alt text--shadow"></i>
                                                </a>
                                            @endif
                                            <a href="{{route('surveyor.survey.question.all',$item->id)}}" class="icon-btn mr-1" data-toggle="tooltip" title="@lang('Preguntas')" data-original-title="@lang('Preguntas')">
                                                <i class="fas fa-question-circle text--shadow"></i>
                                            </a>
                                            <a href="{{route('surveyor.survey.user.add',$item->id)}}" class="icon-btn mr-1" data-toggle="tooltip" title="@lang('Agregar usuarios')" data-original-title="@lang('Agregar usuarios')">
                                            <i class="fas fa-user-plus text--shadow"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table><!-- table end -->
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ $surveys->links('admin.partials.paginate') }}
                </div>
            </div><!-- card end -->
        </div>
    </div>
    <script>
        
    document.getElementById("copy-button").addEventListener("click", function() {
        var copyText = document.getElementById("copy-link");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.getAttribute("data-url");
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("copy");
        document.body.removeChild(textArea);
        alert("URL copiada al portapapeles: " + textArea.value);
    });
    document.getElementById("copy-button|").addEventListener("click", function() {
        var copyText = document.getElementById("copy-link|");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.getAttribute("data-url");
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("copy");
        document.body.removeChild(textArea);
        alert("URL copiada al portapapeles: " + textArea.value);
    });
</script>
@endsection

@push('breadcrumb-plugins')
    <a href="{{route('surveyor.survey.new')}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="fa fa-fw fa-plus"></i>@lang('Agregar Formulario')</a>
@endpush
