# Generador de Formularios

Este proyecto es un generador de formularios web que te permite crear y personalizar formularios de manera sencilla. Puedes utilizarlo para crear encuestas, formularios de contacto y más.

## Uso

Para utilizar el generador de formularios, sigue estos pasos:

1. Clona este repositorio en tu máquina local:

