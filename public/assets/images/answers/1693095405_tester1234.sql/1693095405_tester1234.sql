-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.30 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando datos para la tabla system_service.about_us: ~0 rows (aproximadamente)
INSERT INTO `about_us` (`id`, `header`, `header_description`, `status`, `about_us_title`, `about_us`, `foreground_image`, `background_image`, `small_image_one`, `small_image_two`, `small_image_three`, `total_rating`, `why_choose_us_title`, `why_choose_desciption`, `why_choose_background`, `why_choose_foreground`, `title_one`, `title_two`, `title_three`, `description_one`, `description_two`, `description_three`, `created_at`, `updated_at`) VALUES
	(1, 'How It Works', 'There are many variations of passages of Lorem Ipsum available but the majority', NULL, 'Know About Us', '<p style="font-size: 16px; font-family: Roboto, sans-serif;">What sets Websolutionus apart, we believe in our commitment to providing actual value to our consumers. In the business, our dedication and quality are unrivaled. We\'re more than a technology service provider. We care as much about our customer&rsquo;s achievements as we do about our own, therefore we share business risks with them so they may take chances with technological innovations. We provide the following services.</p>\r\n<ul>\r\n<li>Laravel Website Development</li>\r\n<li>Mobile Application Development</li>\r\n<li>WordPress Theme Development</li>\r\n<li>Search Engine Optimization (SEO)</li>\r\n</ul>', 'uploads/website-images/about-us-foreground-2022-08-28-01-05-24-9243.jpg', 'uploads/website-images/about-us-bg-2022-08-28-01-05-24-2606.jpg', 'uploads/website-images/about-us-client-one-2023-06-01-01-01-36-1053.png', 'uploads/website-images/about-us-client-one-2023-06-01-01-01-38-4371.png', 'uploads/website-images/about-us-client-one-2023-06-01-01-01-38-5296.png', '25k+', 'There Some Reasons to Hire The Proeasy', '<p>We are dedicated to work with all dynamic features like Laravel, customized website, PHP, SEO, etc. We believe on just in time. We provide all web solutions accordingly and ensure the best service. We rely on new creation and the best management policy. We usually monitor the market and policies.</p>', 'uploads/website-images/about-us-bg-2022-08-28-01-40-24-9733.jpg', 'uploads/website-images/about-us-foreground-2022-08-28-01-40-33-7719.jpg', 'Top-Rated Company', 'Superior Quality', 'Friendly Provide Services', 'We offer low-cost services and cutting-edge technologies to help you improve your application and bring more value to your consumers', 'We assist enterprises to decrease the risk of security events across the SDLC and launch internet solutions with protection.', 'We assist our customers to determine the right way for them and provide business eLearning solutions.', '2022-01-30 12:30:23', '2023-06-01 05:01:38');

-- Volcando datos para la tabla system_service.additional_services: ~48 rows (aproximadamente)

-- Volcando datos para la tabla system_service.admins: ~3 rows (aproximadamente)
INSERT INTO `admins` (`id`, `admin_type`, `name`, `email`, `image`, `email_verified_at`, `password`, `remember_token`, `status`, `forget_password_token`, `created_at`, `updated_at`) VALUES
	(1, 1, 'John Doe', 'admin@gmail.com', 'uploads/website-images/john-doe-2022-12-25-04-13-32-5577.jpg', NULL, '$2y$10$E9vUQogef2us1sas54MD6e3Z5yBoTSerndPBBtC7438PIy2M3dhoO', NULL, 1, NULL, NULL, '2023-01-14 10:44:02'),
	(13, 0, 'David Richard', 'admin1@gmail.com', 'uploads/website-images/david-richard-2022-12-25-04-27-24-8827.jpg', NULL, '$2y$10$ugyPYFYfSSvoBDMlMA4AM.xzLRmtdFlzg6H74Z6ZzZCIfOM4dNQuu', NULL, 1, NULL, '2022-12-25 10:23:18', '2022-12-25 10:27:24'),
	(14, 0, 'Daniel Paul', 'admin2@gmail.com', NULL, NULL, '$2y$10$YLbQclElSRHXqvlTCgG95uy.lQeaqSESueQNO9YqSXfdbNBGozHJi', NULL, 1, NULL, '2022-12-25 10:23:29', '2022-12-25 10:23:29');

-- Volcando datos para la tabla system_service.appointment_schedules: ~170 rows (aproximadamente)
INSERT INTO `appointment_schedules` (`id`, `user_id`, `day`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Sunday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(2, 2, 'Sunday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(3, 2, 'Sunday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(5, 2, 'Sunday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(6, 2, 'Sunday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(7, 2, 'Sunday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(8, 2, 'Sunday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(9, 2, 'Sunday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(10, 2, 'Sunday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(11, 2, 'Sunday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(12, 2, 'Sunday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(13, 2, 'Sunday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(14, 2, 'Sunday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(15, 2, 'Sunday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(16, 2, 'Sunday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(17, 2, 'Sunday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(18, 2, 'Sunday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(19, 2, 'Monday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(20, 2, 'Monday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(21, 2, 'Monday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(22, 2, 'Monday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(23, 2, 'Monday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(24, 2, 'Monday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(25, 2, 'Monday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(26, 2, 'Monday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(27, 2, 'Monday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(28, 2, 'Monday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(29, 2, 'Monday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(30, 2, 'Monday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(31, 2, 'Monday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(32, 2, 'Monday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(33, 2, 'Monday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(34, 2, 'Monday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(35, 2, 'Monday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(36, 2, 'Tuesday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(37, 2, 'Tuesday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(38, 2, 'Tuesday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(39, 2, 'Tuesday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(40, 2, 'Tuesday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(41, 2, 'Tuesday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(42, 2, 'Tuesday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(43, 2, 'Tuesday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(44, 2, 'Tuesday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(45, 2, 'Tuesday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(46, 2, 'Tuesday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(47, 2, 'Tuesday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(48, 2, 'Tuesday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(49, 2, 'Tuesday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(50, 2, 'Tuesday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(51, 2, 'Tuesday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(52, 2, 'Tuesday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(53, 2, 'Wednesday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(54, 2, 'Wednesday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(55, 2, 'Wednesday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(56, 2, 'Wednesday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(57, 2, 'Wednesday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(58, 2, 'Wednesday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(59, 2, 'Wednesday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(60, 2, 'Wednesday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(61, 2, 'Wednesday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(62, 2, 'Wednesday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(63, 2, 'Wednesday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(64, 2, 'Wednesday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(65, 2, 'Wednesday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(66, 2, 'Wednesday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(67, 2, 'Wednesday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(68, 2, 'Wednesday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(69, 2, 'Wednesday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(70, 2, 'Thursday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(71, 2, 'Thursday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(72, 2, 'Thursday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(73, 2, 'Thursday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(74, 2, 'Thursday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(75, 2, 'Thursday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(76, 2, 'Thursday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(77, 2, 'Thursday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(78, 2, 'Thursday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(79, 2, 'Thursday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(80, 2, 'Thursday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(81, 2, 'Thursday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(82, 2, 'Thursday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(83, 2, 'Thursday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(84, 2, 'Thursday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(85, 2, 'Thursday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(86, 2, 'Thursday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(87, 4, 'Sunday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(88, 4, 'Sunday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(89, 4, 'Sunday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(90, 4, 'Sunday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(91, 4, 'Sunday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(92, 4, 'Sunday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(93, 4, 'Sunday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(94, 4, 'Sunday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(95, 4, 'Sunday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(96, 4, 'Sunday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(97, 4, 'Sunday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(98, 4, 'Sunday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(99, 4, 'Sunday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(100, 4, 'Sunday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(101, 4, 'Sunday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(102, 4, 'Sunday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(103, 4, 'Sunday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(104, 4, 'Monday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(105, 4, 'Monday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(106, 4, 'Monday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(107, 4, 'Monday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(108, 4, 'Monday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(109, 4, 'Monday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(110, 4, 'Monday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(111, 4, 'Monday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(112, 4, 'Monday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(113, 4, 'Monday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(114, 4, 'Monday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(115, 4, 'Monday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(116, 4, 'Monday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(117, 4, 'Monday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(118, 4, 'Monday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(119, 4, 'Monday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(120, 4, 'Monday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(121, 4, 'Tuesday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(122, 4, 'Tuesday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(123, 4, 'Tuesday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(124, 4, 'Tuesday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(125, 4, 'Tuesday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(126, 4, 'Tuesday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(127, 4, 'Tuesday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(128, 4, 'Tuesday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(129, 4, 'Tuesday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(130, 4, 'Tuesday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(131, 4, 'Tuesday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(132, 4, 'Tuesday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(133, 4, 'Tuesday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(134, 4, 'Tuesday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(135, 4, 'Tuesday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(136, 4, 'Tuesday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(137, 4, 'Tuesday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(138, 4, 'Wednesday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(139, 4, 'Wednesday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(140, 4, 'Wednesday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(141, 4, 'Wednesday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(142, 4, 'Wednesday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(143, 4, 'Wednesday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(144, 4, 'Wednesday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(145, 4, 'Wednesday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(146, 4, 'Wednesday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(147, 4, 'Wednesday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(148, 4, 'Wednesday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(149, 4, 'Wednesday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(150, 4, 'Wednesday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(151, 4, 'Wednesday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(152, 4, 'Wednesday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(153, 4, 'Wednesday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(154, 4, 'Wednesday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54'),
	(155, 4, 'Thursday', '08:00', '08:20', 1, '2023-01-09 04:57:25', '2023-01-09 04:57:25'),
	(156, 4, 'Thursday', '08:20', '08:40', 1, '2023-01-09 04:57:42', '2023-01-09 04:57:42'),
	(157, 4, 'Thursday', '08:40', '09:00', 1, '2023-01-09 05:15:41', '2023-01-09 05:15:41'),
	(158, 4, 'Thursday', '09:00', '09:20', 1, '2023-01-09 05:49:13', '2023-01-09 05:49:13'),
	(159, 4, 'Thursday', '09:20', '09:40', 1, '2023-01-09 05:49:24', '2023-01-09 05:49:24'),
	(160, 4, 'Thursday', '09:40', '10:00', 1, '2023-01-09 05:49:40', '2023-01-09 05:49:40'),
	(161, 4, 'Thursday', '10:00', '10:20', 1, '2023-01-09 05:55:16', '2023-01-09 05:55:16'),
	(162, 4, 'Thursday', '10:20', '10:40', 1, '2023-01-09 05:55:56', '2023-01-09 05:55:56'),
	(163, 4, 'Thursday', '10:40', '11:00', 1, '2023-01-09 05:57:47', '2023-01-09 05:57:47'),
	(164, 4, 'Thursday', '11:00', '11:20', 1, '2023-01-09 05:57:54', '2023-01-09 05:57:54'),
	(165, 4, 'Thursday', '11:20', '11:40', 1, '2023-01-09 05:58:09', '2023-01-09 05:58:09'),
	(166, 4, 'Thursday', '11:40', '12:00', 1, '2023-01-09 05:58:19', '2023-01-09 05:58:19'),
	(167, 4, 'Thursday', '12:00', '12:20', 1, '2023-01-09 05:59:35', '2023-01-09 05:59:35'),
	(168, 4, 'Thursday', '12:20', '12:40', 1, '2023-01-09 06:00:06', '2023-01-09 06:00:06'),
	(169, 4, 'Thursday', '12:40', '13:00', 1, '2023-01-09 06:00:54', '2023-01-09 06:01:18'),
	(170, 4, 'Thursday', '13:00', '13:30', 1, '2023-01-09 06:01:42', '2023-01-09 06:01:42'),
	(171, 4, 'Thursday', '13:30', '14:00', 1, '2023-01-09 06:01:54', '2023-01-09 06:01:54');

-- Volcando datos para la tabla system_service.bank_payments: ~0 rows (aproximadamente)
INSERT INTO `bank_payments` (`id`, `status`, `account_info`, `image`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Esta seguro de guardar toda la información', 'uploads/website-images/bank-2023-06-06-09-10-39-9289.png', NULL, '2023-06-06 13:10:39');

-- Volcando datos para la tabla system_service.banner_images: ~15 rows (aproximadamente)
INSERT INTO `banner_images` (`id`, `title`, `description`, `link`, `image`, `button_text`, `banner_location`, `status`, `header`, `created_at`, `updated_at`) VALUES
	(1, 'Up To - 35% Off', 'Hot Deals', 'product', 'uploads/website-images/Mega-menu-2022-02-13-07-53-14-1062.png', 'Shop Now', 'Mega Menu Banner', 1, NULL, NULL, '2022-02-13 13:53:14'),
	(2, 'Up To -20% Off', 'Hot Deals', 'product', 'uploads/website-images/banner--2022-02-10-10-24-47-2663.jpg', 'Shop Now', 'Home Page One Column Banner', 1, NULL, NULL, '2022-02-13 13:45:52'),
	(3, 'Up To -35% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-06-03-42-16-1335.png', 'Shop Now', 'Home Page First Two Column Banner One', 1, NULL, NULL, '2022-02-13 13:46:01'),
	(4, 'Up To -40% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-06-03-42-16-1434.png', 'Shop Now', 'Home Page First Two Column Banner Two', 1, NULL, NULL, '2022-02-13 13:46:01'),
	(5, 'Up To -28% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-06-04-18-01-2862.jpg', 'Shop Now', 'Home Page Second Two Column Banner one', 1, NULL, NULL, '2022-02-13 13:46:15'),
	(6, 'Up To -22% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-06-04-18-01-6995.jpg', 'Shop Now', 'Home Page Second Two Column Banner two', 1, NULL, NULL, '2022-02-13 13:46:15'),
	(7, 'Up To -35% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-13-04-57-46-4114.jpg', 'Shop Now', 'Home Page Third Two Column Banner one', 1, NULL, NULL, '2022-02-13 13:46:27'),
	(8, 'Up To -15% Off', 'Hot Deals', 'product', 'uploads/website-images/banner-2022-02-13-04-58-43-7437.jpg', 'Shop Now', 'Home Page Third Two Column Banner Two', 1, NULL, NULL, '2022-02-13 13:46:27'),
	(9, 'This is Tittle', 'This is Description', 'product', 'uploads/website-images/banner-2022-02-06-04-24-44-6895.jpg', 'dd', 'Shopping cart bottom', 1, '', NULL, '2022-02-13 13:47:23'),
	(10, 'This is Title', 'This is Description', 'product', 'uploads/website-images/banner-2022-02-06-04-25-59-9719.jpg', NULL, 'Shopping cart bottom', 0, NULL, NULL, '2022-02-13 13:47:23'),
	(11, 'This is Tittle', 'This is Description', 'product', 'uploads/website-images/banner-2022-02-06-04-26-46-8505.jpg', 'dd', 'Campaign page', 1, '', NULL, '2022-02-13 13:47:31'),
	(12, 'This is Tittle', 'This is Description', 'product', 'uploads/website-images/banner-2022-01-30-06-21-06-4562.png', 'dd', 'Campaign page', 0, '', NULL, '2022-02-13 13:47:31'),
	(13, 'This is Tittle', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Shop Now', 'uploads/website-images/banner-2022-02-07-10-48-37-9226.jpg', 'dd', 'Login page', 0, 'Our Achievement', NULL, '2022-02-07 04:48:39'),
	(14, 'Black Friday Sale', 'Up To -70% Off', 'product', 'uploads/website-images/banner-2022-02-06-04-24-02-9777.jpg', NULL, 'Product Detail', 1, NULL, NULL, '2022-02-13 13:46:54'),
	(15, 'Default Profile Image', NULL, NULL, 'uploads/website-images/default-avatar-2022-02-07-10-10-46-1477.jpg', NULL, 'Default Profile Image', 0, NULL, NULL, '2022-02-07 04:10:50');

-- Volcando datos para la tabla system_service.blogs: ~8 rows (aproximadamente)
INSERT INTO `blogs` (`id`, `admin_id`, `title`, `slug`, `blog_category_id`, `image`, `description`, `views`, `seo_title`, `seo_description`, `status`, `show_homepage`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Servicios de limpieza en general TOTE´S', 'servicios-de-limpieza-en-general-totes', 1, 'uploads/custom-images/blog--2022-09-29-11-23-39-3061.jpg', '<p>Servicios de limpieza en toda la ciudad de santa cruz empresa ubicada en zona central de santa cruz en el primer anillo&nbsp;</p>', 0, 'Logistics of container cargo ship and cargo plane', 'Logistics of container cargo ship and cargo plane', 1, 1, '2022-09-29 05:23:40', '2023-05-04 10:02:50'),
	(2, 1, 'Servicios de mantenimientos de transporte pesado', 'servicios-de-mantenimientos-de-transporte-pesado-', 4, 'uploads/custom-images/blog--2022-09-29-11-27-10-7404.jpg', '<p>En la ciudad de Santa Cruz de la Sierra, Bolivia, el transporte pesado es una actividad fundamental para la econom&iacute;a y el desarrollo de la regi&oacute;n. Desde el transporte de mercanc&iacute;as hasta la construcci&oacute;n de infraestructuras, la maquinaria pesada es imprescindible para mantener el flujo de la econom&iacute;a.</p>', 0, 'Service maintenance repair engine in transport', 'Service maintenance repair engine in transport', 1, 1, '2022-09-29 05:27:10', '2023-05-04 10:06:12'),
	(3, 1, 'Todo lo que tienes que saber sobre el urbanismo', 'todo-lo-que-tienes-que-saber-sobre-el-urbanismo-', 3, 'uploads/custom-images/blog--2022-09-29-11-31-38-4858.jpg', '<p>La arquitectura y el urbanismo son dos disciplinas estrechamente relacionadas que tienen un gran impacto en nuestras vidas cotidianas. La arquitectura se refiere al dise&ntilde;o y construcci&oacute;n de edificios y estructuras, mientras que el urbanismo se enfoca en la planificaci&oacute;n y dise&ntilde;o de ciudades y comunidades.</p>', 0, 'Rubber glove cleaning table disinfectant alcohol spray', 'Rubber glove cleaning table disinfectant alcohol spray', 1, 1, '2022-09-29 05:31:39', '2023-05-04 10:12:41'),
	(4, 1, 'la importancia de la preservación del patrimonio cultural y natural', 'la-importancia-de-la-preservacin-del-patrimonio-cultural-y-natural', 3, 'uploads/custom-images/blog--2022-09-29-11-33-28-5301.jpg', '<p>El urbanismo es una&nbsp; disciplina que tiene un gran impacto en nuestras vidas cotidianas. Un buen dise&ntilde;o arquitect&oacute;nico y urbano puede mejorar la calidad de vida de las personas y promover la sostenibilidad y la inclusi&oacute;n social. Adem&aacute;s, la tecnolog&iacute;a contin&uacute;a transformando la forma en que se dise&ntilde;a y construye el entorno construido.</p>', 0, 'Man cutting beard at a shop barber salon', 'Man cutting beard at a shop barber salon', 1, 1, '2022-09-29 05:33:28', '2023-05-04 10:15:50'),
	(5, 1, 'Lo que tienes que saber sobre la desinfección para evitar el virus covid', 'lo-que-tienes-que-saber-sobre-la-desinfeccin-para-evitar-el-virus-covid-', 1, 'uploads/custom-images/blog--2022-09-29-11-35-00-7694.jpg', '<p>La pandemia de COVID-19 ha puesto en primer plano la importancia de la desinfecci&oacute;n de lugares para prevenir la propagaci&oacute;n de virus y enfermedades. La desinfecci&oacute;n es un proceso que elimina y reduce la presencia de microorganismos pat&oacute;genos en superficies y objetos, reduciendo el riesgo de infecci&oacute;n y enfermedad.</p>', 0, 'Spry and disinfection of office to prevent', 'Spry and disinfection of office to prevent', 1, 1, '2022-09-29 05:35:00', '2023-05-04 10:09:24'),
	(6, 1, 'Paola Kaiser - UNO Corporación Inmobiliaria', 'paola-kaiser-uno-corporacin-inmobiliaria', 6, 'uploads/custom-images/blog--2023-05-04-05-03-16-6321.png', '<p>El proyecto se ubica en la zona norte, entrando por la Chonta, Av. Banzer y 9no anillo otra v&iacute;a de acceso por la Av. G77 o la Av. Beni.</p>', 0, 'Switchboard an a electrical connecting cable.', 'Switchboard an a electrical connecting cable.', 1, 1, '2022-09-29 05:36:42', '2023-05-04 09:03:18'),
	(7, 1, 'Lo que tenés que saber antes de vender o alquilar tu casa', 'lo-que-tens-que-saber-antes-de-vender-o-alquilar-tu-casa', 8, 'uploads/custom-images/blog--2023-05-04-03-18-22-5733.png', '<p>El proyecto se ubica en la zona norte, entrando por la Chonta, Av. Banzer y 9no anillo otra v&iacute;a de acceso por la Av. G77 o la Av. Beni.</p>', 0, 'Home Move Service From One City to Another City', 'Home Move Service From One City to Another City', 1, 0, '2022-09-29 05:47:01', '2023-05-04 07:18:29'),
	(8, 1, 'Reviví el Open Day Virtual: Urbanización El Vallecito', 'reviv-el-open-day-virtual-urbanizacin-el-vallecito', 1, 'uploads/custom-images/blog--2023-05-04-03-04-17-5218.png', '<p>El proyecto se ubica en la zona norte, entrando por la Chonta, Av. Banzer y 9no anillo otra v&iacute;a de acceso por la Av. G77 o la Av. Beni.</p>', 0, 'Now Get Massage Service From Mr Joe', 'Now Get Massage Service From Mr Joe', 1, 0, '2022-09-29 05:48:32', '2023-05-04 07:04:25');

-- Volcando datos para la tabla system_service.blog_categories: ~9 rows (aproximadamente)
INSERT INTO `blog_categories` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Últimas noticias', 'ltimas-noticias', 1, '2022-09-29 05:18:01', '2023-05-04 04:41:32'),
	(2, 'Mercado inmobiliario', 'mercado-inmobiliario', 1, '2022-09-29 05:20:11', '2023-05-04 04:42:13'),
	(3, 'Arquitectura y urbanismo', 'arquitectura-y-urbanismo', 1, '2022-09-29 05:20:23', '2023-05-04 04:44:22'),
	(4, 'Tips', 'tips', 1, '2022-09-29 05:20:34', '2023-05-04 04:45:03'),
	(5, 'Real State', 'real-state', 1, '2022-09-29 05:20:44', '2023-05-04 04:45:30'),
	(6, 'Lo más leído', 'lo-ms-ledo', 1, '2022-09-29 05:20:52', '2023-05-04 04:45:56'),
	(8, 'Materiales de construcción', 'materiales-de-construccin', 1, '2022-09-29 05:21:18', '2023-05-04 04:47:48'),
	(9, 'Servicios profesionales', 'servicios-profesionales', 1, '2023-05-04 04:49:21', '2023-05-04 04:49:21'),
	(10, 'Otros', 'otros', 1, '2023-05-04 04:50:16', '2023-05-04 04:50:16');

-- Volcando datos para la tabla system_service.blog_comments: ~4 rows (aproximadamente)
INSERT INTO `blog_comments` (`id`, `blog_id`, `name`, `email`, `comment`, `status`, `created_at`, `updated_at`) VALUES
	(2, 7, 'David Richard', 'user@gmail.com', 'Id est maiorum volutpat, ad nominavi suscipit suscipiantur vix. Ut ius veri aperiam reprehendunt. Ut per unum sapientem consequuntur', 1, '2022-09-29 05:53:04', '2022-09-29 05:53:13'),
	(3, 7, 'John Doe', 'john@gmail.com', 'Appetere fabellas ius te. Nonumes splendide deseruisse ea vis, alii velit vel eu. Eos ut scaevola platonem rationibus. Vis natum vivendo', 1, '2022-09-29 05:53:41', '2022-09-29 05:53:53'),
	(4, 8, 'David Richard', 'david@gmail.com', 'Appetere fabellas ius te. Nonumes splendide deseruisse ea vis, alii velit vel eu. Eos ut scaevola platonem rationibus vis natum vivendo.', 1, '2022-09-29 05:54:38', '2022-09-29 05:56:19'),
	(5, 8, 'David Simmons', 'simmons@gmail.com', 'Per ex vero nonumy. Ius eu doming nominavi mediocrem, aliquid efficiantur no vim, sanctus admodum mnesarchum ad pro. No sea invidunt', 1, '2022-09-29 05:55:08', '2022-09-29 05:56:16');

-- Volcando datos para la tabla system_service.breadcrumb_images: ~14 rows (aproximadamente)
INSERT INTO `breadcrumb_images` (`id`, `location`, `image_type`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'About Page', 1, 'uploads/website-images/banner-us-2023-07-06-04-53-03-8333.png', NULL, '2023-07-06 08:53:03'),
	(2, 'Contact Page', 1, 'uploads/website-images/banner-us-2023-05-04-08-22-54-3092.png', NULL, '2023-05-04 12:22:54'),
	(3, 'Blog Page', 1, 'uploads/website-images/banner-us-2023-06-28-09-26-31-2351.jpg', NULL, '2023-06-29 01:26:31'),
	(4, 'FAQ Page', 1, 'uploads/website-images/banner-us-2022-11-06-04-24-30-8075.jpg', NULL, '2022-11-06 10:24:30'),
	(5, 'Terms & Conditions Page', 1, 'uploads/website-images/banner-us-2023-06-28-09-32-09-5906.png', NULL, '2023-06-29 01:32:10'),
	(6, 'Privacy Policy Page', 1, 'uploads/website-images/banner-us-2023-06-28-09-32-48-7448.png', NULL, '2023-06-29 01:32:50'),
	(7, 'Custom Page', 1, 'uploads/website-images/banner-us-2022-11-06-04-25-22-7776.jpg', NULL, '2022-11-06 10:25:22'),
	(8, 'Service, Booking Page', 1, 'uploads/website-images/banner-us-2023-05-17-08-43-10-1452.jpg', NULL, '2023-05-17 12:43:19'),
	(9, 'Provider Page', 1, 'uploads/website-images/banner-us-2022-09-15-01-43-35-3681.jpg', NULL, '2022-09-15 07:43:35'),
	(10, 'Dashboard', 1, 'uploads/website-images/banner-us-2023-05-23-06-32-51-5320.png', NULL, '2023-05-23 10:33:00'),
	(11, 'Login, Register', 1, 'uploads/website-images/banner-us-2023-06-28-09-25-32-8512.png', NULL, '2023-06-29 01:25:39'),
	(12, 'Join as a provider', 1, 'uploads/website-images/banner-us-2023-05-25-08-41-10-5447.png', NULL, '2023-05-25 12:41:13'),
	(13, 'Material', 1, 'uploads/website-images/banner-us-2023-06-28-09-22-12-3929.jpg', '2023-06-28 15:19:52', '2023-06-29 01:22:18'),
	(14, 'Construction', 1, 'uploads/website-images/banner-us-2023-06-28-09-39-00-7150.png', '2023-06-28 15:37:23', '2023-06-29 01:39:18');

-- Volcando datos para la tabla system_service.captcha: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.categories: ~6 rows (aproximadamente)
INSERT INTO `categories` (`id`, `name`, `slug`, `icon`, `image`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Plomero', 'painting', 'uploads/custom-images/category-2022-12-04-05-48-57-1083.png', 'uploads/custom-images/category-2022-09-29-01-25-32-7758.jpg', 1, '2022-09-29 07:25:32', '2022-12-04 11:48:58'),
	(2, 'Pintor', 'cleaning', 'uploads/custom-images/category-2022-12-04-05-49-19-9484.png', 'uploads/custom-images/category-2022-11-06-01-29-18-4240.jpg', 1, '2022-09-29 07:26:46', '2022-12-04 11:49:19'),
	(3, 'Electricista', 'pest-control', 'uploads/custom-images/category-2022-12-04-05-54-52-6882.png', 'uploads/custom-images/category-2022-09-29-01-27-41-4934.jpg', 1, '2022-09-29 07:27:41', '2022-12-04 11:54:52'),
	(4, 'Evaluador', 'ac-repair', 'uploads/custom-images/category-2022-12-04-05-49-40-7608.png', 'uploads/custom-images/category-2022-09-29-01-28-59-3308.jpg', 1, '2022-09-29 07:28:59', '2022-12-04 11:49:40'),
	(5, 'Carpintero', 'car-services', 'uploads/custom-images/category-2022-12-04-05-49-57-3871.png', 'uploads/custom-images/category-2022-09-29-01-29-56-4318.jpg', 1, '2022-09-29 07:29:56', '2022-12-04 11:49:57'),
	(6, 'Albañil', 'plumbing', 'uploads/custom-images/category-2022-12-04-05-59-25-1109.png', 'uploads/custom-images/category-2022-09-29-01-31-49-2090.jpg', 1, '2022-09-29 07:31:49', '2022-12-04 11:59:25');

-- Volcando datos para la tabla system_service.citas: ~15 rows (aproximadamente)
INSERT INTO `citas` (`id`, `name`, `detail_construction_id`, `email`, `phone`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'ramiro mamani soto', 2, '201601ramiro@gmail.com', 67470820, '2023-06-29 09:52:28', '2023-06-29 13:11:53', 'aprobado'),
	(2, 'jhuliza delgadillo moscoso', 2, '201601ramiro@gmail.com', 67470820, '2023-06-29 09:54:03', '2023-06-29 09:54:03', 'en espera'),
	(3, 'ronal mamani soto', 2, '201601ramiro@gmail.com', 67470820, '2023-06-29 09:57:47', '2023-06-29 09:57:47', 'en espera'),
	(4, 'jhuliza te amo', 2, '201601ramiro@gmail.com', 67470820, '2023-06-29 10:03:26', '2023-06-29 10:03:26', 'aprobado'),
	(5, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:01', '2023-06-30 09:39:01', 'en espera'),
	(6, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:19', '2023-06-30 09:39:19', 'en espera'),
	(7, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:31', '2023-06-30 09:39:31', 'en espera'),
	(8, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:36', '2023-06-30 09:39:36', 'en espera'),
	(9, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:41', '2023-06-30 09:39:41', 'en espera'),
	(10, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:44', '2023-06-30 09:39:44', 'en espera'),
	(11, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:48', '2023-06-30 09:39:48', 'en espera'),
	(12, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:39:52', '2023-06-30 09:39:52', 'en espera'),
	(13, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:40:11', '2023-06-30 09:40:11', 'en espera'),
	(14, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:40:21', '2023-06-30 09:40:21', 'en espera'),
	(15, 'RAMIRO', 1, '201601ramiro@gmail.com', 67470820, '2023-06-30 09:40:32', '2023-06-30 09:40:32', 'en espera');

-- Volcando datos para la tabla system_service.cities: ~9 rows (aproximadamente)
INSERT INTO `cities` (`id`, `country_state_id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Centro-primer anillo', 'centro-primer-anillo', 1, '2022-01-30 09:29:19', '2022-02-06 04:18:33'),
	(2, 1, 'Nueva Santa Cruz', 'nueva-santa-cruz', 1, '2022-01-30 09:29:29', '2022-02-06 04:20:30'),
	(4, 1, 'Primer Anillo', 'primer-anillo', 1, '2022-02-06 04:18:49', '2022-02-06 04:18:49'),
	(5, 3, 'Quillacollo', 'quillacollo', 1, '2022-02-06 04:19:56', '2022-02-06 04:19:56'),
	(6, 1, 'Tronpillo', 'tronpillo', 1, '2022-02-06 04:21:08', '2022-02-06 04:21:08'),
	(7, 1, 'Warnes', 'warnes', 1, '2022-02-06 04:21:26', '2022-02-06 04:21:26'),
	(8, 3, 'Sacaba', 'sacaba', 1, '2022-02-06 04:22:21', '2022-02-06 04:22:21'),
	(9, 3, 'Cercado', 'cercado', 1, '2022-02-06 04:22:44', '2022-02-06 04:22:44'),
	(10, 2, 'El alto', 'el-alto', 1, '2022-02-06 04:23:12', '2022-02-06 04:23:12');

-- Volcando datos para la tabla system_service.company: ~3 rows (aproximadamente)
INSERT INTO `company` (`id`, `name`, `imagen`, `address`, `nit`, `represent`, `type_company_id`, `user_id`, `phone`, `created_at`, `updated_at`, `latitude`, `longitude`, `status`, `make_popular`, `description`) VALUES
	(3, 'Redafor', 'uploads/custom-images/Company-2023-06-15-08-47-38-5062.png', 'Segundo anillo Santa cruz', 12345678, 'Chistian Yabeta', 1, 13, 125345678, '2023-06-15 12:47:39', '2023-06-15 12:47:39', '-17.798118495741598', '-63.17162428564453', 1, 1, 'En Redafor, nos enorgullece ofrecer una amplia gama de materiales de construcción para todos nuestros clientes y servicios relacionados');

-- Volcando datos para la tabla system_service.complete_requests: ~3 rows (aproximadamente)
INSERT INTO `complete_requests` (`id`, `provider_id`, `order_id`, `resone`, `created_at`, `updated_at`) VALUES
	(1, 2, 6, 'this is test resone', '2022-11-10 05:38:11', '2022-11-10 05:38:11'),
	(2, 2, 9, 'this is test resone', '2022-11-10 05:44:49', '2022-11-10 05:44:49'),
	(3, 2, 10, 'Please complete the booking.', '2022-12-21 03:48:10', '2022-12-21 03:48:10');

-- Volcando datos para la tabla system_service.confort_property: ~40 rows (aproximadamente)
INSERT INTO `confort_property` (`id`, `details`, `code`, `property_id`, `created_at`, `updated_at`) VALUES
	(1, 'Box', NULL, 8, '2023-05-10 11:15:13', '2023-05-10 12:38:25'),
	(2, 'Depósito', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:38:25'),
	(3, 'Placard en cocina', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:38:25'),
	(4, 'Placard en dormitorio', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:38:25'),
	(5, 'Terraza', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:42:18'),
	(6, 'Ascensor', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:42:18'),
	(7, 'Salón de uso común', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:42:18'),
	(8, 'Instalación de TV cable', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:42:18'),
	(9, 'Gas por Cañería', NULL, 8, '2023-05-10 11:15:14', '2023-05-10 12:42:18'),
	(10, 'Living comedor', NULL, 8, '2023-05-10 11:20:53', '2023-05-10 12:42:19'),
	(11, 'Terrazas', NULL, 8, '2023-05-10 12:32:34', '2023-05-10 12:42:19'),
	(16, 'Box', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(17, 'Depósito', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(18, 'Placard en cocina', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(19, 'Placard en dormitorio', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(20, 'Terraza · Ascensor', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(21, 'Salón de uso común', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(22, 'Instalación de TV cable', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(23, 'Gas por Cañería', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(24, 'Living comedor', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(25, 'Terrazas', NULL, 9, '2023-05-11 02:03:23', '2023-05-11 02:03:23'),
	(26, 'Box', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(27, 'Depósito', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(28, 'Placard en cocina', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(29, 'Placard en dormitorio', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(30, 'Terraza · Ascensor', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(31, 'Salón de uso común', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(32, 'Instalación de TV cable', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(33, 'Gas por Cañería', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(34, 'Living comedor', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(35, 'Terrazas', NULL, 10, '2023-05-11 02:27:10', '2023-05-11 02:27:10'),
	(36, 'Box', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(37, 'Depósito', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(38, 'Placard en cocina', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(39, 'Placard en dormitorio', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(40, 'Terraza', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(41, 'Ascensor', NULL, 11, '2023-05-11 02:36:42', '2023-05-11 02:36:42'),
	(42, 'Salón de uso común', NULL, 11, '2023-05-11 02:36:43', '2023-05-11 02:36:43'),
	(43, 'Instalación de TV cable', NULL, 11, '2023-05-11 02:36:43', '2023-05-11 02:36:43'),
	(44, 'Living comedor', NULL, 11, '2023-05-11 02:36:43', '2023-05-11 02:36:43');

-- Volcando datos para la tabla system_service.construction: ~2 rows (aproximadamente)
INSERT INTO `construction` (`id`, `name`, `description`, `code`, `price`, `phone`, `image`, `status`, `approve_by_admin`, `make_featured`, `make_popular`, `date`, `is_banned`, `users_id`, `longitude`, `latitude`, `address`, `created_at`, `updated_at`, `cities_id`) VALUES
	(1, 'CONSTRUCTORA ICONICA', 'Icónica SRL orienta sus esfuerzos a que todos los Bolivianos tengan casa propia y tiene como misión gestionar financiamiento crediticio y construir viviendas que dignifiquen la vida familiar y comunitaria de los Bolivianos.', 'ASTM A 496', 45600, 75020158, 'uploads/custom-images/Constructora-2023-06-24-02-09-47-6728.png', 1, 1, 1, 1, NULL, 0, 1, '-63.171334607070925', '-17.798371325421986', 'Av. El Tronpillo  #1100  local 8', '2023-06-20 03:03:41', '2023-06-29 12:31:08', 6),
	(2, 'Desocom', 'Producto laminado en caliente con sección en forma de “U” (con alas paralelas), de calidad dual porque cumple con las normas ASTM A36 y ASTM A572 Grado 50 simultáneamente.', 'ASTM A 496', 45000, 67470820, 'uploads/custom-images/Constructora-2023-06-29-08-15-17-6747.png', 1, 1, 0, 0, NULL, 0, 1, '-63.16398535437012', '-17.781650595522123', 'tronpillo', '2023-06-29 12:15:18', '2023-06-29 12:15:18', 1);

-- Volcando datos para la tabla system_service.contact_messages: ~4 rows (aproximadamente)
INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
	(2, 'John Doe', 'user@gmail.com', '123-343-4444', 'Subscribe Verification', 'Feel Free to Get in Touch', '2022-12-21 03:20:18', '2022-12-21 03:20:18'),
	(3, 'John Doe', 'user@gmail.com', '123-343-4444', 'Subscribe Verification', 'Feel Free to Get in Touch', '2022-12-21 03:24:38', '2022-12-21 03:24:38'),
	(4, 'John Doe', 'agent@gmail.com', '123-343-4444', 'Subscribe Verification', 'Fill the form now & Request an Estimate', '2022-12-21 03:25:12', '2022-12-21 03:25:12'),
	(6, 'John Doe', 'user@gmail.com', '123-343-4444', 'Subscribe Verification', 'Do you have any question ?\r\nFill the form now &amp; Request an Estimate', '2023-01-15 05:24:20', '2023-01-15 05:24:20');

-- Volcando datos para la tabla system_service.contact_pages: ~0 rows (aproximadamente)
INSERT INTO `contact_pages` (`id`, `supporter_image`, `support_time`, `off_day`, `email`, `address`, `phone`, `map`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/website-images/supporter--2022-08-28-02-04-43-1575.jpg', '10.00AM to 07.00PM', 'Friday Off', 'info@redafor.com\r\nInformaciones y solicitudes', 'Edf. Los Jardines\r\nSopocachi, 6 de Agosto.', '+591 70101010\r\nAtención al cliente', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.681138843672!2d-73.89482218459395!3d40.747041279328165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25f01328248b3%3A0x62300784dd275f96!2s7232%20Broadway%20%23%20308%2C%20Flushing%2C%20NY%2011372%2C%20USA!5e0!3m2!1sen!2sbd!4v1652467683397!5m2!1sen!2sbd" width="600" height="450" style="border:0;" style="align: center" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>', '2022-01-30 12:31:58', '2023-05-04 12:39:55');

-- Volcando datos para la tabla system_service.cookie_consents: ~0 rows (aproximadamente)
INSERT INTO `cookie_consents` (`id`, `status`, `border`, `corners`, `background_color`, `text_color`, `border_color`, `btn_bg_color`, `btn_text_color`, `message`, `link_text`, `btn_text`, `link`, `created_at`, `updated_at`) VALUES
	(1, 1, 'thin', 'normal', '#184dec', '#f9f6f6', '#0a58d6', '#fffceb', '#222758', 'Bienvenido a REDAFOR un sistema confiable donde puedes adquirir lo que mas necesitas al instante.', 'More Info', 'Yes', NULL, NULL, '2023-07-04 13:16:34');

-- Volcando datos para la tabla system_service.counters: ~4 rows (aproximadamente)
INSERT INTO `counters` (`id`, `title`, `icon`, `number`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Total Orders', 'uploads/custom-images/counter--2022-09-29-12-40-42-5094.png', '2547', 1, '2022-09-29 06:40:42', '2022-09-29 06:40:42'),
	(2, 'Active Clients', 'uploads/custom-images/counter--2022-09-29-12-41-15-9354.png', '1532', 1, '2022-09-29 06:41:15', '2022-09-29 06:41:15'),
	(3, 'Team Members', 'uploads/custom-images/counter--2022-09-29-12-41-37-4353.png', '2103', 1, '2022-09-29 06:41:37', '2022-09-29 06:41:37'),
	(4, 'Years of Experience', 'uploads/custom-images/counter--2022-09-29-12-42-06-6458.png', '25', 1, '2022-09-29 06:42:06', '2022-09-29 06:42:06');

-- Volcando datos para la tabla system_service.countries: ~4 rows (aproximadamente)
INSERT INTO `countries` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Bolivia', 'bolivia', 1, '2022-01-30 09:28:28', '2022-02-06 04:11:42'),
	(2, 'Chile', 'chile', 1, '2022-01-30 09:28:39', '2022-08-30 06:18:46'),
	(4, 'Peru', 'peru', 1, '2022-02-06 04:11:51', '2022-02-06 04:11:51'),
	(5, 'Argentina', 'argentina', 1, '2022-02-06 04:12:36', '2022-02-06 04:12:36');

-- Volcando datos para la tabla system_service.country_states: ~9 rows (aproximadamente)
INSERT INTO `country_states` (`id`, `country_id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Santa Cruz', 'santa-cruz', 1, '2022-01-30 09:29:00', '2022-02-06 04:14:28'),
	(2, 1, 'La Paz', 'la-paz', 1, '2022-01-30 09:29:07', '2022-02-06 04:14:42'),
	(3, 1, 'Cochabamba', 'cochabamba', 1, '2022-02-05 07:49:14', '2022-02-06 04:15:09'),
	(4, 1, 'Oruro', 'oruro', 1, '2022-02-06 04:16:27', '2022-02-06 04:16:27'),
	(5, 1, 'Potosi', 'potosi', 1, '2022-02-06 04:16:39', '2022-02-06 04:16:39'),
	(6, 1, 'Tarija', 'tarija', 1, '2022-02-06 04:16:48', '2022-02-06 04:16:48'),
	(7, 1, 'Chuquisaca', 'chuquisaca', 1, '2022-02-06 04:17:35', '2022-02-06 04:17:35'),
	(8, 1, 'Beni', 'beni', 1, '2022-02-06 04:17:44', '2022-02-06 04:17:44'),
	(9, 1, 'Pando', 'pando', 1, '2022-02-06 04:17:53', '2022-02-06 04:17:53');

-- Volcando datos para la tabla system_service.coupons: ~6 rows (aproximadamente)
INSERT INTO `coupons` (`id`, `provider_id`, `coupon_code`, `offer_percentage`, `expired_date`, `price`, `status`, `limit_coupon`, `created_at`, `updated_at`) VALUES
	(2, 2, 'newyear23', '15', '2023-03-29', 0, 1, 0, '2023-03-09 05:40:26', '2023-03-09 05:50:28'),
	(3, 0, 'blackfriday', '25', '2023-03-31', 0, 1, 0, '2023-03-09 06:24:24', '2023-03-09 06:24:24'),
	(4, 0, 'startsunday', '20', '2023-03-28', 0, 1, 0, '2023-03-09 06:24:38', '2023-03-09 06:24:38'),
	(5, 4, '1200123', '10', '2023-05-24', 0, 1, 0, '2023-05-23 12:57:23', '2023-05-23 12:57:23'),
	(6, 0, '123245121', '10', '2023-07-29', 150, 1, 10, '2023-06-08 02:43:02', '2023-06-08 02:43:02'),
	(7, 0, 'venta de cupones', '0', '2023-07-31', 20, 1, 10, '2023-06-10 09:57:12', '2023-06-10 09:57:12');

-- Volcando datos para la tabla system_service.coupon_histories: ~8 rows (aproximadamente)
INSERT INTO `coupon_histories` (`id`, `provider_id`, `user_id`, `coupon_code`, `coupon_id`, `discount_amount`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 'newyear23', 2, '20', '2023-03-09 06:01:18', NULL),
	(2, 0, 1, 'blackfriday', 3, '20', '2023-03-09 06:01:18', NULL),
	(3, 2, 1, 'newyear23', 2, '6.3', '2023-03-09 09:06:50', '2023-03-09 09:06:50'),
	(4, 2, 1, 'newyear23', 2, '5.1', '2023-03-09 09:13:28', '2023-03-09 09:13:28'),
	(5, 0, 1, 'blackfriday', 3, '8.5', '2023-03-09 09:19:31', '2023-03-09 09:19:31'),
	(6, 0, 1, 'startsunday', 4, '7.4', '2023-03-09 09:24:22', '2023-03-09 09:24:22'),
	(7, 2, 1, 'newyear23', 2, '1.5', '2023-03-09 09:27:37', '2023-03-09 09:27:37'),
	(8, 2, 1, 'newyear23', 2, '5.1', '2023-03-09 09:31:14', '2023-03-09 09:31:14');

-- Volcando datos para la tabla system_service.currencies: ~164 rows (aproximadamente)
INSERT INTO `currencies` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'AFA', 'Afghan Afghani', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'ALL', 'Albanian Lek', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'DZD', 'Algerian Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'AOA', 'Angolan Kwanza', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'ARS', 'Argentine Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 'AMD', 'Armenian Dram', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 'AWG', 'Aruban Florin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 'AUD', 'Australian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 'AZN', 'Azerbaijani Manat', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 'BSD', 'Bahamian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 'BHD', 'Bahraini Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 'BDT', 'Bangladeshi Taka', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 'BBD', 'Barbadian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 'BYR', 'Belarusian Ruble', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 'BEF', 'Belgian Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 'BZD', 'Belize Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 'BMD', 'Bermudan Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 'BTN', 'Bhutanese Ngultrum', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(19, 'BTC', 'Bitcoin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(20, 'BOB', 'Bolivian Boliviano', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, 'BAM', 'Bosnia-Herzegovina Convertible Mark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, 'BWP', 'Botswanan Pula', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, 'BRL', 'Brazilian Real', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, 'GBP', 'British Pound Sterling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(25, 'BND', 'Brunei Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, 'BGN', 'Bulgarian Lev', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, 'BIF', 'Burundian Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, 'KHR', 'Cambodian Riel', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, 'CAD', 'Canadian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, 'CVE', 'Cape Verdean Escudo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, 'KYD', 'Cayman Islands Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(32, 'XOF', 'CFA Franc BCEAO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(33, 'XAF', 'CFA Franc BEAC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(34, 'XPF', 'CFP Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(35, 'CLP', 'Chilean Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(36, 'CNY', 'Chinese Yuan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(37, 'COP', 'Colombian Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(38, 'KMF', 'Comorian Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(39, 'CDF', 'Congolese Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(40, 'CRC', 'Costa Rican ColÃ³n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(41, 'HRK', 'Croatian Kuna', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(42, 'CUC', 'Cuban Convertible Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(43, 'CZK', 'Czech Republic Koruna', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(44, 'DKK', 'Danish Krone', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(45, 'DJF', 'Djiboutian Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(46, 'DOP', 'Dominican Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(47, 'XCD', 'East Caribbean Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(48, 'EGP', 'Egyptian Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(49, 'ERN', 'Eritrean Nakfa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(50, 'EEK', 'Estonian Kroon', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(51, 'ETB', 'Ethiopian Birr', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(52, 'EUR', 'Euro', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(53, 'FKP', 'Falkland Islands Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(54, 'FJD', 'Fijian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(55, 'GMD', 'Gambian Dalasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(56, 'GEL', 'Georgian Lari', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(57, 'DEM', 'German Mark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(58, 'GHS', 'Ghanaian Cedi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(59, 'GIP', 'Gibraltar Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(60, 'GRD', 'Greek Drachma', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(61, 'GTQ', 'Guatemalan Quetzal', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(62, 'GNF', 'Guinean Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(63, 'GYD', 'Guyanaese Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(64, 'HTG', 'Haitian Gourde', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(65, 'HNL', 'Honduran Lempira', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(66, 'HKD', 'Hong Kong Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(67, 'HUF', 'Hungarian Forint', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(68, 'ISK', 'Icelandic KrÃ³na', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(69, 'INR', 'Indian Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(70, 'IDR', 'Indonesian Rupiah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(71, 'IRR', 'Iranian Rial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(72, 'IQD', 'Iraqi Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(73, 'ILS', 'Israeli New Sheqel', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(74, 'ITL', 'Italian Lira', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(75, 'JMD', 'Jamaican Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(76, 'JPY', 'Japanese Yen', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(77, 'JOD', 'Jordanian Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(78, 'KZT', 'Kazakhstani Tenge', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(79, 'KES', 'Kenyan Shilling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(80, 'KWD', 'Kuwaiti Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(81, 'KGS', 'Kyrgystani Som', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(82, 'LAK', 'Laotian Kip', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(83, 'LVL', 'Latvian Lats', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(84, 'LBP', 'Lebanese Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(85, 'LSL', 'Lesotho Loti', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(86, 'LRD', 'Liberian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(87, 'LYD', 'Libyan Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(88, 'LTL', 'Lithuanian Litas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(89, 'MOP', 'Macanese Pataca', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(90, 'MKD', 'Macedonian Denar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(91, 'MGA', 'Malagasy Ariary', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(92, 'MWK', 'Malawian Kwacha', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(93, 'MYR', 'Malaysian Ringgit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(94, 'MVR', 'Maldivian Rufiyaa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(95, 'MRO', 'Mauritanian Ouguiya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(96, 'MUR', 'Mauritian Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(97, 'MXN', 'Mexican Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(98, 'MDL', 'Moldovan Leu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(99, 'MNT', 'Mongolian Tugrik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(100, 'MAD', 'Moroccan Dirham', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(101, 'MZM', 'Mozambican Metical', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(102, 'MMK', 'Myanmar Kyat', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(103, 'NAD', 'Namibian Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(104, 'NPR', 'Nepalese Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(105, 'ANG', 'Netherlands Antillean Guilder', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(106, 'TWD', 'New Taiwan Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(107, 'NZD', 'New Zealand Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(108, 'NIO', 'Nicaraguan CÃ³rdoba', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(109, 'NGN', 'Nigerian Naira', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(110, 'KPW', 'North Korean Won', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(111, 'NOK', 'Norwegian Krone', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(112, 'OMR', 'Omani Rial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(113, 'PKR', 'Pakistani Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(114, 'PAB', 'Panamanian Balboa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(115, 'PGK', 'Papua New Guinean Kina', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(116, 'PYG', 'Paraguayan Guarani', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(117, 'PEN', 'Peruvian Nuevo Sol', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(118, 'PHP', 'Philippine Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(119, 'PLN', 'Polish Zloty', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(120, 'QAR', 'Qatari Rial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(121, 'RON', 'Romanian Leu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(122, 'RUB', 'Russian Ruble', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(123, 'RWF', 'Rwandan Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(124, 'SVC', 'Salvadoran ColÃ³n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(125, 'WST', 'Samoan Tala', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(126, 'SAR', 'Saudi Riyal', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(127, 'RSD', 'Serbian Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(128, 'SCR', 'Seychellois Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(129, 'SLL', 'Sierra Leonean Leone', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(130, 'SGD', 'Singapore Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(131, 'SKK', 'Slovak Koruna', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(132, 'SBD', 'Solomon Islands Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(133, 'SOS', 'Somali Shilling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(134, 'ZAR', 'South African Rand', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(135, 'KRW', 'South Korean Won', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(136, 'XDR', 'Special Drawing Rights', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(137, 'LKR', 'Sri Lankan Rupee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(138, 'SHP', 'St. Helena Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(139, 'SDG', 'Sudanese Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(140, 'SRD', 'Surinamese Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(141, 'SZL', 'Swazi Lilangeni', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(142, 'SEK', 'Swedish Krona', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(143, 'CHF', 'Swiss Franc', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(144, 'SYP', 'Syrian Pound', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(145, 'STD', 'São Tomé and Príncipe Dobra', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(146, 'TJS', 'Tajikistani Somoni', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(147, 'TZS', 'Tanzanian Shilling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(148, 'THB', 'Thai Baht', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(149, 'TOP', 'Tongan pa\'anga', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(150, 'TTD', 'Trinidad & Tobago Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(151, 'TND', 'Tunisian Dinar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(152, 'TRY', 'Turkish Lira', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(153, 'TMT', 'Turkmenistani Manat', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(154, 'UGX', 'Ugandan Shilling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(155, 'UAH', 'Ukrainian Hryvnia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(156, 'AED', 'United Arab Emirates Dirham', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(157, 'UYU', 'Uruguayan Peso', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(158, 'USD', 'US Dollar', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(159, 'UZS', 'Uzbekistan Som', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(160, 'VUV', 'Vanuatu Vatu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(161, 'VEF', 'Venezuelan BolÃ­var', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(162, 'VND', 'Vietnamese Dong', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(163, 'YER', 'Yemeni Rial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(164, 'ZMK', 'Zambian Kwacha', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- Volcando datos para la tabla system_service.currency_countries: ~249 rows (aproximadamente)
INSERT INTO `currency_countries` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'Andorra', 'AD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'Afghanistan', 'AF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'Åland Islands', 'AX', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'Albania', 'AL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'Algeria', 'DZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 'American Samoa', 'AS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 'Angola', 'AO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 'Anguilla', 'AI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 'Antarctica', 'AQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 'Antigua and Barbuda', 'AG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 'Argentina', 'AR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 'Armenia', 'AM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 'Aruba', 'AW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 'Australia', 'AU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 'Austria', 'AT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 'Azerbaijan', 'AZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 'Bahamas', 'BS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 'Bahrain', 'BH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(19, 'Bangladesh', 'BD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(20, 'Barbados', 'BB', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, 'Belarus', 'BY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, 'Belgium', 'BE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, 'Belize', 'BZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, 'Benin', 'BJ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(25, 'Bermuda', 'BM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, 'Bhutan', 'BT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, 'Bolivia (Plurinational State of)', 'BO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, 'Bonaire, Sint Eustatius and Saba', 'BQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, 'Bosnia and Herzegovina', 'BA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, 'Botswana', 'BW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, 'Bouvet Island', 'BV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(32, 'Brazil', 'BR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(33, 'British Indian Ocean Territory', 'IO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(34, 'Brunei Darussalam', 'BN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(35, 'Bulgaria', 'BG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(36, 'Burkina Faso', 'BF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(37, 'Burundi', 'BI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(38, 'Cabo Verde', 'CV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(39, 'Cambodia', 'KH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(40, 'Cameroon', 'CM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(41, 'Canada', 'CA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(42, 'Cayman Islands', 'KY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(43, 'Central African Republic', 'CF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(44, 'Chad', 'TD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(45, 'Chile', 'CL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(46, 'China', 'CN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(47, 'Christmas Island', 'CX', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(48, 'Cocos (Keeling) Islands', 'CC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(49, 'Colombia', 'CO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(50, 'Comoros', 'KM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(51, 'Congo', 'CG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(52, 'Congo (Democratic Republic of the)', 'CD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(53, 'Cook Islands', 'CK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(54, 'Costa Rica', 'CR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(55, 'Côte d\'Ivoire', 'CI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(56, 'Croatia', 'HR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(57, 'Cuba', 'CU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(58, 'Curaçao', 'CW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(59, 'Cyprus', 'CY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(60, 'Czech Republic', 'CZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(61, 'Denmark', 'DK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(62, 'Djibouti', 'DJ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(63, 'Dominica', 'DM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(64, 'Dominican Republic', 'DO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(65, 'Ecuador', 'EC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(66, 'Egypt', 'EG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(67, 'El Salvador', 'SV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(68, 'Equatorial Guinea', 'GQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(69, 'Eritrea', 'ER', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(70, 'Estonia', 'EE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(71, 'Ethiopia', 'ET', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(72, 'Falkland Islands (Malvinas)', 'FK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(73, 'Faroe Islands', 'FO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(74, 'Fiji', 'FJ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(75, 'Finland', 'FI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(76, 'France', 'FR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(77, 'French Guiana', 'GF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(78, 'French Polynesia', 'PF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(79, 'French Southern Territories', 'TF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(80, 'Gabon', 'GA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(81, 'Gambia', 'GM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(82, 'Georgia', 'GE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(83, 'Germany', 'DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(84, 'Ghana', 'GH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(85, 'Gibraltar', 'GI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(86, 'Greece', 'GR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(87, 'Greenland', 'GL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(88, 'Grenada', 'GD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(89, 'Guadeloupe', 'GP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(90, 'Guam', 'GU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(91, 'Guatemala', 'GT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(92, 'Guernsey', 'GG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(93, 'Guinea', 'GN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(94, 'Guinea-Bissau', 'GW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(95, 'Guyana', 'GY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(96, 'Haiti', 'HT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(97, 'Heard Island and McDonald Islands', 'HM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(98, 'Holy See', 'VA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(99, 'Honduras', 'HN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(100, 'Hong Kong', 'HK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(101, 'Hungary', 'HU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(102, 'Iceland', 'IS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(103, 'India', 'IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(104, 'Indonesia', 'ID', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(105, 'Iran (Islamic Republic of)', 'IR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(106, 'Iraq', 'IQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(107, 'Ireland', 'IE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(108, 'Isle of Man', 'IM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(109, 'Israel', 'IL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(110, 'Italy', 'IT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(111, 'Jamaica', 'JM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(112, 'Japan', 'JP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(113, 'Jersey', 'JE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(114, 'Jordan', 'JO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(115, 'Kazakhstan', 'KZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(116, 'Kenya', 'KE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(117, 'Kiribati', 'KI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(118, 'Korea (Democratic People\'s Republic of)', 'KP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(119, 'Korea (Republic of)', 'KR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(120, 'Kuwait', 'KW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(121, 'Kyrgyzstan', 'KG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(122, 'Lao People\'s Democratic Republic', 'LA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(123, 'Latvia', 'LV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(124, 'Lebanon', 'LB', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(125, 'Lesotho', 'LS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(126, 'Liberia', 'LR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(127, 'Libya', 'LY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(128, 'Liechtenstein', 'LI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(129, 'Lithuania', 'LT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(130, 'Luxembourg', 'LU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(131, 'Macao', 'MO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(132, 'Macedonia (the former Yugoslav Republic of)', 'MK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(133, 'Madagascar', 'MG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(134, 'Malawi', 'MW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(135, 'Malaysia', 'MY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(136, 'Maldives', 'MV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(137, 'Mali', 'ML', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(138, 'Malta', 'MT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(139, 'Marshall Islands', 'MH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(140, 'Martinique', 'MQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(141, 'Mauritania', 'MR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(142, 'Mauritius', 'MU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(143, 'Mayotte', 'YT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(144, 'Mexico', 'MX', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(145, 'Micronesia (Federated States of)', 'FM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(146, 'Moldova (Republic of)', 'MD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(147, 'Monaco', 'MC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(148, 'Mongolia', 'MN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(149, 'Montenegro', 'ME', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(150, 'Montserrat', 'MS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(151, 'Morocco', 'MA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(152, 'Mozambique', 'MZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(153, 'Myanmar', 'MM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(154, 'Namibia', 'NA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(155, 'Nauru', 'NR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(156, 'Nepal', 'NP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(157, 'Netherlands', 'NL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(158, 'New Caledonia', 'NC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(159, 'New Zealand', 'NZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(160, 'Nicaragua', 'NI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(161, 'Niger', 'NE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(162, 'Nigeria', 'NG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(163, 'Niue', 'NU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(164, 'Norfolk Island', 'NF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(165, 'Northern Mariana Islands', 'MP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(166, 'Norway', 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(167, 'Oman', 'OM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(168, 'Pakistan', 'PK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(169, 'Palau', 'PW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(170, 'Palestine, State of', 'PS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(171, 'Panama', 'PA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(172, 'Papua New Guinea', 'PG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(173, 'Paraguay', 'PY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(174, 'Peru', 'PE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(175, 'Philippines', 'PH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(176, 'Pitcairn', 'PN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(177, 'Poland', 'PL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(178, 'Portugal', 'PT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(179, 'Puerto Rico', 'PR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(180, 'Qatar', 'QA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(181, 'Réunion', 'RE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(182, 'Romania', 'RO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(183, 'Russian Federation', 'RU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(184, 'Rwanda', 'RW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(185, 'Saint Barthélemy', 'BL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(186, 'Saint Helena, Ascension and Tristan da Cunha', 'SH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(187, 'Saint Kitts and Nevis', 'KN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(188, 'Saint Lucia', 'LC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(189, 'Saint Martin (French part)', 'MF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(190, 'Saint Pierre and Miquelon', 'PM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(191, 'Saint Vincent and the Grenadines', 'VC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(192, 'Samoa', 'WS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(193, 'San Marino', 'SM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(194, 'Sao Tome and Principe', 'ST', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(195, 'Saudi Arabia', 'SA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(196, 'Senegal', 'SN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(197, 'Serbia', 'RS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(198, 'Seychelles', 'SC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(199, 'Sierra Leone', 'SL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(200, 'Singapore', 'SG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(201, 'Sint Maarten (Dutch part)', 'SX', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(202, 'Slovakia', 'SK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(203, 'Slovenia', 'SI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(204, 'Solomon Islands', 'SB', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(205, 'Somalia', 'SO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(206, 'South Africa', 'ZA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(207, 'South Georgia and the South Sandwich Islands', 'GS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(208, 'South Sudan', 'SS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(209, 'Spain', 'ES', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(210, 'Sri Lanka', 'LK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(211, 'Sudan', 'SD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(212, 'Suriname', 'SR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(213, 'Svalbard and Jan Mayen', 'SJ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(214, 'Swaziland', 'SZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(215, 'Sweden', 'SE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(216, 'Switzerland', 'CH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(217, 'Syrian Arab Republic', 'SY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(218, 'Taiwan, Province of China', 'TW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(219, 'Tajikistan', 'TJ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(220, 'Tanzania, United Republic of', 'TZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(221, 'Thailand', 'TH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(222, 'Timor-Leste', 'TL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(223, 'Togo', 'TG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(224, 'Tokelau', 'TK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(225, 'Tonga', 'TO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(226, 'Trinidad and Tobago', 'TT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(227, 'Tunisia', 'TN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(228, 'Turkey', 'TR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(229, 'Turkmenistan', 'TM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(230, 'Turks and Caicos Islands', 'TC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(231, 'Tuvalu', 'TV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(232, 'Uganda', 'UG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(233, 'Ukraine', 'UA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(234, 'United Arab Emirates', 'AE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(235, 'United Kingdom of Great Britain and Northern Ireland', 'GB', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(236, 'United States Minor Outlying Islands', 'UM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(237, 'United States of America', 'US', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(238, 'Uruguay', 'UY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(239, 'Uzbekistan', 'UZ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(240, 'Vanuatu', 'VU', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(241, 'Venezuela (Bolivarian Republic of)', 'VE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(242, 'Viet Nam', 'VN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(243, 'Virgin Islands (British)', 'VG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(244, 'Virgin Islands (U.S.)', 'VI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(245, 'Wallis and Futuna', 'WF', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(246, 'Western Sahara', 'EH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(247, 'Yemen', 'YE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(248, 'Zambia', 'ZM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(249, 'Zimbabwe', 'ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- Volcando datos para la tabla system_service.currency_property: ~0 rows (aproximadamente)
INSERT INTO `currency_property` (`id`, `name`, `worth`, `sale`, `buys`, `created_at`, `updated_at`) VALUES
	(1, 'dolar', '$us', 6.96, 6.98, '2023-05-08 20:12:00', '2023-05-08 20:12:01');

-- Volcando datos para la tabla system_service.custom_pages: ~2 rows (aproximadamente)
INSERT INTO `custom_pages` (`id`, `page_name`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Custom Page One', 'custom-page-one', '<p><strong>1. What is custom page?</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriefss asbut also the on leap into a electironc typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andeiss more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p><strong>2. How does work custom page</strong></p>\r\n<p>While it&rsquo;s not legally required for ecommerce websites to have a terms and conditions agreement, adding one will help protect your as sonline business.As terms and conditions are legally enforceable rules, they allow you to set standards for how users interact with your site. Here are some of the major abenefits of including terms and conditions on your ecommerce site:</p>\r\n<p>It has survived not only five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the obb1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop.</p>\r\n<p><strong>Features :</strong></p>\r\n<ul>\r\n<li>Slim body with metal cover</li>\r\n<li>Latest Intel Core i5-1135G7 processor (4 cores / 8 threads)</li>\r\n<li>8GB DDR4 RAM and fast 512GB PCIe SSD</li>\r\n<li>NVIDIA GeForce MX350 2GB GDDR5 graphics card backlit keyboard, touchpad with gesture support</li>\r\n</ul>\r\n<p><strong>3. Protect Your Property</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriezcs but also the on leap into as eylectronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraszvxet sheets containing Lorem Ipsum our spassages, andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book. five centuries but also a the on leap into electronic typesetting, remaining essentially unchanged. It aswasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop our aspublishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p><strong>4. What to Include in Terms and Conditions for Online Stores</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five as centuries but also the on leap into as electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of as Leitraset sheets containing Loriem Ipsum passages, our andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p>Five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing software like Aldus PageMaker our as including versions of Loriem Ipsum to make a type specimen book. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets as containing Lorem Ipsum passages, andei more recently with a desktop publishing software like Aldus PageMaker including versions of Loremas&nbsp; Ipsum to make a type specimen book.</p>\r\n<p><strong>05.Pricing and Payment Terms</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five as centuries but also as the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release as of Letraset sheets containing Lorem Ipsum our spassages, andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p>Five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing software like Aldus PageMaker our as including versions of Lorem aIpsum to make a type specimen book. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheetsasd containing Lorem Ipsum passages, andei more recentlysl with desktop publishing software like Aldus PageMaker including versions of Loremadfsfds Ipsum to make a type specimen book.</p>\r\n<p>It has survived not only five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the our 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing asou software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>', 1, '2022-09-29 04:30:53', '2023-01-18 09:16:13'),
	(2, 'Custom Page Two', 'custom-page-two', '<p><strong>1. What is custom page?</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriefss asbut also the on leap into a electironc typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andeiss more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p><strong>2. How does work custom page</strong></p>\r\n<p>While it&rsquo;s not legally required for ecommerce websites to have a terms and conditions agreement, adding one will help protect your as sonline business.As terms and conditions are legally enforceable rules, they allow you to set standards for how users interact with your site. Here are some of the major abenefits of including terms and conditions on your ecommerce site:</p>\r\n<p>It has survived not only five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the obb1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop.</p>\r\n<p><strong>Features :</strong></p>\r\n<ul>\r\n<li>Slim body with metal cover</li>\r\n<li>Latest Intel Core i5-1135G7 processor (4 cores / 8 threads)</li>\r\n<li>8GB DDR4 RAM and fast 512GB PCIe SSD</li>\r\n<li>NVIDIA GeForce MX350 2GB GDDR5 graphics card backlit keyboard, touchpad with gesture support</li>\r\n</ul>\r\n<p><strong>3. Protect Your Property</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriezcs but also the on leap into as eylectronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraszvxet sheets containing Lorem Ipsum our spassages, andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book. five centuries but also a the on leap into electronic typesetting, remaining essentially unchanged. It aswasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop our aspublishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p><strong>4. What to Include in Terms and Conditions for Online Stores</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five as centuries but also the on leap into as electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of as Leitraset sheets containing Loriem Ipsum passages, our andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p>Five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing software like Aldus PageMaker our as including versions of Loriem Ipsum to make a type specimen book. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets as containing Lorem Ipsum passages, andei more recently with a desktop publishing software like Aldus PageMaker including versions of Loremas&nbsp; Ipsum to make a type specimen book.</p>\r\n<p><strong>05.Pricing and Payment Terms</strong></p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five as centuries but also as the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release as of Letraset sheets containing Lorem Ipsum our spassages, andei more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>\r\n<p>Five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing software like Aldus PageMaker our as including versions of Lorem aIpsum to make a type specimen book. It wasn&rsquo;t popularised in the 1960s with the release of Letraset sheetsasd containing Lorem Ipsum passages, andei more recentlysl with desktop publishing software like Aldus PageMaker including versions of Loremadfsfds Ipsum to make a type specimen book.</p>\r\n<p>It has survived not only five centuries but also the on leap into electronic typesetting, remaining essentially unchanged. It wasn&rsquo;t popularised in the our 1960s with the release of Letraset sheets containing Lorem Ipsum passages, andei more recently with desktop publishing asou software like Aldus PageMaker including versions of Lorem Ipsum to make a type specimen book.</p>', 1, '2022-09-29 06:17:38', '2023-01-18 09:20:42');

-- Volcando datos para la tabla system_service.custom_paginations: ~5 rows (aproximadamente)
INSERT INTO `custom_paginations` (`id`, `page_name`, `qty`, `created_at`, `updated_at`) VALUES
	(1, 'Blog Page', 6, NULL, '2022-02-07 08:39:56'),
	(2, 'Service Page', 9, NULL, '2022-10-03 10:18:39'),
	(3, 'Provider Page', 8, NULL, '2022-02-07 02:14:01'),
	(4, 'Blog Comment', 4, NULL, '2022-09-15 03:06:58'),
	(5, 'Provider Review', 2, NULL, '2022-09-15 05:01:34');

-- Volcando datos para la tabla system_service.decoration: ~5 rows (aproximadamente)

-- Volcando datos para la tabla system_service.details_construction: ~3 rows (aproximadamente)
INSERT INTO `details_construction` (`id`, `imagen`, `details`, `price`, `status`, `construction_id`, `created_at`, `updated_at`, `name`) VALUES
	(1, 'uploads/custom-images/Detail-Constructora-2023-06-24-03-10-24-5157.jpg', 'CONSTRUCCION DE CASA CON 3 DOMITORIOS UNA SALA', 50, 1, 1, '2023-06-20 03:03:41', '2023-06-29 12:31:08', 'CONSTRUCTORA'),
	(5, 'uploads/custom-images/Detail-Constructora-2023-06-24-03-12-59-8412.jpg', 'CASA CON CREDITO DIRECTO', 49900, 1, 1, '2023-06-24 07:13:01', '2023-06-24 07:13:01', 'CASA DE 3 DORMITORIOS'),
	(6, 'uploads/custom-images/Detail-Constructora-2023-06-24-03-13-03-4611.jpg', 'CASA CAMPING', 49900, 1, 1, '2023-06-24 07:13:03', '2023-06-24 07:13:03', 'CASA CON DOS SALAS');

-- Volcando datos para la tabla system_service.details_property: ~24 rows (aproximadamente)

-- Volcando datos para la tabla system_service.detail_decoration: ~6 rows (aproximadamente)
INSERT INTO `detail_decoration` (`id`, `imagen`, `status`, `decoration_id`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/custom-images/Detail-decoration-2023-07-18-05-06-51-7847.jpg', 1, 1, 'detalles de la decoracion  para el registro de la misma', '2023-07-18 09:06:56', '2023-07-18 09:06:56'),
	(2, 'uploads/custom-images/Detail-decoration-2023-07-18-05-40-26-4865.png', 1, 1, 'pruebas de eedicion  de registro para las vistas', '2023-07-18 09:40:27', '2023-07-18 09:40:27'),
	(3, 'uploads/custom-images/Detail-decoration-2023-07-18-07-50-56-4197.png', 1, 2, 'detalles de registro de decoracion y amoblado', '2023-07-18 23:50:57', '2023-07-18 23:50:57'),
	(4, 'uploads/custom-images/Detail-decoration-2023-07-18-07-52-31-4999.png', 1, 3, 'detalles de la decoracion y amoblado', '2023-07-18 23:52:31', '2023-07-18 23:52:31'),
	(5, 'uploads/custom-images/Detail-decoration-2023-07-19-02-42-40-7440.png', 1, 4, 'detalles de decoracion y amoblado', '2023-07-19 06:42:40', '2023-07-19 06:42:40'),
	(6, 'uploads/custom-images/Detail-decoration-2023-07-19-04-53-25-2465.png', 1, 5, 'detalles de la decoracion ingresada', '2023-07-19 08:53:25', '2023-07-19 08:53:25');

-- Volcando datos para la tabla system_service.detail_material: ~18 rows (aproximadamente)
INSERT INTO `detail_material` (`id`, `name`, `code`, `material_id`, `created_at`, `updated_at`, `price`, `amount`, `image`) VALUES
	(1, 'Fierrro galvanizado', 'ASTM', 1, '2023-06-15 02:26:38', '2023-06-26 12:44:25', 30, 20, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-4991.png'),
	(2, 'Medidas de 1 1/2 3/4', 'ASTM', 1, '2023-06-15 02:26:38', '2023-06-26 12:44:26', 120, 30, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-9356.png'),
	(3, 'Fierro brasileño', 'ASTM', 1, '2023-06-15 06:23:21', '2023-06-26 12:44:26', 15, 10, 'uploads/custom-images/Detail-material-2023-06-26-08-44-26-2851.png'),
	(4, 'calaminas simples', 'ASTM', 2, '2023-06-15 07:13:39', '2023-06-26 12:59:17', 1, 120, 'uploads/custom-images/Detail-material-2023-06-26-08-59-16-5510.png'),
	(5, 'calaminas dobles', 'ASTM', 2, '2023-06-15 07:13:39', '2023-06-26 12:59:17', 35, 60, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-7802.png'),
	(6, 'hoja entera', 'ASTM', 2, '2023-06-15 07:13:39', '2023-06-26 12:59:17', 60, 20, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-9984.png'),
	(7, 'media hoja', 'ASTM', 2, '2023-06-15 07:13:39', '2023-06-26 12:59:17', 30, 40, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-2458.png'),
	(8, 'vigas de fierros en medida de 2 metros', 'ASTM', 3, '2023-06-15 07:18:20', '2023-06-15 07:18:20', 20, 63, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-2458.png'),
	(9, 'vigas trenzados', 'ASTM', 3, '2023-06-15 07:18:21', '2023-06-15 07:18:21', 20, 78, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-2458.png'),
	(10, 'vigas para techos', 'ASTM', 3, '2023-06-15 07:18:21', '2023-06-15 07:18:21', 34, 63, 'uploads/custom-images/Detail-material-2023-06-26-08-59-17-2458.png'),
	(11, 'Fierrros para construccion', 'ASTM', 4, '2023-06-15 07:20:01', '2023-06-15 07:20:01', 50, 50, 'uploads/custom-images/Detail-material-2023-06-26-08-59-16-5510.png'),
	(12, 'medidas de 30, 60 cm', 'ASTM', 4, '2023-06-15 07:20:01', '2023-06-15 07:20:01', 45, 50, 'uploads/custom-images/Detail-material-2023-06-26-08-59-16-5510.png'),
	(13, 'Fierros galvanizados', 'ASTM', 5, '2023-06-15 07:25:26', '2023-06-15 07:25:26', 45, 5, 'uploads/custom-images/Detail-material-2023-06-26-08-59-16-5510.png'),
	(14, 'Fierrros de media 1, 2', 'ASTM', 5, '2023-06-15 07:25:26', '2023-06-15 07:25:26', 78, 45, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-9356.png'),
	(15, 'vigas para puertas', 'ASTM', 6, '2023-06-15 07:28:48', '2023-06-15 07:28:48', 45, 87, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-9356.png'),
	(16, 'vigas en t', 'ASTM', 6, '2023-06-15 07:28:48', '2023-06-15 07:28:48', 42, 45, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-9356.png'),
	(17, 'Fierros navales', 'ASTM', 7, '2023-06-15 12:47:40', '2023-06-15 12:47:40', 45, 52, 'uploads/custom-images/Detail-material-2023-06-26-08-44-25-9356.png'),
	(18, 'estribos de 30 x 30', 'ASTM A 496', 9, '2023-06-26 12:20:14', '2023-06-26 12:33:40', 50, 120, 'uploads/custom-images/Detail-material-2023-06-26-08-20-14-3819.png');

-- Volcando datos para la tabla system_service.detail_terrain: ~3 rows (aproximadamente)
INSERT INTO `detail_terrain` (`id`, `image`, `description`, `terrain_id`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/custom-images/Detail-Terrain-2023-06-22-08-12-33-7218.png', 'TERRENO EN VENTA EN CONDOMINIO RIO SIERRA', 1, '2023-06-22 12:13:17', '2023-06-22 12:13:17'),
	(2, 'uploads/custom-images/Detail-Terrain-2023-06-22-09-26-34-6284.png', 'BUEN TERRENO', 1, '2023-06-22 13:26:35', '2023-06-22 13:26:35'),
	(3, 'uploads/custom-images/Detail-Terrain-2023-06-23-05-00-30-7480.png', 'cerca de una urbanizacion', 2, '2023-06-23 09:00:31', '2023-06-23 09:00:31');

-- Volcando datos para la tabla system_service.email_configurations: ~0 rows (aproximadamente)
INSERT INTO `email_configurations` (`id`, `mail_type`, `mail_host`, `mail_port`, `email`, `email_password`, `smtp_username`, `smtp_password`, `mail_encryption`, `created_at`, `updated_at`) VALUES
	(1, 2, 'sandbox.smtp.mailtrap.io', '2525', '201601ramiro@gmail.com', 'mary+pass@', 'acc536ec817be8', '7c99a39d65495a', 'tls', NULL, '2023-05-27 02:32:32');

-- Volcando datos para la tabla system_service.email_templates: ~10 rows (aproximadamente)
INSERT INTO `email_templates` (`id`, `name`, `subject`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Password Reset', 'Password Reset', '<h4>Dear <b>{{name}}</b>,</h4>\r\n    <p>Do you want to reset your password? Please Click the following link and Reset Your Password.</p>', NULL, '2021-12-09 10:06:57'),
	(2, 'Contact Email', 'Contact Email', '<p>Name: <b>{{name}}</b></p><p>\r\n\r\nEmail: <b>{{email}}</b></p><p>\r\n\r\nPhone: <b>{{phone}}</b></p><p><span style="background-color: transparent;">Subject: <b>{{subject}}</b></span></p><p>\r\n\r\nMessage: <b>{{message}}</b></p>', NULL, '2021-12-10 23:44:34'),
	(3, 'Subscribe Notification', 'Subscribe Notification', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es"><span style="font-size: 14pt;"><strong>Hola,</strong></span>\r\n\r\n&iexcl;Felicidades! Su suscripci&oacute;n ha sido creada con &eacute;xito. Haga clic en el siguiente enlace y verifique su suscripci&oacute;n. Si no aprueba este enlace, despu&eacute;s de 24 horas su suscripci&oacute;n ser&aacute; denegada<br><br>REDAFOR </span></pre>', NULL, '2023-05-27 02:36:05'),
	(4, 'User Verification', 'User Verification', '<p>Dear <b>{{user_name}}</b>,\r\n</p><p>Congratulations! Your Account has been created successfully. Please Click the following link and Active your Account.</p>', NULL, '2021-12-10 23:45:25'),
	(5, 'Provider Withdraw', 'Provider Withdraw Approval', '<p>Hi <b>{{provider_name}}</b>,</p><p>Your withdraw Request Approval successfully. Please check your account.</p><p>Withdraw Details:</p><p>Withdraw method : <b>{{withdraw_method}}</b>,</p><p>Total Amount :<b> {{total_amount}}</b>,</p><p>Withdraw charge : <b>{{withdraw_charge}}</b>,</p><p>Withdraw&nbsp; Amount : <b>{{withdraw_amount}}</b>,</p><p>Approval Date :<b> {{approval_date}}</b></p>', NULL, '2022-08-30 03:24:53'),
	(6, 'Order Successfully', 'Order Successfully', '<p>Hi {{user_name}},</p><p> \r\nThanks for your new order. Your order id has been submited .</p><p>Total Amount : {{total_amount}},</p><p>Payment Method : {{payment_method}},</p><p>Payment Status : {{payment_status}},</p><p>Order Status : {{order_status}},</p><p>Order Date: {{order_date}},</p><p>Order Detail: {{order_detail}}</p>', NULL, '2022-01-10 21:37:03'),
	(8, 'New Order Mail to Client', 'New Order Mail to Client', '<p>Hi&nbsp;{{name}}, Thanks for your new order. Your order has been placed.</p><p>Order Id : {{order_id}}</p><p>Amount : {{amount}}</p><p>Schedule Date : {{schedule_date}}</p>', NULL, '2022-09-24 10:15:23'),
	(9, 'New Order Mail to Provider', 'New Order Mail to Provider', '<p>Hi&nbsp;{{name}}, A new order has been placed to you .</p><p>Order Id : {{order_id}}</p><p>Amount : {{amount}}</p><p>Schedule Date : {{schedule_date}}</p>', NULL, '2022-09-24 10:16:22'),
	(10, 'User Verification For OTP', 'User Verification', '<p>Dear <b>{{user_name}}</b>,\r\n</p><p>Congratulations! Your Account has been created successfully. Please Copy the code and verify your account</p>', NULL, '2021-12-10 23:45:25'),
	(11, 'Password Reset For OTP', 'Password Reset', '<h4>Dear <b>{{name}}</b>,</h4>\r\n    <p>Do you want to reset your password? Please copy and past the code</p>', NULL, '2021-12-09 10:06:57');

-- Volcando datos para la tabla system_service.error_pages: ~3 rows (aproximadamente)
INSERT INTO `error_pages` (`id`, `page_name`, `page_number`, `header`, `description`, `button_text`, `created_at`, `updated_at`) VALUES
	(1, '404 Error', '404', 'Estamos trabajando para brindarle una mejor experiencia', 'Los sentimos pero pronto solucionaremos los problemas suscitados.', 'Volver a Inicio', NULL, '2023-05-31 09:27:47'),
	(2, '500 Error', '500', 'Estamos trabajando para brindarle una mejor experiencia', 'Estamos trabajando para brindarle una mejor experiencia REDAFOR', 'Volver a Inicio', NULL, '2023-05-31 09:28:39'),
	(3, '505 Error', '505', 'Estamos trabajando para brindarle una mejor experiencia', 'Estamos trabajando para brindarle una mejor experiencia', 'Volver a Inicio', NULL, '2023-05-31 09:29:14');

-- Volcando datos para la tabla system_service.facebook_comments: ~0 rows (aproximadamente)
INSERT INTO `facebook_comments` (`id`, `app_id`, `comment_type`, `created_at`, `updated_at`) VALUES
	(1, '882238482112522', 1, NULL, '2022-10-08 07:22:03');

-- Volcando datos para la tabla system_service.facebook_pixels: ~0 rows (aproximadamente)
INSERT INTO `facebook_pixels` (`id`, `status`, `app_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '972911606915059', NULL, '2021-12-13 22:38:44');

-- Volcando datos para la tabla system_service.failed_jobs: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.faqs: ~8 rows (aproximadamente)
INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Que hace REDAFOR ?', '<p>Proporciona una plataforma donde todas las personas pueden ofertar sus bienes inmuebles sus servicios, construcciones materiales entre otros y las mismas pueden ir adquiriendo segun sus necesidades.</p>', 1, '2022-09-29 06:04:11', '2023-05-31 08:57:36'),
	(2, '¿Tiene un programa de afiliados?', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es">Actualmente estamos trabajando en la configuraci&oacute;n de nuestro programa de afiliados. Pr&oacute;ximamente se dar&aacute; a conocer al p&uacute;blico.</span></pre>', 1, '2022-09-29 06:05:19', '2023-05-31 08:58:30'),
	(3, '¿Que beneficios me da REDAFOR ?', '<p>los beneficios del nuestro sistema es que tu puedes publicar muchos&nbsp; inmuebles y servicios entre otros que cuenta el sistema y tener ventas mas raidas dentro de nuestro sistema.</p>', 1, '2022-09-29 06:05:41', '2023-05-31 09:01:38'),
	(4, '¿Ofrecen servicio de personalización?', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es">S&iacute;, brindamos el servicio de personalizaci&oacute;n por una peque&ntilde;a tarifa. Pero depende Si estamos ocupados con proyectos, no aceptamos pedidos personalizados.</span></pre>', 1, '2022-09-29 06:06:01', '2023-05-31 09:04:33'),
	(5, '¿Puedo probar su producto antes de comprarlo?', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es">Actualmente no ofrecemos este servicio, pero pronto iniciaremos este servicio.</span></pre>', 1, '2022-09-29 06:06:23', '2023-05-31 09:07:06'),
	(6, '¿Qué ayudamos?', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es">REDAFOR es un sitio web donde las personas pueden ofertar sus inmuebles terrenos pueden ofertar sus servicios para encontrar trabajo de manera mas rapida y ofrecer los servicios entre otros donde podras gestionar todo el proceso.</span></pre>', 1, '2022-09-29 06:06:53', '2023-05-31 09:16:46'),
	(7, '¿Puedo hacer uso del soporte de mantenimiento para mis clientes?', '<p>Si, REDAFOR te brinda una amplia gana de soporte para tu quejas o pedidos de las mismas garantizando que el inmuebles servicio o contruccion llege a realizarse de manera exitosa&nbsp;</p>', 1, '2022-09-29 06:07:15', '2023-05-31 09:20:23'),
	(8, '¿Cuál es el detalle de la política de reembolso?', '<pre id="tw-target-text" class="tw-data-text tw-text-large tw-ta" dir="ltr" data-placeholder="Traducci&oacute;n"><span class="Y2IQFc" lang="es">Debido a que est&aacute; utilizando el mejor producto y servicio digital, la mayor&iacute;a de las veces, los reembolsos no ser&aacute;n necesarios y porque no se otorgar&aacute;n devoluciones de art&iacute;culos digitales a menos que el producto que compr&oacute; probablemente no sea necesario y envi&oacute; una solicitud de soporte pero no obtuvo respuesta. dentro de un d&iacute;a h&aacute;bil, y la afirmaci&oacute;n principal del producto era completamente falsa. Para obtener informaci&oacute;n adicional, consulte nuestra Pol&iacute;tica de reembolso.</span></pre>\r\n<p>.</p>', 1, '2022-09-29 06:08:08', '2023-05-31 09:23:31');

-- Volcando datos para la tabla system_service.flutterwaves: ~0 rows (aproximadamente)
INSERT INTO `flutterwaves` (`id`, `public_key`, `secret_key`, `currency_rate`, `country_code`, `currency_code`, `title`, `logo`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'FLWPUBK_TEST-5760e3ff9888aa1ab5e5cd1ec3f99cb1-X', 'FLWSECK_TEST-81cb5da016d0a51f7329d4a8057e766d-X', 460.49, 'NG', 'NGN', 'Ecommerce', 'uploads/website-images/flutterwave-2022-09-25-09-48-17-9566.jpg', 0, NULL, '2023-06-06 10:05:24');

-- Volcando datos para la tabla system_service.footers: ~0 rows (aproximadamente)
INSERT INTO `footers` (`id`, `phone`, `email`, `address`, `about_us`, `first_column`, `second_column`, `third_column`, `copyright`, `payment_image`, `created_at`, `updated_at`) VALUES
	(1, '+591 75020158', 'info@redafor.net', 'Av. El Tronpillo  #1100  local 8', 'En REDAFOR, nos enorgullece ser tu compañero confiable en el mundo de los inmuebles, construcciones, amoblados  y los servicios. Somos una plataforma dinámica que conecta a propietarios, compradores, arrendatarios y proveedores de servicios, ofreciendo una experiencia completa y personalizada para satisfacer todas tus necesidades.', 'Important Link', 'Quick Link', 'Our Service', '© Redafor - Todos los derechos reservados. 2023', 'uploads/website-images/payment-card-2022-08-28-04-29-12-1387.png', NULL, '2023-07-23 09:53:40');

-- Volcando datos para la tabla system_service.footer_links: ~10 rows (aproximadamente)
INSERT INTO `footer_links` (`id`, `column`, `link`, `title`, `created_at`, `updated_at`) VALUES
	(1, '1', 'contact-us', 'Contact Us', '2022-09-29 07:16:42', '2022-09-29 07:16:42'),
	(2, '1', 'blogs', 'Our Blog', '2022-09-29 07:17:20', '2022-09-29 07:17:20'),
	(3, '1', 'faq', 'FAQ', '2022-09-29 07:18:28', '2022-09-29 07:18:28'),
	(4, '1', 'terms-and-conditions', 'Terms and Conditions', '2022-09-29 07:18:45', '2022-09-29 07:18:45'),
	(5, '1', 'privacy-policy', 'Privacy Policy', '2022-09-29 07:19:13', '2022-09-29 07:19:13'),
	(6, '2', 'services', 'Our Services', '2022-09-29 07:20:17', '2022-09-29 07:20:17'),
	(7, '2', 'about-us', 'Why Choose Us', '2022-09-29 07:20:35', '2022-09-29 07:20:35'),
	(8, '2', 'dashboard', 'My Profile', '2022-09-29 07:21:12', '2022-09-29 07:21:12'),
	(9, '2', 'about-us', 'About Us', '2022-09-29 07:21:39', '2022-09-29 07:21:39'),
	(10, '2', 'join-as-a-provider', 'Join as a Provider', '2022-09-29 07:22:37', '2022-09-29 07:22:37');

-- Volcando datos para la tabla system_service.footer_social_links: ~4 rows (aproximadamente)
INSERT INTO `footer_social_links` (`id`, `link`, `icon`, `created_at`, `updated_at`) VALUES
	(1, 'https://www.facebook.com/', 'fa fa-facebook', '2022-09-29 07:14:50', '2023-01-15 09:22:49'),
	(2, 'https://www.twitter.com/', 'fab fa-twitter', '2022-09-29 07:15:06', '2022-09-29 07:15:06'),
	(3, 'https://www.instagram.com/', 'fab fa-instagram', '2022-09-29 07:15:27', '2022-09-29 07:15:27'),
	(4, 'https://www.linkedin.com/', 'fa fa-linkedin', '2022-09-29 07:15:44', '2023-01-15 09:23:05');

-- Volcando datos para la tabla system_service.form: ~7 rows (aproximadamente)
INSERT INTO `form` (`id`, `name`, `phone`, `email`, `message`, `created_at`, `updated_at`, `property_id`, `status`, `aproved_admin`, `users_id`, `client_id`) VALUES
	(7, 'Banco Economico', 46545646, '201601ramiro@gmail.com', 'test', '2023-05-27 01:31:54', '2023-05-27 01:31:54', 8, 1, 1, 2, 10),
	(8, 'Marca y Mercado', 46545646, '201601ramiro@gmail.com', 'mensaje de bienvenida', '2023-05-27 01:38:05', '2023-05-27 01:38:05', 8, 1, 1, 16, 10),
	(9, 'jhuliza', 67470820, '201601ramiro@gmail.com', 'segunda prueba de teste de envio de mensajes a wassap bussines', '2023-05-27 01:48:36', '2023-05-31 13:33:41', 8, 1, 1, 16, 1),
	(10, 'jhuliza', 67470820, '201601ramiro@gmail.com', 'segunda prueba de test de envio de mensajes a wassap bussiines', '2023-05-27 01:49:56', '2023-07-22 01:12:55', 8, 0, 1, 16, 1),
	(11, 'RAMIRO', 67470820, '201601ramiro@gmail.com', 'fdhdfggnfdgn', '2023-06-06 06:07:41', '2023-06-06 06:07:41', 8, 1, 0, 16, NULL),
	(12, 'ramiro', 67470820, '201601ramiro@gmail.com', 'testeando teste', '2023-06-06 06:10:23', '2023-06-06 06:10:23', 8, 1, 0, 16, NULL),
	(13, 'dania', 65440220, 'owner@email.com', 'estoy asiendo pruebas de testeo', '2023-06-06 06:13:34', '2023-06-06 06:13:34', 8, 1, 0, 16, NULL);

-- Volcando datos para la tabla system_service.gallery_property: ~25 rows (aproximadamente)
INSERT INTO `gallery_property` (`id`, `imagen`, `code`, `property_id`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/custom-images/property-image-details-2023-05-10-07-15-14-1013.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(2, 'uploads/custom-images/property-image-details-2023-05-10-07-15-15-6307.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(3, 'uploads/custom-images/property-image-details-2023-05-10-07-15-15-3223.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(4, 'uploads/custom-images/property-image-details-2023-05-10-07-15-15-4513.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(5, 'uploads/custom-images/property-image-details-2023-05-10-07-15-15-1620.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(6, 'uploads/custom-images/property-image-details-2023-05-10-07-15-15-6512.png', NULL, 8, '2023-05-10 11:15:15', '2023-05-10 11:15:15'),
	(7, 'uploads/custom-images/property-image-details-2023-05-10-07-20-53-5820.png', NULL, 8, '2023-05-10 11:20:53', '2023-05-10 11:20:53'),
	(8, 'uploads/custom-images/property-image-details-2023-05-10-10-03-23-1879.png', NULL, 9, '2023-05-11 02:03:24', '2023-05-11 02:03:24'),
	(9, 'uploads/custom-images/property-image-details-2023-05-10-10-03-24-2765.png', NULL, 9, '2023-05-11 02:03:24', '2023-05-11 02:03:24'),
	(10, 'uploads/custom-images/property-image-details-2023-05-10-10-03-24-1088.png', NULL, 9, '2023-05-11 02:03:24', '2023-05-11 02:03:24'),
	(11, 'uploads/custom-images/property-image-details-2023-05-10-10-03-24-9298.png', NULL, 9, '2023-05-11 02:03:25', '2023-05-11 02:03:25'),
	(12, 'uploads/custom-images/property-image-details-2023-05-10-10-03-25-4471.png', NULL, 9, '2023-05-11 02:03:25', '2023-05-11 02:03:25'),
	(13, 'uploads/custom-images/property-image-details-2023-05-10-10-03-25-6107.png', NULL, 9, '2023-05-11 02:03:25', '2023-05-11 02:03:25'),
	(14, 'uploads/custom-images/property-image-details-2023-05-10-10-27-10-6953.png', NULL, 10, '2023-05-11 02:27:11', '2023-05-11 02:27:11'),
	(15, 'uploads/custom-images/property-image-details-2023-05-10-10-27-11-2941.png', NULL, 10, '2023-05-11 02:27:11', '2023-05-11 02:27:11'),
	(16, 'uploads/custom-images/property-image-details-2023-05-10-10-27-11-6543.png', NULL, 10, '2023-05-11 02:27:11', '2023-05-11 02:27:11'),
	(17, 'uploads/custom-images/property-image-details-2023-05-10-10-27-11-9043.png', NULL, 10, '2023-05-11 02:27:11', '2023-05-11 02:27:11'),
	(18, 'uploads/custom-images/property-image-details-2023-05-10-10-27-12-8671.png', NULL, 10, '2023-05-11 02:27:12', '2023-05-11 02:27:12'),
	(19, 'uploads/custom-images/property-image-details-2023-05-10-10-27-12-3916.png', NULL, 10, '2023-05-11 02:27:12', '2023-05-11 02:27:12'),
	(20, 'uploads/custom-images/property-image-details-2023-05-10-10-36-43-6270.png', NULL, 11, '2023-05-11 02:36:44', '2023-05-11 02:36:44'),
	(21, 'uploads/custom-images/property-image-details-2023-05-10-10-36-44-7725.png', NULL, 11, '2023-05-11 02:36:44', '2023-05-11 02:36:44'),
	(22, 'uploads/custom-images/property-image-details-2023-05-10-10-36-44-4857.png', NULL, 11, '2023-05-11 02:36:45', '2023-05-11 02:36:45'),
	(23, 'uploads/custom-images/property-image-details-2023-05-10-10-36-45-3926.png', NULL, 11, '2023-05-11 02:36:45', '2023-05-11 02:36:45'),
	(24, 'uploads/custom-images/property-image-details-2023-05-10-10-36-45-2316.png', NULL, 11, '2023-05-11 02:36:45', '2023-05-11 02:36:45'),
	(25, 'uploads/custom-images/property-image-details-2023-05-10-10-36-45-3285.png', NULL, 11, '2023-05-11 02:36:45', '2023-05-11 02:36:45');

-- Volcando datos para la tabla system_service.google_analytics: ~0 rows (aproximadamente)
INSERT INTO `google_analytics` (`id`, `analytic_id`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'UA-84213520-6', 1, NULL, '2021-12-10 22:15:51');

-- Volcando datos para la tabla system_service.google_recaptchas: ~0 rows (aproximadamente)
INSERT INTO `google_recaptchas` (`id`, `site_key`, `secret_key`, `status`, `created_at`, `updated_at`) VALUES
	(1, '6LcVO6cbAAAAAOzIEwPlU66nL1rxD4VAS38tjpBX', '6LcVO6cbAAAAALVNrpZfNRfd0Gy_9a_fJRLiMVUI', 0, NULL, '2023-03-13 04:46:41');

-- Volcando datos para la tabla system_service.how_it_works: ~3 rows (aproximadamente)
INSERT INTO `how_it_works` (`id`, `image`, `title`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/custom-images/how-it-work-2022-09-29-12-20-12-1071.png', 'Online Booking', 'If you are going to use a passage of you need to be sure there isn\'t anything emc barrassing hidden in the middle', '2022-09-29 06:20:12', '2022-09-29 06:20:12'),
	(2, 'uploads/custom-images/how-it-work-2022-09-29-12-20-54-8399.png', 'Get Expert Cleaner', 'If you are going to use a passage of you need to be sure there isn\'t anything emc barrassing hidden in the middle', '2022-09-29 06:20:55', '2022-09-29 06:20:55'),
	(3, 'uploads/custom-images/how-it-work-2022-09-29-12-21-46-2428.png', 'Enjoy Services', 'If you are going to use a passage of you need to be sure there isn\'t anything emc barrassing hidden in the middle', '2022-09-29 06:21:46', '2022-09-29 06:21:46');

-- Volcando datos para la tabla system_service.image_material: ~8 rows (aproximadamente)
INSERT INTO `image_material` (`id`, `imagen`, `material_id`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/custom-images/material-image-details-2023-06-14-10-26-37-1838.png', 1, '2023-06-15 02:26:37', '2023-06-15 02:26:37'),
	(3, 'uploads/custom-images/material-image-details-2023-06-15-03-13-38-4598.png', 2, '2023-06-15 07:13:39', '2023-06-15 07:13:39'),
	(4, 'uploads/custom-images/material-image-details-2023-06-15-03-18-20-2262.png', 3, '2023-06-15 07:18:20', '2023-06-15 07:18:20'),
	(5, 'uploads/custom-images/material-image-details-2023-06-15-03-18-20-4053.png', 3, '2023-06-15 07:18:20', '2023-06-15 07:18:20'),
	(6, 'uploads/custom-images/material-image-details-2023-06-15-03-20-00-9791.png', 4, '2023-06-15 07:20:01', '2023-06-15 07:20:01'),
	(7, 'uploads/custom-images/material-image-details-2023-06-15-03-25-25-2999.png', 5, '2023-06-15 07:25:26', '2023-06-15 07:25:26'),
	(8, 'uploads/custom-images/material-image-details-2023-06-15-03-28-47-2694.png', 6, '2023-06-15 07:28:47', '2023-06-15 07:28:47'),
	(9, 'uploads/custom-images/material-image-details-2023-06-15-08-47-40-2178.png', 7, '2023-06-15 12:47:40', '2023-06-15 12:47:40');

-- Volcando datos para la tabla system_service.instamojo_payments: ~0 rows (aproximadamente)
INSERT INTO `instamojo_payments` (`id`, `api_key`, `auth_token`, `currency_rate`, `account_mode`, `status`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'test_5f4a2c9a58ef216f8a1a688910f', 'test_994252ada69ce7b3d282b9941c2', '81.98', 'Sandbox', 0, 'uploads/website-images/instamojo-2022-09-25-10-05-31-7719.png', NULL, '2023-06-06 10:06:06');

-- Volcando datos para la tabla system_service.issuance: ~0 rows (aproximadamente)
INSERT INTO `issuance` (`id`, `fee`, `ids`, `full_name`, `email`, `phone`, `address`, `latitude`, `longitude`, `note`, `created_at`, `updated_at`, `status`) VALUES
	(90, 7, '4', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.781609730357413', '-63.16613112158203', 'probando pagos en linea', '2023-08-11 08:26:00', '2023-08-11 08:26:00', 0);

-- Volcando datos para la tabla system_service.issuance_decoration: ~18 rows (aproximadamente)
INSERT INTO `issuance_decoration` (`id`, `fee`, `ids`, `full_name`, `email`, `phone`, `address`, `latitude`, `longitude`, `note`, `created_at`, `updated_at`, `status`) VALUES
	(50, 0, '1,2', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:15:59', '2023-07-20 02:15:59', 0),
	(51, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:17:01', '2023-07-20 02:17:01', 0),
	(52, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:17:46', '2023-07-20 02:17:46', 0),
	(53, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:18:20', '2023-07-20 02:18:20', 0),
	(54, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:27:38', '2023-07-20 02:27:38', 0),
	(55, 20, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Equipetrol, Santa Cruz', '-17.78594138579195', '-63.15866385168457', 'tester', '2023-07-20 02:28:06', '2023-07-20 02:28:06', 0),
	(56, 30, '2', 'dania rivera', 'owner@email.com', 65440220, 'Av. El Tronpillo  #1100  local 8', '-17.78030204015188', '-63.16321287817383', 'quiero este sofa para mi patio', '2023-07-22 06:37:46', '2023-07-22 06:37:46', 0),
	(57, 10, '2', 'dania rivera', 'owner@email.com', 65440220, 'Av. El Tronpillo  #1100  local 8', '-17.78030204015188', '-63.16321287817383', 'quiero este sofa para mi patio', '2023-07-22 06:49:08', '2023-07-22 06:49:08', 0),
	(58, 0, '2', 'dania rivera', 'owner@email.com', 65440220, 'Av. El Tronpillo  #1100  local 8', '-17.78030204015188', '-63.16321287817383', 'quiero este sofa para mi patio', '2023-07-23 00:58:52', '2023-07-23 00:58:52', 0),
	(59, 1, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 08:33:46', '2023-08-11 08:33:46', 0),
	(60, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:43:26', '2023-08-11 09:43:26', 0),
	(61, 1, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:45:19', '2023-08-11 09:45:19', 0),
	(62, 1, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:46:34', '2023-08-11 09:46:34', 0),
	(63, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:49:09', '2023-08-11 09:49:09', 0),
	(64, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:49:46', '2023-08-11 09:49:46', 0),
	(65, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:55:20', '2023-08-11 09:55:20', 0),
	(66, 0, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77433558214405', '-63.190592867797854', 'pruebas de testing de pago', '2023-08-11 09:58:45', '2023-08-11 09:58:45', 0),
	(67, 1, '1', 'jhuliza delgadillo moscoso', '201601ramiro@gmail.com', 67470820, 'Av. El Tronpillo  #1100  local 8', '-17.77351824361655', '-63.18621550268555', 'haciendo pruebas de test', '2023-08-16 10:47:28', '2023-08-16 10:47:28', 0);

-- Volcando datos para la tabla system_service.maintainance_texts: ~0 rows (aproximadamente)
INSERT INTO `maintainance_texts` (`id`, `status`, `image`, `description`, `created_at`, `updated_at`) VALUES
	(1, 0, 'uploads/website-images/maintainance-mode-2022-08-31-09-12-11-5142.png', 'Estamos actualizando nuestro sitio. Volveremos pronto.\r\nPor favor, quédate con nosotros.\r\nGracias.', NULL, '2023-07-21 00:43:21');

-- Volcando datos para la tabla system_service.material: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.menu_visibilities: ~10 rows (aproximadamente)
INSERT INTO `menu_visibilities` (`id`, `menu_name`, `custom_name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Home', 'Inicio', 1, NULL, '2023-04-29 06:29:31'),
	(2, 'About Us', 'Quiénes Somos', 1, NULL, '2023-04-29 06:29:31'),
	(3, 'Service', 'Service', 0, NULL, '2023-04-30 01:17:53'),
	(4, 'Pages', 'Pages', 0, NULL, '2023-04-30 01:17:53'),
	(5, 'FAQ', 'FAQ', 1, NULL, '2022-10-10 02:10:38'),
	(6, 'Terms and Conditions', 'Terms and Conditions', 1, NULL, '2022-10-10 02:10:38'),
	(7, 'Privacy Policy', 'Privacy Policy', 1, NULL, '2022-10-10 02:10:38'),
	(8, 'Custom Page', 'Custom Page', 1, NULL, '2022-10-08 10:38:09'),
	(9, 'Blog', 'Blog', 1, NULL, '2022-10-10 02:10:38'),
	(10, 'Contact Us', 'Contacto', 1, NULL, '2023-04-29 06:29:31');

-- Volcando datos para la tabla system_service.messages: ~102 rows (aproximadamente)
INSERT INTO `messages` (`id`, `buyer_id`, `provider_id`, `message`, `buyer_read_msg`, `provider_read_msg`, `send_by`, `service_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 'This is test message', 1, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(2, 1, 2, 'This is test message two', 1, 1, 'provider', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(3, 1, 2, 'This is test message three', 1, 1, 'provider', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(4, 1, 2, 'This is test message four', 1, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(5, 1, 2, 'hello developer', 1, 1, 'provider', 0, '2023-03-09 11:28:02', '2023-07-21 09:28:29'),
	(6, 1, 2, 'hello developer', 1, 1, 'provider', 0, '2023-03-09 11:29:11', '2023-07-21 09:28:29'),
	(7, 1, 2, 'are you there ?', 1, 1, 'provider', 0, '2023-03-09 11:29:56', '2023-07-21 09:28:29'),
	(8, 1, 2, 'This is test message', 1, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(9, 1, 2, 'This is test message four', 1, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(10, 1, 2, 'how can I help you ?', 1, 1, 'provider', 0, '2023-03-09 11:30:47', '2023-07-21 09:28:29'),
	(11, 1, 2, 'please tell me', 1, 1, 'provider', 0, '2023-03-09 11:32:45', '2023-07-21 09:28:29'),
	(12, 10, 2, 'This is test message', 10, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-03-17 05:19:11'),
	(13, 10, 2, 'This is test message two', 0, 1, 'provider', 0, '2023-03-09 10:11:50', '2023-03-17 05:19:11'),
	(14, 1, 2, 'This is test message four', 1, 1, 'buyer', 0, '2023-03-09 10:11:50', '2023-07-21 09:28:29'),
	(15, 10, 2, 'are  you there ?', 0, 1, 'provider', 0, '2023-03-09 11:38:30', '2023-03-17 05:19:11'),
	(16, 10, 2, 'Hello ki obostha ?', 0, 1, 'provider', 0, '2023-03-13 02:53:41', '2023-03-17 05:19:11'),
	(17, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:55:47', '2023-07-21 09:28:29'),
	(18, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:56:41', '2023-07-21 09:28:29'),
	(19, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:56:55', '2023-07-21 09:28:29'),
	(20, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:57:24', '2023-07-21 09:28:29'),
	(21, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:58:25', '2023-07-21 09:28:29'),
	(22, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:58:40', '2023-07-21 09:28:29'),
	(23, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 08:58:46', '2023-07-21 09:28:29'),
	(24, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:01:58', '2023-07-21 09:28:29'),
	(25, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:02:26', '2023-07-21 09:28:29'),
	(26, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:05:28', '2023-07-21 09:28:29'),
	(27, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:13:34', '2023-07-21 09:28:29'),
	(28, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:14:38', '2023-07-21 09:28:29'),
	(29, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:14:58', '2023-07-21 09:28:29'),
	(30, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:15:33', '2023-07-21 09:28:29'),
	(31, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:16:53', '2023-07-21 09:28:29'),
	(32, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:18:01', '2023-07-21 09:28:29'),
	(33, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:18:09', '2023-07-21 09:28:29'),
	(34, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:18:39', '2023-07-21 09:28:29'),
	(35, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:18:48', '2023-07-21 09:28:29'),
	(36, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:19:11', '2023-07-21 09:28:29'),
	(37, 1, 2, 'hello', 1, 1, 'provider', 0, '2023-03-13 09:20:19', '2023-07-21 09:28:29'),
	(38, 1, 2, 'ki obostha ?', 1, 1, 'provider', 0, '2023-03-13 09:20:23', '2023-07-21 09:28:29'),
	(39, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:20:29', '2023-07-21 09:28:29'),
	(40, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:24:27', '2023-07-21 09:28:29'),
	(41, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:26:02', '2023-07-21 09:28:29'),
	(42, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:27:13', '2023-07-21 09:28:29'),
	(43, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:27:18', '2023-07-21 09:28:29'),
	(44, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:31:45', '2023-07-21 09:28:29'),
	(45, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:38:21', '2023-07-21 09:28:29'),
	(46, 1, 2, 'this is test message', 1, 1, 'buyer', 0, '2023-03-13 09:44:42', '2023-07-21 09:28:29'),
	(49, 6, 2, 'this is test message', 1, 0, 'buyer', 0, '2023-03-13 10:10:45', '2023-03-16 10:45:04'),
	(50, 6, 2, 'this is test message', 1, 0, 'buyer', 0, '2023-03-13 10:10:58', '2023-03-16 10:45:04'),
	(51, 6, 2, 'ki obostha', 0, 0, 'provider', 0, '2023-03-13 10:11:12', '2023-03-16 10:45:04'),
	(52, 6, 2, 'this is test message', 1, 0, 'buyer', 0, '2023-03-13 10:13:09', '2023-03-16 10:45:04'),
	(53, 1, 2, 'ki re hudai ajaira msg send koros kn', 1, 1, 'provider', 0, '2023-03-13 10:17:09', '2023-07-21 09:28:29'),
	(54, 1, 2, 'This is test message', 1, 1, 'buyer', 0, '2023-03-16 06:54:07', '2023-07-21 09:28:29'),
	(55, 1, 2, 'Nice work', 1, 1, 'buyer', 0, '2023-03-16 06:56:39', '2023-07-21 09:28:29'),
	(56, 1, 2, 'ki obostha', 1, 1, 'buyer', 0, '2023-03-16 07:03:27', '2023-07-21 09:28:29'),
	(57, 1, 2, 'any update ?', 1, 1, 'buyer', 0, '2023-03-16 07:04:10', '2023-07-21 09:28:29'),
	(58, 1, 2, 'hello', 1, 1, 'buyer', 0, '2023-03-16 07:06:06', '2023-07-21 09:28:29'),
	(59, 1, 2, 'hi', 1, 1, 'provider', 0, '2023-03-16 07:10:33', '2023-07-21 09:28:29'),
	(60, 1, 2, 'hellow', 1, 1, 'buyer', 0, '2023-03-16 07:19:52', '2023-07-21 09:28:29'),
	(61, 1, 2, 'ki re ?', 1, 1, 'buyer', 0, '2023-03-16 07:20:37', '2023-07-21 09:28:29'),
	(62, 1, 2, 'ki obostha re ?', 1, 1, 'provider', 0, '2023-03-16 07:38:01', '2023-07-21 09:28:29'),
	(63, 1, 2, 'hellow', 1, 1, 'provider', 0, '2023-03-16 07:39:17', '2023-07-21 09:28:29'),
	(64, 1, 2, 'test essage', 1, 1, 'provider', 0, '2023-03-16 07:44:05', '2023-07-21 09:28:29'),
	(65, 1, 2, 'kemon achis re ?', 1, 1, 'buyer', 0, '2023-03-16 07:44:21', '2023-07-21 09:28:29'),
	(66, 1, 2, 'valoi achi', 1, 1, 'provider', 0, '2023-03-16 07:44:32', '2023-07-21 09:28:29'),
	(68, 1, 2, 'hei', 1, 1, 'provider', 0, '2023-03-16 07:54:31', '2023-07-21 09:28:29'),
	(69, 1, 2, 'ki obostha ?', 1, 1, 'provider', 0, '2023-03-16 07:55:02', '2023-07-21 09:28:29'),
	(70, 1, 2, 'ki obostha ?', 1, 1, 'provider', 0, '2023-03-16 10:46:09', '2023-07-21 09:28:29'),
	(71, 1, 2, 'eito obostha', 1, 1, 'buyer', 0, '2023-03-16 10:46:47', '2023-07-21 09:28:29'),
	(72, 1, 2, 'koi achis ?', 1, 1, 'buyer', 0, '2023-03-16 10:47:06', '2023-07-21 09:28:29'),
	(73, 1, 2, 'hellow', 1, 1, 'buyer', 0, '2023-03-16 10:49:09', '2023-07-21 09:28:29'),
	(74, 1, 2, 'kooi ?', 1, 1, 'buyer', 0, '2023-03-16 10:49:21', '2023-07-21 09:28:29'),
	(75, 1, 2, '4521', 1, 1, 'provider', 0, '2023-03-16 10:57:05', '2023-07-21 09:28:29'),
	(80, 1, 4, NULL, 1, 1, 'buyer', 21, '2023-03-16 12:06:40', '2023-05-23 12:42:51'),
	(81, 1, 4, 'I need help about it ?', 1, 1, 'buyer', 0, '2023-03-16 12:07:01', '2023-05-23 12:42:51'),
	(82, 1, 4, 'kk', 1, 1, 'buyer', 0, '2023-03-16 12:07:34', '2023-05-23 12:42:51'),
	(83, 1, 2, 'ol', 1, 1, 'buyer', 0, '2023-03-16 12:07:56', '2023-07-21 09:28:29'),
	(84, 1, 4, NULL, 1, 1, 'buyer', 21, '2023-03-16 12:08:12', '2023-05-23 12:42:51'),
	(85, 1, 2, 'igg', 1, 1, 'provider', 0, '2023-03-16 12:09:00', '2023-07-21 09:28:29'),
	(86, 1, 2, 'igg', 1, 1, 'provider', 0, '2023-03-16 12:09:00', '2023-07-21 09:28:29'),
	(87, 1, 4, 'do you have any coupon ?', 1, 1, 'buyer', 0, '2023-03-17 05:23:36', '2023-05-23 12:42:51'),
	(88, 1, 2, 'ki obostha ?', 1, 1, 'buyer', 0, '2023-03-17 05:36:51', '2023-07-21 09:28:29'),
	(89, 1, 4, NULL, 1, 1, 'buyer', 20, '2023-03-18 03:38:44', '2023-05-23 12:42:51'),
	(90, 1, 4, NULL, 1, 1, 'buyer', 15, '2023-03-18 03:40:28', '2023-05-23 12:42:51'),
	(91, 1, 4, NULL, 1, 1, 'buyer', 15, '2023-03-18 03:42:55', '2023-05-23 12:42:51'),
	(92, 1, 2, NULL, 1, 1, 'buyer', 14, '2023-03-18 03:43:38', '2023-07-21 09:28:29'),
	(93, 1, 2, 'ki obostha etar ?', 1, 1, 'buyer', 0, '2023-03-18 03:43:47', '2023-07-21 09:28:29'),
	(94, 1, 2, 'obostha valo na, ami ektu busy achi, tai ses kora hoiteche na', 1, 1, 'provider', 0, '2023-03-18 03:44:36', '2023-07-21 09:28:29'),
	(95, 1, 2, 'ektu tara tari ses korte hobe', 1, 1, 'buyer', 0, '2023-03-18 03:45:43', '2023-07-21 09:28:29'),
	(96, 1, 2, NULL, 1, 1, 'buyer', 14, '2023-03-18 03:45:49', '2023-07-21 09:28:29'),
	(97, 1, 2, 'etar ki obostha', 1, 1, 'buyer', 0, '2023-03-18 03:46:23', '2023-07-21 09:28:29'),
	(98, 1, 2, NULL, 1, 1, 'buyer', 14, '2023-03-18 03:46:27', '2023-07-21 09:28:29'),
	(99, 1, 2, 'hi', 1, 1, 'buyer', 0, '2023-03-18 04:10:19', '2023-07-21 09:28:29'),
	(100, 1, 2, 'ki obostha', 1, 1, 'provider', 0, '2023-03-18 04:10:53', '2023-07-21 09:28:29'),
	(101, 1, 2, 'achi valoi', 1, 1, 'buyer', 0, '2023-03-18 04:11:06', '2023-07-21 09:28:29'),
	(102, 1, 2, 'kajer ki obostha', 1, 1, 'buyer', 0, '2023-03-18 04:11:10', '2023-07-21 09:28:29'),
	(103, 1, 2, 'kaj to ses hoy na', 1, 1, 'provider', 0, '2023-03-18 04:11:18', '2023-07-21 09:28:29'),
	(104, 1, 2, 'tara tari ses korte hobe', 1, 1, 'buyer', 0, '2023-03-18 04:11:28', '2023-07-21 09:28:29'),
	(105, 1, 4, 'hi', 1, 1, 'buyer', 0, '2023-03-18 04:41:45', '2023-05-23 12:42:51'),
	(106, 12, 4, NULL, 1, 1, 'buyer', 21, '2023-05-17 13:05:23', '2023-05-23 12:43:02'),
	(107, 1, 4, 'hola', 1, 1, 'buyer', 0, '2023-05-23 10:44:33', '2023-05-23 12:42:51'),
	(108, 1, 4, 'hola', 1, 1, 'buyer', 0, '2023-05-23 10:44:46', '2023-05-23 12:42:51'),
	(109, 1, 4, 'hola', 1, 1, 'buyer', 0, '2023-05-23 10:44:49', '2023-05-23 12:42:51');

-- Volcando datos para la tabla system_service.message_documents: ~9 rows (aproximadamente)
INSERT INTO `message_documents` (`id`, `ticket_message_id`, `file_name`, `created_at`, `updated_at`) VALUES
	(1, 6, 'support-file-1664946649.png', '2022-10-05 05:10:49', '2022-10-05 05:10:49'),
	(2, 6, 'support-file-1664946649.png', '2022-10-05 05:10:49', '2022-10-05 05:10:49'),
	(3, 6, 'support-file-1664946649.png', '2022-10-05 05:10:49', '2022-10-05 05:10:49'),
	(4, 26, 'support-file-1664949635.png', '2022-10-05 06:00:35', '2022-10-05 06:00:35'),
	(5, 26, 'support-file-1664949635.png', '2022-10-05 06:00:35', '2022-10-05 06:00:35'),
	(6, 30, 'support-file-1664949755.png', '2022-10-05 06:02:35', '2022-10-05 06:02:35'),
	(7, 30, 'support-file-1664949755.png', '2022-10-05 06:02:35', '2022-10-05 06:02:35'),
	(8, 38, 'support-file-1668053670.jpg', '2022-11-10 04:14:30', '2022-11-10 04:14:30'),
	(9, 39, 'support-file-1668053722.jpg', '2022-11-10 04:15:22', '2022-11-10 04:15:22');

-- Volcando datos para la tabla system_service.migrations: ~73 rows (aproximadamente)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2021_11_30_035230_create_admins_table', 2),
	(6, '2021_11_30_065435_create_email_configurations_table', 3),
	(7, '2021_11_30_065508_create_email_templates_table', 3),
	(8, '2021_12_01_035206_create_categories_table', 4),
	(19, '2021_12_06_054423_create_about_us_table', 10),
	(20, '2021_12_06_055028_create_custom_pages_table', 10),
	(21, '2021_12_07_030532_create_terms_and_conditions_table', 11),
	(22, '2021_12_07_035810_create_blog_categories_table', 12),
	(23, '2021_12_07_035822_create_blogs_table', 12),
	(24, '2021_12_07_040749_create_popular_posts_table', 12),
	(25, '2021_12_07_061613_create_blog_comments_table', 13),
	(30, '2021_12_09_095158_create_contact_messages_table', 16),
	(31, '2021_12_09_095220_create_subscribers_table', 16),
	(32, '2021_12_09_124226_create_settings_table', 17),
	(33, '2021_12_11_022207_create_cookie_consents_table', 18),
	(34, '2021_12_11_025358_create_google_recaptchas_table', 19),
	(35, '2021_12_11_025449_create_facebook_comments_table', 19),
	(36, '2021_12_11_025556_create_tawk_chats_table', 19),
	(37, '2021_12_11_025618_create_google_analytics_table', 19),
	(38, '2021_12_11_025712_create_custom_paginations_table', 19),
	(39, '2021_12_11_083503_create_faqs_table', 20),
	(40, '2021_12_11_094707_create_currencies_table', 21),
	(43, '2021_12_13_101056_create_error_pages_table', 23),
	(44, '2021_12_13_102725_create_maintainance_texts_table', 24),
	(45, '2021_12_13_110144_create_subscribe_modals_table', 25),
	(47, '2021_12_13_132626_create_countries_table', 27),
	(48, '2021_12_13_132909_create_country_states_table', 27),
	(49, '2021_12_13_132935_create_cities_table', 27),
	(50, '2021_12_14_032937_create_social_login_information_table', 28),
	(51, '2021_12_14_042928_create_facebook_pixels_table', 29),
	(52, '2021_12_14_054908_create_paypal_payments_table', 30),
	(53, '2021_12_14_054922_create_stripe_payments_table', 30),
	(54, '2021_12_14_054939_create_razorpay_payments_table', 30),
	(55, '2021_12_14_055252_create_bank_payments_table', 30),
	(62, '2021_12_22_034106_create_banner_images_table', 35),
	(63, '2021_12_22_044839_create_sliders_table', 36),
	(67, '2021_12_23_065722_create_paystack_and_mollies_table', 40),
	(68, '2021_12_23_085225_create_withdraw_methods_table', 41),
	(71, '2021_12_25_172918_create_seller_withdraws_table', 42),
	(81, '2021_12_26_054841_create_orders_table', 45),
	(88, '2021_12_28_192057_create_contact_pages_table', 47),
	(89, '2021_12_28_200846_create_breadcrumb_images_table', 48),
	(90, '2021_12_30_032959_create_flutterwaves_table', 49),
	(91, '2021_12_30_034716_create_footers_table', 50),
	(92, '2021_12_30_035201_create_footer_links_table', 50),
	(93, '2021_12_30_035247_create_footer_social_links_table', 50),
	(99, '2022_01_12_080218_create_seo_settings_table', 54),
	(100, '2022_01_17_012111_create_menu_visibilities_table', 55),
	(101, '2022_01_17_122016_create_instamojo_payments_table', 56),
	(102, '2022_01_29_055523_create_messages_table', 57),
	(103, '2022_01_29_122621_create_pusher_credentails_table', 58),
	(104, '2022_08_28_070755_create_how_it_works_table', 59),
	(105, '2022_08_29_072358_create_testimonials_table', 60),
	(106, '2022_08_31_083601_create_services_table', 61),
	(108, '2022_08_31_093322_create_additional_services_table', 62),
	(112, '2022_09_01_103923_create_schedules_table', 63),
	(113, '2022_09_05_111413_create_refund_requests_table', 64),
	(114, '2022_09_06_054021_create_complete_requests_table', 65),
	(115, '2022_09_06_064506_create_provider_client_reports_table', 66),
	(116, '2022_09_06_072831_create_tickets_table', 67),
	(117, '2022_09_06_073338_create_ticket_messages_table', 67),
	(118, '2022_09_06_101227_create_message_documents_table', 68),
	(119, '2022_09_26_070233_create_section_contents_table', 69),
	(120, '2022_09_26_083106_create_section_controls_table', 70),
	(121, '2022_09_29_044208_create_provider_client_reports_table', 71),
	(122, '2023_01_09_043222_create_appointment_schedules_table', 72),
	(123, '2023_02_02_062116_create_mobile_sliders_table', 73),
	(124, '2023_03_09_045111_create_coupons_table', 74),
	(125, '2023_03_09_055745_create_coupon_histories_table', 75);

-- Volcando datos para la tabla system_service.mobile_sliders: ~3 rows (aproximadamente)
INSERT INTO `mobile_sliders` (`id`, `title_one`, `title_two`, `image`, `status`, `serial`, `created_at`, `updated_at`) VALUES
	(1, 'Title One', 'Service', 'uploads/custom-images/mb-slider-2023-02-02-01-17-30-2566.jpg', 1, 2, '2023-02-02 06:55:00', '2023-02-02 07:17:30'),
	(3, 'Digital marketing', 'Title Two', 'uploads/custom-images/mb-slider-2023-02-02-01-17-19-2477.jpg', 1, 1, '2023-02-02 07:17:19', '2023-02-02 07:18:26'),
	(4, 'Wemen\'s Jeans Collection', '35% Offer', 'uploads/custom-images/mb-slider-2023-02-02-01-18-15-4748.jpg', 1, 10, '2023-02-02 07:18:16', '2023-02-02 07:18:36');

-- Volcando datos para la tabla system_service.orders: ~5 rows (aproximadamente)
INSERT INTO `orders` (`id`, `order_id`, `client_id`, `provider_id`, `service_id`, `package_amount`, `permisos`, `total_amount`, `booking_date`, `appointment_schedule_id`, `schedule_time_slot`, `additional_amount`, `payment_method`, `payment_status`, `refound_status`, `payment_refound_date`, `transection_id`, `order_status`, `order_approval_date`, `order_completed_date`, `order_declined_date`, `package_features`, `additional_services`, `client_address`, `order_note`, `complete_by_admin`, `created_at`, `updated_at`, `coupon_discount`) VALUES
	(70, '644899810', 1, 2, 13, 10, 0, 28.9, '04-04-2023', 52, '01:30 PM - 02:00 PM', 24, 'Bank Payment', 'pending', 0, NULL, 'TNX_3343KJDFDFR334', 'approved_by_provider', NULL, NULL, NULL, '["Room Cleaning","Roof Clean","Kitchen Clean","Washroom","Kitchenroom Cleaning"]', '[{"service_name":"Service One","qty":"1","price":12},{"service_name":"Service Two","qty":"1","price":9},{"service_name":"Service Three","qty":"1","price":3}]', '{"name":"John Doe","email":"user@gmail.com","phone":"125-985-4587","address":"Los Angeles, CA, USA","post_code":"5841","order_note":null}', NULL, NULL, '2023-03-09 09:31:14', '2023-05-20 09:17:52', 5.1),
	(71, '705192750', 1, 4, 15, 28, 0, 38, '30-04-2023', 88, '08:20 AM - 08:40 AM', 10, 'Bank Payment', 'pending', 0, NULL, 'teste', 'awaiting_for_provider_approval', NULL, NULL, NULL, '["Room Cleaning","Roof Clean","Kitchen Clean","Washroom","Kitchenroom Cleaning"]', '[{"service_name":"Service One","qty":"1","price":10}]', '{"name":"ramiro","email":"user@gmail.com","phone":"67470820","address":"sacaba cochabamba","post_code":"0000","order_note":"testeando"}', 'testeando', NULL, '2023-04-30 00:24:10', '2023-04-30 00:24:10', 0),
	(72, '1430191076', 1, 4, 19, 65, 0, 65, '24-05-2023', 139, '08:20 AM - 08:40 AM', 0, 'Bank Payment', 'pending', 0, NULL, 'teste', 'awaiting_for_provider_approval', NULL, NULL, NULL, '["Room Cleaning","Roof Clean","Kitchen Clean","Washroom","Kitchenroom Cleaning"]', '[]', '{"name":"jhuliza","email":"admin@admin.com","phone":"78451212","address":"AV. TUNEL","post_code":"0000","order_note":"quiero este servicio donde estoy realizando las pruebas"}', 'quiero este servicio donde estoy realizando las pruebas', NULL, '2023-05-24 10:53:16', '2023-05-24 10:53:16', 0),
	(73, '1117777495', 1, 4, 19, 65, 0, 65, '06-06-2023', 127, '10:00 AM - 10:20 AM', 0, 'Bank Payment', 'pending', 0, NULL, 'TNX_3343KJDFDFR334', 'awaiting_for_provider_approval', NULL, NULL, NULL, '["Room Cleaning","Roof Clean","Kitchen Clean","Washroom","Kitchenroom Cleaning"]', '[]', '{"name":"jhuliza delgadillo","email":"jhuliza@gmail.com","phone":"65440220","address":"Equipetrol, Santa Cruz","post_code":"0000","order_note":"asiendo pruebas de test"}', 'asiendo pruebas de test', NULL, '2023-06-06 07:55:29', '2023-06-06 07:55:29', 0),
	(74, '263849360', 1, 4, 19, 65, 0, 65, '05-06-2023', 104, '08:00 AM - 08:20 AM', 0, 'Bank Payment', 'pending', 0, NULL, NULL, 'awaiting_for_provider_approval', NULL, NULL, NULL, '["Room Cleaning","Roof Clean","Kitchen Clean","Washroom","Kitchenroom Cleaning"]', '[]', '{"name":"juana","email":"agente@agente.com","phone":"5498623","address":"Equipetrol, Santa Cruz","post_code":"0000","order_note":"tester"}', 'tester', NULL, '2023-06-06 13:11:02', '2023-06-06 13:11:02', 0);

-- Volcando datos para la tabla system_service.partners: ~8 rows (aproximadamente)
INSERT INTO `partners` (`id`, `link`, `logo`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'https://websolutionus.com/', 'uploads/custom-images/our-partner-2022-09-29-12-53-34-4755.jpg', 1, '2022-09-29 06:53:35', '2022-09-29 06:53:35'),
	(2, 'https://websolutionus.com/service', 'uploads/custom-images/our-partner-2022-09-29-12-54-08-8857.jpg', 1, '2022-09-29 06:54:08', '2022-09-29 06:54:08'),
	(3, 'https://codecanyon.net/user/websolutionus/portfolio', 'uploads/custom-images/our-partner-2022-09-29-12-54-34-2602.jpg', 1, '2022-09-29 06:54:34', '2022-09-29 06:54:34'),
	(4, 'https://www.google.com/', 'uploads/custom-images/-2023-01-15-03-30-10-1839.png', 1, '2022-09-29 06:54:54', '2023-01-15 09:30:10'),
	(5, NULL, 'uploads/custom-images/our-partner-2022-09-29-12-55-08-6101.jpg', 1, '2022-09-29 06:55:08', '2022-09-29 06:55:08'),
	(6, NULL, 'uploads/custom-images/our-partner-2022-09-29-12-55-25-2540.jpg', 1, '2022-09-29 06:55:25', '2022-09-29 06:55:25'),
	(7, NULL, 'uploads/custom-images/our-partner-2022-09-29-12-55-42-2263.jpg', 1, '2022-09-29 06:55:42', '2022-09-29 06:55:42'),
	(8, NULL, 'uploads/custom-images/our-partner-2022-09-29-12-55-55-5814.jpg', 1, '2022-09-29 06:55:55', '2022-09-29 06:55:55');

-- Volcando datos para la tabla system_service.password_resets: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.payment_coupon: ~0 rows (aproximadamente)
INSERT INTO `payment_coupon` (`id`, `name`, `provider_id`, `amount`, `date`, `coupon_id`, `type_payment`, `limit_coupon`, `status`, `service_ids`, `created_at`, `updated_at`) VALUES
	(1, 'tester', 2, 150, '2023-06-08', 6, 'paypal', 3, 1, '"18,19,20"', '2023-06-07 16:58:02', '2023-06-07 16:58:02');

-- Volcando datos para la tabla system_service.payment_decoration: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.payment_material: ~0 rows (aproximadamente)
INSERT INTO `payment_material` (`id`, `material_id`, `price`, `date`, `email`, `name`, `phone`, `note`, `price_trasnport`, `type_payment`, `created_at`, `updated_at`) VALUES
	(1, 0, 225, '0000-00-00', '201601ramiro@gmail.com', 'jhuliza delgadillo moscoso', 67470820, 'testenlmlkjkj', 45, 'paypal', '2023-07-07 03:18:54', '2023-07-07 03:18:54');

-- Volcando datos para la tabla system_service.paypal_payments: ~0 rows (aproximadamente)
INSERT INTO `paypal_payments` (`id`, `status`, `account_mode`, `client_id`, `secret_id`, `country_code`, `currency_code`, `currency_rate`, `image`, `created_at`, `updated_at`) VALUES
	(1, 1, 'sandbox', 'AWlV5x8Lhj9BRF8-TnawXtbNs-zt69mMVXME1BGJUIoDdrAYz8QIeeTBQp0sc2nIL9E529KJZys32Ipy', 'EEvn1J_oIC6alxb-FoF4t8buKwy4uEWHJ4_Jd_wolaSPRMzFHe6GrMrliZAtawDDuE-WKkCKpWGiz0Yn', 'US', 'USD', 6.98, 'uploads/website-images/paypal-2022-09-25-10-04-36-1837.png', NULL, '2023-07-18 02:51:25');

-- Volcando datos para la tabla system_service.paystack_and_mollies: ~0 rows (aproximadamente)
INSERT INTO `paystack_and_mollies` (`id`, `mollie_key`, `mollie_status`, `mollie_currency_rate`, `paystack_public_key`, `paystack_secret_key`, `paystack_currency_rate`, `paystack_status`, `mollie_country_code`, `mollie_currency_code`, `paystack_country_code`, `paystack_currency_code`, `mollie_image`, `paystack_image`, `created_at`, `updated_at`) VALUES
	(1, 'test_HFc5UhscNSGD5jujawhtNFs3wM3B4n', 0, 1.38, 'pk_test_057dfe5dee14eaf9c3b4573df1e3760c02c06e38', 'sk_test_77cb93329abbdc18104466e694c9f720a7d69c97', 460.49, 0, 'CA', 'CAD', 'NG', 'NGN', 'uploads/website-images/mollie-2022-09-25-10-05-09-3231.png', 'uploads/website-images/paystact-2022-09-25-10-05-19-6818.png', NULL, '2023-06-06 10:05:55');

-- Volcando datos para la tabla system_service.personal_access_tokens: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.popular_posts: ~4 rows (aproximadamente)
INSERT INTO `popular_posts` (`id`, `blog_id`, `status`, `created_at`, `updated_at`) VALUES
	(2, 6, 1, '2022-09-29 05:49:56', '2022-09-29 05:49:56'),
	(3, 7, 1, '2022-09-29 05:50:02', '2022-09-29 05:50:02'),
	(4, 2, 1, '2022-09-29 05:50:33', '2022-09-29 05:50:33'),
	(5, 1, 1, '2022-12-25 10:48:28', '2022-12-25 10:48:28');

-- Volcando datos para la tabla system_service.preguntas: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.property: ~4 rows (aproximadamente)

-- Volcando datos para la tabla system_service.property_category: ~13 rows (aproximadamente)
INSERT INTO `property_category` (`id`, `name`, `code`, `created_at`, `updated_at`, `status`) VALUES
	(1, 'Casa', 'CS', '2023-05-08 20:13:07', '2023-05-08 20:13:08', 1),
	(2, 'Departamento', 'DP', '2023-05-08 20:13:36', '2023-05-08 20:13:37', 1),
	(3, 'Oficina', 'OF', '2023-05-08 20:13:57', '2023-05-08 20:13:58', 1),
	(4, 'Condominio', 'CD', '2023-05-12 03:13:03', '2023-05-12 03:13:04', 1),
	(5, 'Local comercial', 'LC', '2023-05-12 03:13:29', '2023-05-12 03:13:29', 1),
	(6, 'Galpón', 'GP', '2023-05-12 03:14:00', '2023-05-12 03:14:01', 1),
	(7, 'Edificio', 'ED', '2023-05-12 03:14:34', '2023-05-12 03:14:35', 1),
	(8, 'Lote o terreno', 'LT', '2023-05-12 03:14:59', '2023-05-12 03:15:00', 1),
	(9, 'Habitación', 'HB', '2023-05-12 03:15:27', '2023-05-12 03:15:28', 1),
	(10, 'Parqueo', 'PQ', '2023-05-12 03:15:53', '2023-05-12 03:15:54', 1),
	(11, 'Quinta o propiedad', 'QP', '2023-05-12 03:16:20', '2023-05-12 03:16:21', 1),
	(12, 'Garage', 'GR', '2023-05-12 03:16:42', '2023-05-12 03:16:43', 1),
	(13, 'Otros', 'OT', '2023-05-12 03:17:00', '2023-05-12 03:17:00', 1);

-- Volcando datos para la tabla system_service.provider_client_reports: ~0 rows (aproximadamente)

-- Volcando datos para la tabla system_service.provider_withdraws: ~2 rows (aproximadamente)
INSERT INTO `provider_withdraws` (`id`, `user_id`, `method`, `total_amount`, `withdraw_amount`, `withdraw_charge`, `account_info`, `status`, `approved_date`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Bank Payment', 12, 11.88, 1, 'IBBL Uttara Branch,\r\nAccount : 4545315455...45541', 1, '2022-12-25', '2022-10-08 03:26:18', '2022-12-25 11:15:38'),
	(2, 2, 'Bank Payment', 15, 14.85, 1, 'Bank Name:  IBBL\r\nAccount Number:  545455.....4587555\r\nBranch: Uttara, Dhaka', 0, NULL, '2022-12-26 04:56:10', '2022-12-26 04:56:10');

-- Volcando datos para la tabla system_service.pusher_credentails: ~0 rows (aproximadamente)
INSERT INTO `pusher_credentails` (`id`, `app_id`, `app_key`, `app_secret`, `app_cluster`, `created_at`, `updated_at`) VALUES
	(1, '1338069', 'e013174602072a186b1d', '46de951521010c14b205', 'mt1', NULL, '2022-01-29 12:41:05');

-- Volcando datos para la tabla system_service.qualification_property: ~3 rows (aproximadamente)

-- Volcando datos para la tabla system_service.razorpay_payments: ~0 rows (aproximadamente)
INSERT INTO `razorpay_payments` (`id`, `status`, `name`, `currency_rate`, `country_code`, `currency_code`, `description`, `image`, `color`, `key`, `secret_key`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Ecommerce', 74.66, 'IN', 'INR', 'This is description', 'uploads/website-images/razorpay-2022-09-25-09-45-59-6378.png', '#2d15e5', 'rzp_test_K7CipNQYyyMPiS', 'zSBmNMorJrirOrnDrbOd1ALO', NULL, '2023-06-06 10:04:49');

-- Volcando datos para la tabla system_service.refund_requests: ~4 rows (aproximadamente)
INSERT INTO `refund_requests` (`id`, `client_id`, `order_id`, `account_information`, `resone`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 4, 'IBBL AAA Branch\r\nAccount Number : 32145221112', 'this is test resone', 'awaiting_for_admin_approval', '2022-10-04 09:54:02', '2022-10-04 09:54:02'),
	(2, 1, 5, 'this is my bank account number.', 'this is test resone', 'awaiting_for_admin_approval', '2022-10-04 10:17:51', '2022-10-04 10:17:51'),
	(3, 1, 6, 'this is my bank information.', 'this is test resone', 'awaiting_for_admin_approval', '2022-11-09 05:49:24', '2022-11-09 05:49:24'),
	(4, 1, 10, 'this is my account information\r\nIBBL USA, Account : 9485998434.....9895453', 'This is test resone', 'awaiting_for_admin_approval', '2022-12-21 03:47:22', '2022-12-21 03:47:22');

-- Volcando datos para la tabla system_service.reviews: ~4 rows (aproximadamente)
INSERT INTO `reviews` (`id`, `service_id`, `user_id`, `provider_id`, `review`, `rating`, `status`, `created_at`, `updated_at`) VALUES
	(1, 13, 1, 21, 'Lorem ipsum dolor sit amet, nibh saperet te pri, at nam diceret disputationi. Quo an consul impedit, usu possim evertitur dissentiet ei, ridens minimum nominavi et vix.', 5, 1, '2022-10-03 11:22:13', '2022-10-03 11:22:36'),
	(2, 12, 1, 2, 'There are many variations of passages of Lo rem Ipsum available but the majorty have suffered in as some form, by injected humour.', 5, 1, '2022-10-04 10:29:20', '2022-10-04 10:29:48'),
	(3, 21, 1, 4, 'There are many variations of passages of Lo rem Ipsum available but the majorty have suffered in as some form', 5, 1, '2023-01-14 04:55:14', '2023-01-14 04:55:38'),
	(4, 21, 2, 4, 'If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anythng embarrassing as hidden in the middle of text.', 5, 1, '2023-01-14 05:07:38', '2023-01-14 05:07:53');

-- Volcando datos para la tabla system_service.schedules: ~7 rows (aproximadamente)
INSERT INTO `schedules` (`id`, `provider_id`, `day`, `serial_of_day`, `start`, `end`, `status`, `created_at`, `updated_at`) VALUES
	(43, 2, 'Sunday', 0, '10:00', '18:00', 1, '2022-10-05 10:32:35', '2022-10-05 10:32:35'),
	(44, 2, 'Monday', 1, '09:00', '19:00', 1, '2022-10-05 10:32:35', '2022-11-10 03:58:00'),
	(45, 2, 'Tuesday', 2, '10:30', '18:00', 1, '2022-10-05 10:32:35', '2022-11-10 03:58:00'),
	(46, 2, 'Wednesday', 3, '08:00', '20:00', 1, '2022-10-05 10:32:35', '2022-11-10 03:58:00'),
	(47, 2, 'Thursday', 4, '09:30', '19:30', 1, '2022-10-05 10:32:35', '2022-11-10 03:58:00'),
	(48, 2, 'Friday', 5, '10:00', '18:30', 0, '2022-10-05 10:32:35', '2022-11-10 03:58:00'),
	(49, 2, 'Saturday', 6, '11:00', '18:00', 0, '2022-10-05 10:32:35', '2022-11-10 03:58:00');

-- Volcando datos para la tabla system_service.section_contents: ~5 rows (aproximadamente)
INSERT INTO `section_contents` (`id`, `section_name`, `title`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Category', 'Our Categories', 'There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration', NULL, '2023-01-15 04:23:04'),
	(2, 'Featured Service', 'Featured Services', 'There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration', NULL, '2022-11-06 07:11:42'),
	(3, 'Popular Service', 'Servicios Recomendados:', 'There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration', NULL, '2023-04-30 08:17:17'),
	(4, 'Testimonial', 'Testimonial', 'There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration', NULL, '2022-11-06 07:11:56'),
	(5, 'Latest News', 'Latest News', 'There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration', NULL, '2022-11-06 07:12:04');

-- Volcando datos para la tabla system_service.section_controls: ~20 rows (aproximadamente)
INSERT INTO `section_controls` (`id`, `page_name`, `section_name`, `status`, `qty`, `created_at`, `updated_at`) VALUES
	(1, 'home1', 'Intro(Home1, Home2, Home3)', 1, 0, NULL, '2022-09-27 07:34:04'),
	(2, 'home1', 'Category (Home1, Home2, Home3)', 1, 9, NULL, '2022-09-29 07:30:29'),
	(3, 'home1', 'Featured Services (Home1, Home2, Home3)', 1, 6, NULL, '2022-10-03 10:20:38'),
	(4, 'home1', 'Countdown (Home1, Home2, Home3)', 1, 4, NULL, '2022-09-29 06:42:30'),
	(5, 'home1', 'Popular Service (Home1, Home2, Home3)', 1, 6, NULL, '2022-10-03 10:21:35'),
	(6, 'home1', 'Join as a provider (Home1, Home2, Home3)', 1, 0, NULL, '2022-09-27 08:08:01'),
	(7, 'home1', 'Mobile app (Home1, Home2, Home3)', 1, 0, NULL, '2022-09-27 08:11:30'),
	(8, 'home1', 'Testimonial (Home1, Home2, Home3)', 1, 6, NULL, '2022-09-29 06:47:03'),
	(9, 'home1', 'Blog (Home1, Home2, Home3)', 1, 3, NULL, '2022-12-21 03:06:41'),
	(10, 'home1', 'Subscribe Now (Home1, Home2, Home3)', 1, 0, NULL, NULL),
	(21, 'home2', 'Contact Us(Home2)', 1, 0, NULL, '2022-09-27 09:07:40'),
	(22, 'home2', 'Our Partner(Home2, Home3)', 1, 20, NULL, '2022-09-29 06:56:54'),
	(33, 'home3', 'How it work (Home3)', 1, 0, NULL, NULL),
	(35, 'about', 'How it work(About)', 1, 0, NULL, '2022-09-27 09:19:53'),
	(36, 'about', 'About Us (About)', 1, 0, NULL, '2022-09-27 09:19:53'),
	(37, 'about', 'Countdown (About)', 1, 4, NULL, '2022-09-29 06:42:30'),
	(38, 'about', 'Why choose us (About)', 1, 0, NULL, '2022-09-27 09:25:25'),
	(39, 'about', 'Join as provider (About)', 0, 0, NULL, '2022-09-27 09:30:26'),
	(40, 'about', 'Testimonial (About)', 1, 6, NULL, '2022-09-29 06:47:04'),
	(41, 'service', 'Our Partner(Service)', 1, 20, NULL, '2022-09-29 06:56:54');

-- Volcando datos para la tabla system_service.seo_settings: ~11 rows (aproximadamente)
INSERT INTO `seo_settings` (`id`, `page_name`, `seo_title`, `seo_description`, `created_at`, `updated_at`) VALUES
	(1, 'Home Page', 'Home page - Service', 'Home Page', NULL, '2022-09-27 10:11:58'),
	(2, 'About Us', 'About Us - Service', 'About Us', NULL, '2022-09-27 10:12:02'),
	(3, 'Contact Us', 'Contact Us - Service', 'Contact Us', NULL, '2022-09-27 10:12:07'),
	(5, 'Service', 'Our Service - Service', 'Our Service', NULL, '2022-09-27 10:19:48'),
	(6, 'Blog', 'Blog - Service', 'Blog', NULL, '2022-09-27 10:12:15'),
	(7, 'Inmuebles', 'Nuestros Inmuebles - Inmueble ', 'Inmuebles', '2023-05-11 14:36:02', '2023-05-11 14:36:01'),
	(8, 'Detalle Inmueble', 'Detalle-Inmueble', 'Detail', '2023-05-15 23:29:00', '2023-05-15 23:29:00'),
	(9, 'Materiales', 'Material-Redafor', 'Material', '2023-06-14 23:42:37', '2023-06-14 23:42:38'),
	(10, 'Construccion', 'Construccion-Redafor', 'Construction', '2023-06-20 01:55:48', '2023-06-20 01:55:51'),
	(11, 'Terrenos', 'Terrenos-Redafor', 'Terrenos', '2023-06-23 13:47:03', '2023-06-23 13:47:04'),
	(12, 'Decoracion y Amoblado', 'Decoration ', 'Decoration', '2023-07-18 13:27:20', '2023-07-18 13:27:21');

-- Volcando datos para la tabla system_service.services: ~21 rows (aproximadamente)

-- Volcando datos para la tabla system_service.settings: ~0 rows (aproximadamente)
INSERT INTO `settings` (`id`, `maintenance_mode`, `logo`, `favicon`, `contact_email`, `enable_subscription_notify`, `enable_save_contact_message`, `text_direction`, `timezone`, `sidebar_lg_header`, `sidebar_sm_header`, `topbar_phone`, `topbar_email`, `opening_time`, `currency_name`, `currency_icon`, `currency_rate`, `theme_one`, `counter_bg_image`, `join_as_a_provider_banner`, `home2_join_as_provider`, `home3_join_as_provider`, `join_as_a_provider_title`, `join_as_a_provider_btn`, `app_short_title`, `app_full_title`, `app_description`, `google_playstore_link`, `app_store_link`, `app_image`, `home2_app_image`, `home3_app_image`, `subscriber_image`, `subscriber_title`, `subscriber_description`, `subscription_bg`, `home2_subscription_bg`, `home3_subscription_bg`, `blog_page_subscription_image`, `default_avatar`, `home2_contact_foreground`, `home2_contact_background`, `home2_contact_call_as`, `home2_contact_phone`, `home2_contact_available`, `home2_contact_form_title`, `home2_contact_form_description`, `how_it_work_background`, `how_it_work_foreground`, `how_it_work_title`, `how_it_work_description`, `how_it_work_items`, `selected_theme`, `theme_one_color`, `theme_two_color`, `theme_three_color`, `login_image`, `footer_logo`, `created_at`, `updated_at`, `show_provider_contact_info`) VALUES
	(1, 1, 'uploads/website-images/logo-2023-07-20-09-04-22-2706.png', 'uploads/website-images/favicon-2023-04-27-10-21-24-7868.png', 'contact@gmail.com', 1, 1, 'ltr', 'Asia/Dhaka', 'Redafor', 'RD', '+591 67470820', 'infor@redafor.com', '10.00 AM-7.00PM', 'BOB', '$', 85.76, '#009bc2', 'uploads/website-images/counter-bg--2022-09-29-12-43-47-5215.jpg', 'uploads/website-images/join-provider-bg--2022-12-03-06-07-16-1842.png', 'uploads/website-images/join-provider-home2bg--2022-10-04-10-15-33-5535.jpg', 'uploads/website-images/join-provider-home2bg--2022-12-03-06-07-18-5741.png', 'Join with us to Sale your service & growth your Experience', 'Provider Joining', 'Descarga ahora', 'REDAFOR APP store', 'SIstema Redafor donde encontraras lo que necesitas', 'https://play.google.com/store/apps/redafor', 'https://www.apple.com/app-store/redafor', 'uploads/website-images/mobile-app-bg--2022-08-29-01-17-54-3596.png', 'uploads/website-images/mobile-app-bg--2022-09-22-11-27-36-1745.png', 'uploads/website-images/mobile-app-bg--2022-09-22-11-27-52-2026.png', 'uploads/website-images/sub-foreground--2022-09-08-10-47-16-9543.png', 'Subscribe Now', 'Get the updates, offers, tips and enhance your page building experience', 'uploads/website-images/sub-background-2022-09-08-10-47-05-7260.jpg', 'uploads/website-images/sub-background-2022-09-22-11-42-07-6877.jpg', 'uploads/website-images/sub-background-2022-09-22-11-41-47-4054.jpg', 'uploads/website-images/blog-sub-background-2023-05-25-08-33-43-1332.png', 'uploads/website-images/default-avatar-2022-12-25-04-17-13-8891.jpg', 'uploads/website-images/home2-contact-foreground--2022-12-03-06-08-24-3082.png', 'uploads/website-images/home2-contact-background-2022-09-22-12-08-16-6090.jpg', 'Call as now', '+90 456 789 251', 'We are available 24/7', 'Do you have any question ?', 'Fill the form now & Request an Estimate', 'uploads/website-images/home3-hiw-background-2022-09-22-12-52-40-5965.jpg', 'uploads/website-images/home3-hiw-foreground--2022-09-29-01-06-00-1394.jpg', 'Enjoy Services', 'If you are going to use a passage of you need to be sure there isn\'t anything emc barrassing hidden in the middle', '[{"title":"Select The Service","description":"There are many variations of passages of Lorem Ipsum available, but the majority have"},{"title":"Pick Your Schedule","description":"There are many variations of passages of Lorem Ipsum available, but the majority have"},{"title":"Place Your Booking & Relax","description":"There are many variations of passages of Lorem Ipsum available, but the majority have"}]', 0, '#05ad59', '#01ad57', '#01b259', 'uploads/website-images/login-page-2022-11-06-04-12-11-6638.png', 'uploads/website-images/logo-2023-07-20-09-04-22-9099.png', NULL, '2023-07-21 01:45:27', 1);

-- Volcando datos para la tabla system_service.sliders: ~0 rows (aproximadamente)
INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `header_one`, `header_two`, `total_service_sold`, `home2_image`, `home3_image`, `popular_tag`, `created_at`, `updated_at`) VALUES
	(1, 'Premium Service 24/7', 'There are many variations of passages of Lorem Ipsum available, but or randomised words which don\'t look', 'uploads/website-images/slider-2022-10-01-11-03-17-9020.png', 'We Provide High Quality Professional', 'Services', '43434', 'uploads/website-images/slider-2023-01-15-05-42-46-4524.png', 'uploads/website-images/slider-2022-09-22-11-15-09-1295.png', '[{"value":"Painting"},{"value":"Cleaner"},{"value":"Home Move"},{"value":"Electronics"}]', '2022-01-30 10:25:59', '2023-01-15 11:42:48');

-- Volcando datos para la tabla system_service.social_login_information: ~0 rows (aproximadamente)
INSERT INTO `social_login_information` (`id`, `is_facebook`, `facebook_client_id`, `facebook_secret_id`, `is_gmail`, `gmail_client_id`, `gmail_secret_id`, `facebook_redirect_url`, `gmail_redirect_url`, `created_at`, `updated_at`) VALUES
	(1, 1, '1844188565781706', 'f32f45abcf57a4dc23ac6f2b2e8e2241', 1, '810593187924-706in12herrovuq39i2pbn483otijei8.apps.googleusercontent.com', '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', 'http://localhost/web-solution/desocom/callback/google', 'http://localhost/web-solution/desocom/callback/google', NULL, '2023-07-04 13:02:38');

-- Volcando datos para la tabla system_service.state_property: ~4 rows (aproximadamente)
INSERT INTO `state_property` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'En pozo', 'OF', '2023-05-08 20:15:15', '2023-05-08 20:15:16'),
	(2, 'En construcción', 'VN', '2023-05-08 20:15:32', '2023-05-08 20:15:32'),
	(3, 'A estrenar', 'AS', '2023-05-12 03:30:46', '2023-05-12 03:30:47'),
	(4, 'Usados', 'UD', '2023-05-12 03:31:08', '2023-05-12 03:31:09');

-- Volcando datos para la tabla system_service.store_asistente: ~0 rows (aproximadamente)
INSERT INTO `store_asistente` (`id`, `name`, `phone`, `email`, `message`, `code`, `status`, `created_at`, `updated_at`) VALUES
	(2, 'ramiro', 67470820, '201601ramiro@gmail.com', 'asiendo otras pruebas', '2', 1, '2023-07-31 12:20:31', '2023-07-31 12:20:31');

-- Volcando datos para la tabla system_service.stripe_payments: ~0 rows (aproximadamente)
INSERT INTO `stripe_payments` (`id`, `status`, `stripe_key`, `stripe_secret`, `created_at`, `updated_at`, `country_code`, `currency_code`, `currency_rate`, `image`) VALUES
	(1, 1, 'pk_test_51JU61aF56Pb8BOOX5ucAe5DlDwAkCZyffqlKMDUWsAwhKbdtuY71VvIzr0NgFKjq4sOVVaaeeyVXXnNWwu5dKgeq00kMzCBUm5', 'sk_test_51JU61aF56Pb8BOOXlz7jGkzJsCkozuAoRlFJskYGsgunfaHLmcvKLubYRQLCQOuyYHq0mvjoBFLzV7d8F6q8f6Hv00CGwULEEV', NULL, '2023-06-27 09:17:28', 'BO', 'BOB', 6.98, 'uploads/website-images/stripe-2023-06-27-05-04-06-8976.jpg');

-- Volcando datos para la tabla system_service.subscribers: ~3 rows (aproximadamente)
INSERT INTO `subscribers` (`id`, `email`, `status`, `verified_token`, `is_verified`, `created_at`, `updated_at`) VALUES
	(1, 'testapi@gmail.com', 0, 'ZaCyHZZFSJyYQh9Er4ptOPumu', 0, '2022-11-08 09:59:18', '2022-11-08 09:59:18'),
	(2, '201601ramiro@gmail.com', 0, 'YgyUKrGSYcU20NNstKbmnNOf9', 0, '2023-05-27 02:23:12', '2023-05-27 02:23:12'),
	(3, 'transatlantacbba@gmail.com', 0, NULL, 1, '2023-05-27 02:36:22', '2023-05-27 02:36:54');

-- Volcando datos para la tabla system_service.tawk_chats: ~0 rows (aproximadamente)
INSERT INTO `tawk_chats` (`id`, `chat_link`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'https://embed.tawk.to/5a7c31ded7591465c7077c48/default', 0, NULL, '2022-10-08 05:54:40');

-- Volcando datos para la tabla system_service.terms_and_conditions: ~0 rows (aproximadamente)
INSERT INTO `terms_and_conditions` (`id`, `terms_and_condition`, `privacy_policy`, `created_at`, `updated_at`) VALUES
	(1, '<p>T&eacute;rminos y Condiciones</p>\r\n<p>Bienvenido/a a REDAFOR. Al acceder y utilizar nuestra plataforma web, aceptas cumplir con los siguientes t&eacute;rminos y condiciones que rigen su uso. Si no est&aacute;s de acuerdo con alguno de estos t&eacute;rminos, te solicitamos que no utilices nuestro sitio web.</p>\r\n<p><strong>1. Uso de la plataforma</strong></p>\r\n<p>Nuestra plataforma tiene como objetivo ofrecer servicios relacionados con inmuebles, construcci&oacute;n y decoraci&oacute;n. Puedes utilizarla para buscar, ofrecer, comprar, vender o alquilar propiedades, contratar servicios de construcci&oacute;n o decoraci&oacute;n, as&iacute; como para obtener informaci&oacute;n y asesoramiento relacionado.&nbsp; Al utilizar nuestra plataforma, te comprometes a proporcionar informaci&oacute;n precisa, completa y actualizada en todo momento. No est&aacute; permitido utilizar informaci&oacute;n falsa, enga&ntilde;osa o fraudulenta.</p>\r\n<p><strong>2. Propiedad intelectual</strong></p>\r\n<p>Todo el contenido presente en nuestra plataforma, incluyendo logotipos, im&aacute;genes, textos, videos, dise&ntilde;os, software y cualquier otro elemento, est&aacute; protegido por derechos de propiedad intelectual. No est&aacute; permitido utilizar, reproducir, modificar o distribuir dicho contenido sin nuestro consentimiento previo por escrito.</p>\r\n<p><strong>3. Responsabilidad</strong></p>\r\n<p>Nuestra plataforma se proporciona "tal cual" y no ofrecemos garant&iacute;as expresas o impl&iacute;citas sobre su funcionamiento, exactitud, confiabilidad o disponibilidad. No nos responsabilizamos por cualquier p&eacute;rdida, da&ntilde;o o perjuicio derivado del uso de nuestra plataforma o de la informaci&oacute;n proporcionada en ella. No nos hacemos responsables de las transacciones realizadas entre usuarios, incluyendo la compra, venta o alquiler de propiedades, o la contrataci&oacute;n de servicios. Los usuarios son responsables de verificar la veracidad, calidad y legalidad de las transacciones y acuerdos realizados.</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p><strong>4. Privacidad&nbsp;</strong></p>\r\n<p>Respetamos tu privacidad y nos comprometemos a proteger tus datos personales de acuerdo con nuestra Pol&iacute;tica de Privacidad. Te recomendamos leer detenidamente dicha pol&iacute;tica para comprender c&oacute;mo recopilamos, utilizamos y protegemos tu informaci&oacute;n.</p>\r\n<p><strong>5. Enlaces externos</strong></p>\r\n<p>Nuestra plataforma puede contener enlaces a sitios web de terceros. No tenemos control sobre el contenido, las pol&iacute;ticas de privacidad o los t&eacute;rminos y condiciones de esos sitios. No nos hacemos responsables de cualquier da&ntilde;o o perjuicio derivado del uso de esos sitios externos.</p>\r\n<p><strong>6. Modificaciones&nbsp;</strong></p>\r\n<p>Nos reservamos el derecho de realizar modificaciones, actualizaciones o cambios en nuestros t&eacute;rminos y condiciones en cualquier momento. Te recomendamos revisar peri&oacute;dicamente esta secci&oacute;n para estar informado de cualquier actualizaci&oacute;n.</p>\r\n<p>Al utilizar nuestra plataforma, confirmas que has le&iacute;do, comprendido y aceptado estos t&eacute;rminos y condiciones en su totalidad. Si tienes alguna pregunta o inquietud, por favor cont&aacute;ctanos a trav&eacute;s de los canales de comunicaci&oacute;n proporcionados en nuestro sitio web.</p>\r\n\r\n\r\n\r\n\r\n\r\n', '<p><strong>Pol&iacute;ticas de Privacidad</strong></p>\r\n<p>En REDAFOR, nos comprometemos a proteger tu privacidad y garantizar la seguridad de tus datos personales. Esta pol&iacute;tica de privacidad describe c&oacute;mo recopilamos, utilizamos y protegemos la informaci&oacute;n que nos proporcionas al utilizar nuestra plataforma. Al utilizar nuestros servicios, aceptas las pr&aacute;cticas descritas en esta pol&iacute;tica de privacidad.</p>\r\n<p><strong>1. Informaci&oacute;n que recopilamos&nbsp;</strong>&nbsp;</p>\r\n<p>Recopilamos informaci&oacute;n personal que nos proporcionas al registrarte en nuestra plataforma, como tu nombre, direcci&oacute;n de correo electr&oacute;nico, n&uacute;mero de tel&eacute;fono y datos de contacto.Tambi&eacute;n podemos recopilar informaci&oacute;n adicional cuando interact&uacute;as con nuestra plataforma, como preferencias, ubicaci&oacute;n geogr&aacute;fica, historial de transacciones y cualquier otra informaci&oacute;n relevante para brindarte nuestros servicios.</p>\r\n<p><strong>2. Uso de la informaci&oacute;n&nbsp;</strong></p>\r\n<p>Utilizamos la informaci&oacute;n recopilada para ofrecerte nuestros servicios y mejorar tu experiencia en nuestra plataforma. Podemos utilizar tu informaci&oacute;n para enviarte comunicaciones relacionadas con nuestros servicios, como actualizaciones, promociones y noticias relevantes. Si no deseas recibir dichas comunicaciones, puedes optar por no recibirlas en cualquier momento.No compartimos tu informaci&oacute;n personal con terceros sin tu consentimiento, excepto en los casos en que sea necesario para brindarte nuestros servicios o cumplir con obligaciones legales.</p>\r\n<p><strong>3. Protecci&oacute;n de la informaci&oacute;n</strong></p>\r\n<p>Implementamos medidas de seguridad t&eacute;cnicas y organizativas para proteger tu informaci&oacute;n personal contra el acceso no autorizado, el uso indebido, la divulgaci&oacute;n o la destrucci&oacute;n. Mantenemos tu informaci&oacute;n personal almacenada en servidores seguros y restringimos el acceso a ella &uacute;nicamente a personal autorizado que necesita conocer dicha informaci&oacute;n para brindarte nuestros servicios.</p>\r\n<p><strong>4. Cookies y tecnolog&iacute;as similares</strong></p>\r\n<p>Utilizamos cookies y otras tecnolog&iacute;as similares para recopilar informaci&oacute;n sobre el uso de nuestra plataforma y personalizar tu experiencia. Puedes configurar tu navegador para rechazar cookies o recibir notificaciones cuando se env&iacute;en cookies. Sin embargo, ten en cuenta que esto puede afectar la funcionalidad de nuestra plataforma.</p>\r\n<p><strong>5. Enlaces externos</strong>&nbsp;</p>\r\n<p>Nuestra plataforma puede contener enlaces a sitios web de terceros. No nos responsabilizamos por las pr&aacute;cticas de privacidad de esos sitios. Te recomendamos revisar las pol&iacute;ticas de privacidad de esos sitios antes de proporcionarles tu informaci&oacute;n personal.</p>\r\n<p><strong>6. Modificaciones</strong></p>\r\n<p>Nos reservamos el derecho de realizar modificaciones o actualizaciones a esta pol&iacute;tica de privacidad en cualquier momento. Te recomendamos revisar peri&oacute;dicamente esta secci&oacute;n para estar informado de cualquier cambio.</p>\r\n<p>Si tienes alguna pregunta o inquietud acerca de nuestras pol&iacute;ticas de privacidad, por favor cont&aacute;ctanos a trav&eacute;s de los canales de comunicaci&oacute;n proporcionados en nuestro sitio web.</p>\r\n<p>Fecha de entrada en vigencia: 30-05-2023</p>\r\n<p>&Uacute;ltima actualizaci&oacute;n: 30-05-2023</p>\r\n<p>&nbsp;</p>', '2022-01-30 12:34:53', '2023-05-31 08:53:56');

-- Volcando datos para la tabla system_service.terrain: ~2 rows (aproximadamente)

-- Volcando datos para la tabla system_service.testimonials: ~3 rows (aproximadamente)
INSERT INTO `testimonials` (`id`, `name`, `image`, `designation`, `comment`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'John Doe', 'uploads/custom-images/john-doe-20220929124436.png', 'MBBS, FCPS, FRCS', 'There are mainy variatons of passages of abut the majority have suffereds alteration in humour, or randomisejd words which rando generators on the Internet tend', 1, '2022-09-29 06:44:37', '2022-11-06 07:37:38'),
	(2, 'David Richard', 'uploads/custom-images/david-richard-20220929124535.png', 'Web Developer', 'There are mainy variatons of passages of abut the majority have suffereds alteration in humour, or randomisejd words which rando generators on the Internet tend', 1, '2022-09-29 06:45:35', '2022-11-06 07:37:47'),
	(3, 'David Simmons', 'uploads/custom-images/david-simmons-20220929124643.png', 'Graphic Designer', 'There are mainy variatons of passages of abut the majority have suffereds alteration in humour, or randomisejd words which rando generators on the Internet tend', 1, '2022-09-29 06:46:43', '2022-11-06 07:39:22');

-- Volcando datos para la tabla system_service.tickets: ~0 rows (aproximadamente)
INSERT INTO `tickets` (`id`, `user_id`, `order_id`, `subject`, `price`, `ticket_id`, `ticket_from`, `status`, `created_at`, `updated_at`) VALUES
	(6, 2, 70, 'TESTE', 0, '1039350210', 'provider', 'pending', '2023-05-20 09:16:55', '2023-05-20 09:16:55');

-- Volcando datos para la tabla system_service.ticket_messages: ~42 rows (aproximadamente)
INSERT INTO `ticket_messages` (`id`, `ticket_id`, `user_id`, `admin_id`, `message`, `message_from`, `unseen_admin`, `unseen_user`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 0, 'test message', 'client', 1, 1, '2022-10-05 04:25:49', '2022-12-05 11:51:03'),
	(2, 1, 1, 1, 'Please share your problem', 'admin', 1, 1, '2022-10-05 04:41:04', '2022-12-05 11:51:03'),
	(3, 1, 1, 1, 'Test message from support team', 'admin', 1, 1, '2022-10-05 04:43:36', '2022-12-05 11:51:03'),
	(4, 2, 1, 0, 'I can\'t login your site, please help', 'client', 1, 1, '2022-10-05 04:44:18', '2023-01-15 03:18:08'),
	(5, 2, 1, 1, 'We deactivate your account, You can\'t login.', 'admin', 1, 1, '2022-10-05 04:44:49', '2023-01-15 03:18:08'),
	(6, 2, 1, 1, 'Please open file below', 'admin', 1, 1, '2022-10-05 05:10:49', '2023-01-15 03:18:08'),
	(7, 2, 1, 0, 'We are dedicated to work with all dynamic features like Laravel, customized website, PHP, SEO', 'client', 1, 1, '2022-10-05 05:45:22', '2023-01-15 03:18:08'),
	(8, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:47:07', '2023-01-15 03:18:08'),
	(9, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:47:24', '2023-01-15 03:18:08'),
	(10, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:48:40', '2023-01-15 03:18:08'),
	(11, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:48:57', '2023-01-15 03:18:08'),
	(12, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:49:08', '2023-01-15 03:18:08'),
	(13, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-10-05 05:49:15', '2023-01-15 03:18:08'),
	(14, 2, 1, 0, 'We are dedicated to work with all dynamic features like Laravel, customized website, PHP, SEO', 'client', 1, 1, '2022-10-05 05:50:05', '2023-01-15 03:18:08'),
	(15, 2, 1, 0, 'We are dedicated to work with all dynamic features like Laravel, customized website, PHP, SEO', 'client', 1, 1, '2022-10-05 05:51:33', '2023-01-15 03:18:08'),
	(16, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:52:18', '2023-01-15 03:18:08'),
	(17, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:54:01', '2023-01-15 03:18:08'),
	(18, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:54:20', '2023-01-15 03:18:08'),
	(19, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:54:38', '2023-01-15 03:18:08'),
	(20, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:54:54', '2023-01-15 03:18:08'),
	(21, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:55:39', '2023-01-15 03:18:08'),
	(22, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:55:46', '2023-01-15 03:18:08'),
	(23, 2, 1, 0, 'test', 'client', 1, 1, '2022-10-05 05:58:59', '2023-01-15 03:18:08'),
	(24, 2, 1, 0, 'features like Laravel, customized website, PHP,', 'client', 1, 1, '2022-10-05 05:59:57', '2023-01-15 03:18:08'),
	(25, 2, 1, 0, 'features like Laravel, customized website, PHP,', 'client', 1, 1, '2022-10-05 06:00:13', '2023-01-15 03:18:08'),
	(26, 2, 1, 0, 'features like Laravel, customized website, PHP,', 'client', 1, 1, '2022-10-05 06:00:35', '2023-01-15 03:18:08'),
	(27, 2, 1, 0, 'We usually monitor the market and policies. We provide all web solutions accordingly and ensure the best service.', 'client', 1, 1, '2022-10-05 06:01:31', '2023-01-15 03:18:08'),
	(28, 2, 1, 0, 'We usually monitor the market and policies. We provide all web solutions accordingly and ensure the best service.', 'client', 1, 1, '2022-10-05 06:01:35', '2023-01-15 03:18:08'),
	(29, 1, 1, 0, 'We are dedicated to work with all dynamic features like Laravel,', 'client', 1, 1, '2022-10-05 06:02:21', '2022-12-05 11:51:03'),
	(30, 1, 1, 0, 'We are dedicated to work with all dynamic features like Laravel,', 'client', 1, 1, '2022-10-05 06:02:35', '2022-12-05 11:51:03'),
	(31, 1, 1, 1, 'wait', 'admin', 1, 1, '2022-10-05 06:03:31', '2022-12-05 11:51:03'),
	(32, 1, 1, 0, 'any update ?', 'client', 0, 1, '2022-10-05 10:48:00', '2022-12-05 11:51:03'),
	(33, 3, 1, 0, 'this is ticket message', 'client', 0, 1, '2022-11-09 05:55:23', '2023-05-23 10:43:21'),
	(34, 2, 1, 0, 'this is test message', 'client', 1, 1, '2022-11-09 06:04:24', '2023-01-15 03:18:08'),
	(35, 4, 2, 0, 'This is test message', 'provider', 0, 1, '2022-11-10 04:01:49', '2022-11-10 04:06:55'),
	(36, 4, 2, 0, 'are you there ?', 'provider', 0, 1, '2022-11-10 04:06:51', '2022-11-10 04:06:55'),
	(37, 4, 2, 0, 'this is test message', 'provider', 0, 1, '2022-11-10 04:14:09', '2022-11-10 04:14:09'),
	(38, 4, 2, 0, 'this is test message', 'provider', 0, 1, '2022-11-10 04:14:30', '2022-11-10 04:14:30'),
	(39, 4, 2, 0, 'this is test message', 'provider', 0, 1, '2022-11-10 04:15:22', '2022-11-10 04:15:22'),
	(40, 5, 2, 0, 'this is test message', 'provider', 1, 1, '2022-11-10 04:24:43', '2023-05-20 09:12:36'),
	(41, 2, 1, 1, 'We usually monitor the market and policies. We provide all web solutions accordingly and ensure the best service.', 'admin', 1, 1, '2022-12-25 11:20:36', '2023-01-15 03:18:08'),
	(42, 6, 2, 0, 'testeando para que sirve', 'provider', 1, 1, '2023-05-20 09:16:56', '2023-07-13 02:31:19');

-- Volcando datos para la tabla system_service.tipo_pregunta: ~4 rows (aproximadamente)
INSERT INTO `tipo_pregunta` (`idTipo`, `tipo`) VALUES
	('N', 'Numérica'),
	('S', 'Selección simple'),
	('T', 'Texto simple'),
	('X', 'Texto multilínea');

-- Volcando datos para la tabla system_service.type_company: ~2 rows (aproximadamente)
INSERT INTO `type_company` (`id`, `name`, `code`) VALUES
	(1, 'Empresas', 'EMP-01'),
	(2, 'Ferreterias', 'FET-01');

-- Volcando datos para la tabla system_service.type_decoration: ~0 rows (aproximadamente)
INSERT INTO `type_decoration` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'Decoracion', 'Dec-001', '2023-07-17 21:38:33', '2023-07-17 21:38:36'),
	(2, 'Amoblado', 'AM-002', '2023-07-17 21:38:53', '2023-07-17 21:38:54');

-- Volcando datos para la tabla system_service.users: ~0 rows (aproximadamente)
INSERT INTO `users` (`id`, `name`, `user_name`, `email`, `email_verified_at`, `password`, `remember_token`, `forget_password_token`, `forget_password_otp`, `status`, `provider_id`, `provider`, `provider_avatar`, `image`, `phone`, `country_id`, `state_id`, `city_id`, `zip_code`, `address`, `is_provider`, `verify_token`, `otp_mail_verify_token`, `email_verified`, `agree_policy`, `designation`, `created_at`, `updated_at`) VALUES
	(1, 'Pruebas', NULL, 'user@gmail.com', NULL, '$2y$10$3n6zeSzLmJey8QGWIuif0OH26F8/XeNhTUnxMqiNV3izdNe5MCv.a', NULL, 'OdfAwBv0qZRt26ADKL3Vl9LIatjJjb7m6Js1oSSvhch0GXklY0dwM3Gs6XEnqxyiTwWYsjQ4fpqtzimE6CagXRnKbXQKof9ZtUkz', NULL, 1, NULL, NULL, NULL, 'uploads/custom-images/john-doe-2022-11-09-11-27-46-1736.jpg', '123-343-4444', 0, 0, 0, NULL, 'Florida City, FL, USA', 0, NULL, NULL, 1, 0, NULL, '2022-09-29 07:44:31', '2023-01-15 03:45:19'),
	(2, 'Andrés Loaysa Lazcano', 'Andrés Loaysa Lazcano', 'provider@gmail.com', NULL, '$2y$10$Li84xiyyPQtlTfFPbQLZkeUsmwXr87YGc/mbzZ03Y8iakasaZY3yu', NULL, NULL, NULL, 1, NULL, NULL, NULL, 'uploads/custom-images/david-simmons-2023-03-09-05-08-17-5028.jpg', '123-343-4444', 1, 1, 2, NULL, 'Los Angeles, CA, USA', 1, NULL, NULL, 1, 0, 'Electrician', '2022-09-29 07:44:31', '2023-03-09 11:08:17'),
	(4, 'Andrés Loaysa Lazcano', 'Andrés Loaysa Lazcano', 'provider1@gmail.com', NULL, '$2y$10$KVMjtClALn6AGJ4ObaUqpeq/RbbawUgkkMEOSjJs1dqS/A2F9ji5C', NULL, NULL, NULL, 1, NULL, NULL, NULL, 'uploads/custom-images/david-richard-2023-01-15-03-38-17-3004.png', '123-343-4444', 1, 2, 1, NULL, 'Florida City, FL, USA', 1, NULL, NULL, 1, 0, 'Web Developer', '2022-10-04 06:24:21', '2023-01-15 09:38:17'),
	(5, 'Daniel Paul', 'daniel_paul', 'provider2@gmail.com', NULL, '$2y$10$SHHv2G/f/PRLQPwR.dnBIulyGdV1u2bo4fUNrpjJxcKsoEOE9JW3G', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '125-985-4587', 2, 5, 9, NULL, 'Gandhinagar, Gujarat, India', 1, NULL, NULL, 1, 0, 'Graphic Designer', '2022-10-04 06:45:34', '2022-10-04 06:55:05'),
	(6, 'David Miller', 'david_miller', 'provider3@gmail.com', NULL, '$2y$10$XROzn42ksW.wG3an5./30enVsC6OBBV2gUzWc7VXwj8UAZd5yrGse', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '123-343-4444', 1, 1, 2, NULL, 'California, USA', 1, NULL, NULL, 1, 0, 'Web Developer', '2022-10-04 06:54:14', '2022-10-04 06:55:11'),
	(7, 'Ken Williamson', 'testusername_14', 'ken@gmail.com', NULL, '$2y$10$Os9CLodYJrxevTolDPG8TuT6Pr1URAqWx6BkLjlF2krAjiyk2TDmS', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '123-954-8745', 1, 3, 2, NULL, 'test address', 1, 'bSRkndxcC0PrO279K5WWEcE2B14uUCfPZrD3S4ffqtCdGHHzIJodPiJMIED1nXSspy7PGBZYHFLXfDPAC44oMgUUdL9s7h5vSn1H', NULL, 0, 0, 'electrician', '2022-11-08 08:46:47', '2022-11-08 08:46:47'),
	(9, 'api user', NULL, 'apiuser@gmail.com', NULL, '$2y$10$WE4c1puU88mGFD5jOULWEOuFMNfIo4SNiZNTNdsZlXW5ahHGiIbaS', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, '482031', 0, 0, NULL, '2022-11-09 07:24:14', '2022-11-09 07:24:14'),
	(10, 'John Abraham', NULL, 'user1@gmail.com', NULL, '$2y$10$u7XO20zsYqIRGAl8iJB4cu.K2Ip6BpBYKCJqvaijcQYn1vtHPV24m', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 1, 0, NULL, '2023-01-09 09:34:55', '2023-01-09 09:35:55'),
	(11, 'Food &amp; Drink', NULL, 'food&amp;and@gmail.com', NULL, '$2y$10$Y.EJfizRPCj3d2T8J3teEuKBxJI.VKfp43eNtrmflXWDXjc51C8KO', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, 0, 'GpOR1S9BXtLfoEEsg3DQyNlLMxorq5y1z59wPnTVLmCxFADXLnNbEtAvAwdEokYT6EAvWCwJbKgha82BI55PyuRT4K5ZF8TUQYKj', NULL, 0, 0, NULL, '2023-01-15 07:15:38', '2023-01-15 07:15:38'),
	(12, 'Ramiro Mamani Soto', NULL, '201601ramiro@gmail.com', NULL, '$2y$10$KqjGMbH5GmpPl1XMabU67OxYE2rSIcftK1rPmnd/pWU.Scxs3erpW', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '67470820', 0, 0, 0, NULL, NULL, 0, 'xEGoJ8ax4KxE1S1jlX9qvfMnUc6s7nAFnLViQoKIPaMpawtbCVDtHszgmCZFrdCo78342MjWbPLw1LWoehvkQrWz2nXGSaYJ81WP', NULL, 0, 0, NULL, '2023-05-05 08:50:57', '2023-05-05 08:50:57'),
	(16, 'ramiro', 'ramiro', '20160102ramiro@gmail.com', NULL, '$2y$10$Vup4XDaF74tE8JRDuXi5l.0fbZLMLVK0l6Q84yHrPz3PR8LdbSMcm', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '67470820', 1, 1, 7, NULL, 'sacaba cochabamba', 1, 'eXZ6eb2EolmGfJvAHNTwJ9GtZS3E62dmpstfnVdLl8nhmm1Ldd2vOFA6eVxTkAIXulqLMDpDJ1FTfWZBmVeQzrPtf1IxnNkwfsF0', NULL, 0, 0, 'Car Services', '2023-05-06 07:52:03', '2023-05-06 07:52:03');

-- Volcando datos para la tabla system_service.withdraw_methods: ~0 rows (aproximadamente)
INSERT INTO `withdraw_methods` (`id`, `name`, `min_amount`, `max_amount`, `withdraw_charge`, `description`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Bank Payment', 10, 20, 1, '<p>Bank Name: Your bank name</p><p><span style="background-color: transparent;">Account Number:&nbsp; Your bank account number</span></p><p>Routing Number: Your bank routing number</p><p>Branch: Your bank branch name</p>', 1, '2022-10-08 03:15:37', '2022-10-08 03:15:37');

-- Volcando datos para la tabla system_service.zone: ~0 rows (aproximadamente)
INSERT INTO `zone` (`id`, `name`, `code`, `cities_id`, `created_at`, `updated_at`) VALUES
	(1, 'Ramada', 'RM', 1, '2023-06-20 20:32:42', '2023-06-20 20:32:52');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
